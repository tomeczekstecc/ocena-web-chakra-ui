FROM node:16-alpine AS build

WORKDIR /app

COPY ./package.json .

RUN yarn install

COPY . .

RUN yarn build


FROM nginx:latest AS serve

COPY --from=build /app/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/build /usr/share/nginx/html/

CMD nginx -g "daemon off;"
