import {gql} from '@apollo/client'

export const deleteFileM = gql`
    mutation deleteFile($filePath: String!) {
        deleteFile(filePath: $filePath)
    }
`
