import {gql} from "@apollo/client";

export const zwrocOceneM = gql`
    mutation ZwrocOcene($zwrocOceneInput: ZwrocOceneInput) {
        zwrocOcene(zwrocOceneInput: $zwrocOceneInput)
    }

`
