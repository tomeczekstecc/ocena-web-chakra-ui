import gql from "graphql-tag";

export const uploadFileM = gql`
    mutation uploadFile($file: Upload!, $meta: FileMeta) {
        uploadFile(file: $file, meta: $meta) {
            filename
        }
    }
`
