import {gql} from "@apollo/client";

export const zatwierdzOceneM = gql`
    mutation ZatwierdzOcene($zatwierdzOceneInput: ZatwierdzOceneInput) {
        zatwierdzOcene(zatwierdzOceneInput: $zatwierdzOceneInput)
    }

`
