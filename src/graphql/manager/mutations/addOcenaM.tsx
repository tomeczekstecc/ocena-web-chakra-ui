import {gql} from '@apollo/client'

export const addOcenaM = gql`
    mutation addOcena(
        $oceniajacy_id: String!,
        $wniosek_id: Int!,
        $punkty: Int,
        $oceniajacy_email: String!,
        $oceniajacy_first_name: String!,
        $oceniajacy_last_name: String!,
        $oceniajacy_username: String!
    ) {
        addOcena(oceniajacy_id: $oceniajacy_id, wniosek_id: $wniosek_id, punkty: $punkty,  oceniajacy_email:$oceniajacy_email,
            oceniajacy_first_name:$oceniajacy_first_name,oceniajacy_last_name:$oceniajacy_last_name,oceniajacy_username:$oceniajacy_username
        ) {
            id
            wniosek_id
        }
    }
`
