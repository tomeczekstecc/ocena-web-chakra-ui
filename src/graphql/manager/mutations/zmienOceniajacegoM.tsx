import {gql} from "@apollo/client";

export const zmienOceniajacegoM = gql`
    mutation ZmienOceniajacego($ocena_id: Int!, $oceniajacy_id: String!, $oceniajacy_email: String!, $oceniajacy_first_name: String!, $oceniajacy_last_name: String!, ) {
        zmienOceniajacego(ocena_id: $ocena_id, oceniajacy_id: $oceniajacy_id, oceniajacy_email: $oceniajacy_email, oceniajacy_first_name: $oceniajacy_first_name, oceniajacy_last_name: $oceniajacy_last_name)
    }
`
