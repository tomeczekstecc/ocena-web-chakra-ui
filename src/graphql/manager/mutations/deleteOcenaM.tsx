import {gql} from '@apollo/client'

export const deleteOcenaM = gql`
    mutation deleteOcena($ocena_id: Int!) {
        deleteOcena(ocena_id: $ocena_id)
    }
`
