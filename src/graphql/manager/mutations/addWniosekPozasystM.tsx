import {gql} from '@apollo/client'

export const addWniosekPozasystM = gql`
    mutation AddWniosekPozasyst($wniosekPozasystInput: WniosekPozasystInput) {
        addWniosekPozasyst(wniosekPozasystInput: $wniosekPozasystInput)
    }

`
