import {gql} from '@apollo/client'

export const oceniajacyQ = gql`
    query oceniajacy($oceniajacy_id: String!) {
        oceniajacy(oceniajacy_id: $oceniajacy_id) {
            id
            username
            email
            first_name
            last_name

        }
    }
`
