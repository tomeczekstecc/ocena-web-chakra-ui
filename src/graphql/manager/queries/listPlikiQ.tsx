import {gql} from '@apollo/client'

export const listPlikiQ = gql`
    query listPlikiQ($domain: String!) {
        listPliki(domain: $domain) {
            fileName
            filePath
            fileType
            id
            name
        }
    }
`
