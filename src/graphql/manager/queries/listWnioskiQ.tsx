import {gql} from '@apollo/client'

export const listWnioskiQ = gql`
    query ListWnioski {
        listWnioski {
            id
            numer
            wnioskodawca_imie
            wnioskodawca_nazwisko
            status
            uczen {
                imie
                nazwisko
            }
            szkola {
                nazwa
                miejscowosc
            }
            ocena {
                oceniajacy_first_name
                oceniajacy_last_name
                status {
                    nazwa
                }
            }
        }
    }
`
