import {gql} from '@apollo/client'

export const listUsersOcenyQ = gql`
    query ListUsersOceny($oceniajacy_id: String!) {
        listUsersOceny(oceniajacy_id: $oceniajacy_id) {
            id
            status {
                nazwa
                id
            }
            wniosek_id
            wniosek {
                numer
                wnioskodawca_imie
                wnioskodawca_nazwisko
            }
            punkty
        }
    }

`
