import {gql} from '@apollo/client'

export const listOcenyQ = gql`
    query ListOceny {
        listOceny {
            id
            oceniajacy_first_name
            oceniajacy_last_name
            oceniajacy_username
            wniosek_id
            oceniajacy_id
            status {
                id
                nazwa
            }
        }
    }
`
