import {gql} from '@apollo/client'

export const listOceniajacyQ = gql`
    query listOceniajacy {
        listOceniajacy {
            id
            first_name
            last_name
            username
            email
            count
        }
    }
`
