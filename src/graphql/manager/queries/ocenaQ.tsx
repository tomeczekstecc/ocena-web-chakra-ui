import {gql} from '@apollo/client'

export const ocena = gql`
    query ocena($ocena_id: Int!) {
        ocena(ocena_id: $ocena_id) {
            id
            punkty
            oceniajacy_id
        }
    }
`
