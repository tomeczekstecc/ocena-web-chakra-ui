import {gql} from '@apollo/client'

export const reportListWnioskiQ = gql`
    query reportListWnioski {
        reportListWnioski {
            errors
            fileName
            valid
        }
    }
`
