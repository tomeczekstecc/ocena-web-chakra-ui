import {gql} from '@apollo/client'

export const reportListOcenyQ = gql`
    query reportListOceny {
        reportListOceny {
            errors
            fileName
            valid
        }
    }
`
