import {gql} from '@apollo/client'

export const reportListOceniajacyQ = gql`
    query reportListOceniajacy {
        reportListOceniajacy {
            errors
            fileName
            valid
        }
    }
`
