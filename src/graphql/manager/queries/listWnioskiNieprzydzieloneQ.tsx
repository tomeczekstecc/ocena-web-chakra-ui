import {gql} from '@apollo/client'

export const listWnioskiNieprzydzieloneQ = gql`
    query ListWnioskiNieprzydzielone {
        listWnioskiNieprzydzielone {
            id
            wnioskodawca_imie
            wnioskodawca_nazwisko
            uczen {
                imie
                nazwisko
            }
        }
    }


`
