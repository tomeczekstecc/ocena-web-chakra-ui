import {gql} from '@apollo/client'

export const uzupelnijWniosekPozasystM = gql`
    mutation UzupelnijWniosekPozasyst($uzupelnijWniosekPozasystInput: UzupelnijWniosekPozasystInput) {
        uzupelnijWniosekPozasyst(uzupelnijWniosekPozasystInput: $uzupelnijWniosekPozasystInput)
    }
`
