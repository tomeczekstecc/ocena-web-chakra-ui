import {gql} from '@apollo/client'

export const submitOcenaM = gql`
    mutation SubmitOcena($ocenaInput: OcenaInput) {
        submitOcena(ocenaInput: $ocenaInput)
    }
`
