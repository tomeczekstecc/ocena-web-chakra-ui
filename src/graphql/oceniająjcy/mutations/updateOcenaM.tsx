import {gql} from '@apollo/client'

export const updateOcenaM = gql`
    mutation UpdateOcena($ocenaInput: OcenaInput) {
        updateOcena(ocenaInput: $ocenaInput)
    }
`
