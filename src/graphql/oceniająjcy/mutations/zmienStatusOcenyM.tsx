import {gql} from '@apollo/client'

export const zmienStatusOcenyM = gql`
    mutation ZmienStatusOceny($zmienStatusOcenyInput: ZmienStatusOcenyInput) {
        zmienStatusOceny(zmienStatusOcenyInput: $zmienStatusOcenyInput)
    }
`
