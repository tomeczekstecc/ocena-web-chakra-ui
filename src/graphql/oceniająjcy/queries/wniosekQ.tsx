import {gql} from '@apollo/client'

export const WniosekQ = gql`
    query Wniosek($wniosek_id: Int!) {
        wniosek(wniosek_id: $wniosek_id) {
            id
            numer
            status
            systemowy
            uzupelniony
            typ_wnioskodawcy
            telefon
            oswiadczenie
            created_at
            updated_at
            wnioskodawca_id
            wnioskodawca_imie
            wnioskodawca_nazwisko
            wnioskodawca_email
            wnioskodawca_telefon
            szkola {
                nazwa
                ulica
                nr_budynku
                miejscowosc
                kod_pocztowy
                wojewodztwo
                typ
            }
            uczen {
                id
                imie
                nazwisko
                pesel
                telefon
                email
                klasa
            }
            sciezka1 {
                przedmiot
                udzial_w_konk
                praca_badawcza
                referat
                publikacja
                wystawa
                aplikacja
                certyfikat
                uzyskanie_bdb
                strona_www
                inne
                film
            }
            sciezka2 {
                przedmiot
                udzial_w_konk
                praca_badawcza
                referat
                publikacja
                wystawa
                aplikacja
                certyfikat
                uzyskanie_bdb
                strona_www
                inne
                film
            }
            opiekun {
                imie
                nazwisko
                profil
                email
                telefon
            }
            kryteria_dodatk {
                czy_z_niepelnospr
                czy_finalista
                czy_zezwolenie
            }
            kryteria_podst {
                matematyka_ocena
                kluczowy_jezyk_obcy_nazwa
                kluczowy_jezyk_obcy_ocena
                kluczowy_inny_przedmiot_nazwa
                kluczowy_inny_przedmiot_ocena
                inny_przedmiot_nazwa
                srednia_kierunkowe
                srednia
            }
            budzet {
                pomoce_dydakt
                wyposaz_domu_do_nauki
                sprzet_komput
                sprzet_elektron
                internet
                kursy
                transport
                pru
                oplaty
                koszt_udzial_w_konkur
                kultur_wysok
                inne
                razem
                uzasadnienie_pozostale
                uzasadnienie_ponowne
            }
        }
    }
`
