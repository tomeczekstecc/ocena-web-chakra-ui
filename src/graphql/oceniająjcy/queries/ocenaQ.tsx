import {gql} from '@apollo/client'

export const ocenaQ = gql`
    query Ocena($ocena_id: Int!) {
        ocena(ocena_id: $ocena_id) {
            id
            status {
                id
            }
            formalne {
                _1a1
                _1a1_uwagi
                _1a2
                _1a2_uwagi
                _1a3
                _1a3_uwagi
                _1a4
                _1a4_uwagi
                _1b1
                _1b1_uwagi
                _1b2
                _1b2_uwagi
                _1b3
                _1b3_uwagi
                _1c1
                _1c1_uwagi
                _1c2
                _1c2_uwagi
                _1c3
                _1c3_uwagi
                _1c4
                _1c4_uwagi
                _1c5
                _1c5_uwagi
                _1c6
                _1c6_uwagi
                _1c7
                _1c7_uwagi
                _1c8
                _1c8_uwagi
                _1podsumowanie1
                _1podsumowanie1_uwagi
                _1podsumowanie2
                _1podsumowanie2_uwagi
                _2_1
                _2_1_uwagi
                _2_2
                _2_2_uwagi
            }
            merytoryczne {
                obow1
                obow1_punkty
                obow2
                obow2_punkty
                dodatk1_1
                dodatk1_1_punkty
                dodatk1_2
                dodatk1_2_punkty
                dodatk1_3
                dodatk1_3_punkty
                dodatk1_4
                dodatk1_4_punkty
                dodatk2_1
                dodatk2_1_punkty
                dodatk2_2
                dodatk2_2_punkty
                dodatk2_3
                dodatk2_3_punkty
                dodatk2_4
                dodatk2_4_punkty
                indywidualny_program
                indywidualny_program_punkty
                czy_z_niepelnospr
                czy_z_niepelnospr_punkty
            }
            dane {
                numer_wniosku_uwagi
                czy_epuap
                data_wplywu_wniosku
                data_wplywu_wniosku_uwagi
                imie_ucznia_uwagi
                nazwisko_ucznia_uwagi
                imie_wnioskodawcy_uwagi
                nazwisko_wnioskodawcy_uwagi
                telefon_wnioskodawcy_uwagi
                email_wnioskodawcy_uwagi
                rodzaj_szkoly_uwagi
                szkola_uwagi
                klasa_ucznia_uwagi
                imie_opiekuna_uwagi
                nazwisko_opiekuna_uwagi
                telefon_opiekuna_uwagi
                email_opiekuna_uwagi
            }
        }
    }
`
