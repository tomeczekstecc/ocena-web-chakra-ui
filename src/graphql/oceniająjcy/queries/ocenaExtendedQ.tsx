import {gql} from '@apollo/client'

export const ocenaExtendedQ = gql`
    query Ocena($ocena_id: Int!) {
        ocena(ocena_id: $ocena_id) {
            id
            wniosek_id
            oceniajacy_id
            punkty
            oceniajacy_email
            oceniajacy_first_name
            oceniajacy_last_name
            weryfikacja {
                id
                decyzja
                user_id
                user_first_name
                user_last_name
                user_email
                note
                created_at
            }
            status {
                nazwa
                id
            }
            wniosek {
                id
                numer
                status
                typ_wnioskodawcy
                telefon
                oswiadczenie
                created_at
                updated_at
                wnioskodawca_id
                wnioskodawca_imie
                wnioskodawca_nazwisko
                wnioskodawca_email
                wnioskodawca_telefon
                szkola {
                    nazwa
                    ulica
                    nr_budynku
                    miejscowosc
                    kod_pocztowy
                    wojewodztwo
                    typ
                }
                uczen {
                    id
                    imie
                    nazwisko
                    pesel
                    telefon
                    email
                    klasa
                }
                opiekun {
                    imie
                    nazwisko
                    telefon
                    email

                }

            }
        }
    }
`
