import {gql} from '@apollo/client'

export const reportListUsersOcenyQ = gql`
    query ReportListUsersOceny($oceniajacy_id: String!) {
        reportListUsersOceny(oceniajacy_id: $oceniajacy_id) {
            errors
            fileName
            valid
        }
    }
`
