export const keyMapper = (obj: object, str: string) => {
    return str.split(".").reduce(function (o, x) {
        // @ts-ignore*
        return o?.[x]
    }, obj);
}
