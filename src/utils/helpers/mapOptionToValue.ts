export const mapOptionToValue = (option: string | number, mapper: string) => {

    switch (mapper) {
        case 'priGradesAvg':
            switch (option) {
                case 5.33:
                    return 14
                case 5.67:
                    return 16
                case 6.00:
                    return 18
                default:
                    return 0
            }

        case 'allGradesAvg':
            switch (option) {
                case '5,85 – 6,00':
                    return 18
                case '5,68 – 5,84':
                    return 16
                case '5,51 – 5,67':
                    return 14
                case '5,34 – 5,50':
                    return 12
                case '5,18 – 5,33':
                    return 10
                case '5,00 - 5,17':
                    return 8
                default:
                    return 0
            }


        case 'dodatk1_1':
            // @ts-ignore
            return option * 16

        case 'dodatk1_2':
            // @ts-ignore
            return option * 14

        case 'dodatk1_3':
            // @ts-ignore
            return option * 8

        case 'dodatk1_4':
            // @ts-ignore
            return option * 6


        case 'dodatk2_1':
            // @ts-ignore
            return option * 13

        case 'dodatk2_2':
            // @ts-ignore
            return option * 11

        case 'dodatk2_3':
            // @ts-ignore
            return option * 5

        case 'dodatk2_4':
            // @ts-ignore
            return option * 3

        case 'indywidualny_program':
            switch (option) {
                case 'ze wszystkich przedmiotów':
                    return 12
                case 'z 1 lub większej ilości przedmiotów kluczowych':
                    return 6
                case 'z 1 lub większej ilości przedmiotów pozostałych':
                    return 4
                case 'Nie':
                    return 0

                default:
                    return 0
            }

        case 'czy_z_niepelnospr':
            switch (option) {
                case 0:
                    return 0
                case 1:
                    return 5

                default:
                    return 0
            }

        default:
            return
    }

}
