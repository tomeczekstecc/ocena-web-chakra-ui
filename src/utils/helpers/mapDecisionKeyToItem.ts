import {
    ocenaCzescA_B,
    ocenaDecyzjaIFormalna,
    ocenaDecyzjaIIFormalna,
    ocenaKryteriaFormalne, ocenaKryteriaObligatoryjne
} from "../../pages/oceniajacy/PageOcena/rows";

export const mapDecisionKeyToItem = (key: string) => {

    const arrayToSearch = [...ocenaCzescA_B, ...ocenaDecyzjaIFormalna, ...ocenaDecyzjaIFormalna,
        ...ocenaDecyzjaIIFormalna, ...ocenaKryteriaFormalne, ...ocenaKryteriaObligatoryjne]

    return arrayToSearch.find(i => i.dataKey === key)

}
