export const countErrors = (errObj: object, key: string) => {
    const num = Object.keys(errObj).filter(e => e.startsWith(key)).length

    return num > 0 ? num : ''
}
