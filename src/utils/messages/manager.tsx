export const titles = {
    success: {
        created: 'Utworzono!',
        deleted: 'Usunięto!',
    },
    fail: {
        limitExceeded: 'Przekroczono limit'
    }
}


export const msg = {
    success: {
        addedOcena: 'Przydzielono oceniającego!',
        swappedOceniajacy: "Zmieniono oceniającego!",
        reportDownloaded: "Pobrano raport!",
        deletedOcena: "Usunięto ocenę!",
        submittedOcena: "Przesłano ocenę!",
        savedOcena: "Zapisano ocenę!",
        savedWniosekPozasyst: "Zapisano wniosek pozasystemowy!",
        decline: "Zwrócono ocenę!",
        approve: "Zatwierdzono ocenę!",
        filled: "Uzupełniono wniosek pozasystemowy!"
    },
    fail: {
        reportDownloaded: "Nie pobrano raportu!",
        std: "Pojawił się błąd aplikacji. Żądanie tymczasowo nie może być zrealizowane",
    }
}

export const validMsg = {
    stdRequired: 'Pole jest wymagane',
    min10: 'Wartość musi składać się co najmniej z 10 znaków',
    min2: 'Wartość musi składać się co najmniej z 2 znaków',
    email: 'Wartość musi być poprawnym adresem email',
    pesel: 'Wartość musi być poprawnym numerem PESEL',
    klasa: 'Dopuszczalne wartości to: I, II, III, IV'
}
