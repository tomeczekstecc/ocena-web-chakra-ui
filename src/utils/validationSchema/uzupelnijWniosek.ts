import * as Yup from "yup";
import {validMsg} from "../messages/manager";

export const uzupelnijWniosek = Yup.object({
    email_opiekuna: Yup.string().email(validMsg.email).required(validMsg.stdRequired),
    wnioskodawca_email: Yup.string().email(validMsg.email).required(validMsg.stdRequired),
    imie_opiekuna: Yup.string().min(2, validMsg.min2).required(validMsg.stdRequired),
    imie_ucznia: Yup.string().min(2, validMsg.min2).required(validMsg.stdRequired),
    klasa_ucznia: Yup.string().oneOf(["I", "II", "III", "IV"], validMsg.klasa).required(validMsg.stdRequired),
    nazwisko_opiekuna: Yup.string().min(2, validMsg.min2).required(validMsg.stdRequired),
    nazwisko_ucznia: Yup.string().min(2, validMsg.min2).required(validMsg.stdRequired),
    rodzaj_szkoly: Yup.string().min(2, validMsg.min2).required(validMsg.stdRequired),
    szkola: Yup.string().min(2, validMsg.min2).required(validMsg.stdRequired),
    telefon_opiekuna: Yup.string().min(2, validMsg.min2).required(validMsg.stdRequired),
    wnioskodawca_telefon: Yup.string().min(2, validMsg.min2).required(validMsg.stdRequired),
})
