import * as Yup from "yup";

export const dodajWniosekPozasystemowy = Yup.object({
    pesel: Yup.string().min(2, 'Wartość musi być poprawnym numerem PESEL').required('Pole jest wymagane'),
    wnioskodawca_imie: Yup.string().min(2, 'Wartość musi składać się co najmniej z 2 znaków').required('Pole jest wymagane'),
    wnioskodawca_nazwisko: Yup.string().min(2, 'Wartość musi składać się co najmniej z 2 znaków').required('Pole jest wymagane'),
})
