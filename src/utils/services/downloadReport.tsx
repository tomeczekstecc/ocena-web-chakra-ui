import {OperationVariables, QueryResult} from "@apollo/client";
import {saveAs} from "file-saver";

import {Axios} from "../../components/useAxios";
import store from "../../redux/store";
import {addToast} from "../../redux/toaster";
import {logToC} from "../helpers";


export const downloadReport = (fileName: QueryResult<any, OperationVariables>) => {


    Axios
        .get(`/reports/download/tmp/${fileName}/xlsx`, {
            responseType: 'blob',
        })
        .then((response: { data: BlobPart }) => {
            // @ts-ignore
            if (response.status === 200) {
                const blob = new Blob([response.data])
                saveAs(blob, fileName + '.xlsx')
            } else {
                store.dispatch(addToast({
                    message: 'Nie udało się pobrać pliku',
                    type: 'error',
                }))
            }


        }).catch(e => store.dispatch(
        addToast({
            variant: "error",
            text: `Wewnętrzny błąd serwera:${e.message} `,

        })))
}



