import {saveAs} from "file-saver";
import {Axios} from "../../components/useAxios";
import store from "../../redux/store";
import {addToast} from "../../redux/toaster";


export const downloadFile = (name: string, fileName: string, folder: string, subfolder = undefined) => {

    const directory = subfolder ? (folder + '/' + subfolder) : folder

    Axios
        .get(`/files/download/${directory}/${name}`, {
            responseType: 'blob',
        })
        .then((response: { data: BlobPart }) => {
            // @ts-ignore
            const blob = new Blob([response.data])
            if (response.data) {
                saveAs(blob, fileName)
            } else {
                store.dispatch(addToast({
                    message: 'Nie udało się pobrać pliku',
                    type: 'error',
                }))
            }
        }).catch(e => store.dispatch(
        addToast({
            variant: "error",
            text: `Nie udało się pobrać pliku`,

        })))
}





