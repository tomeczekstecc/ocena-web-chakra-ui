import {saveAs} from "file-saver";
import {Axios} from "../../components/useAxios";


export const downloadPdf = (fileName: string, folder: string, subfolder: string) => {
    Axios
        .get(`/pdf/download/${folder}/${subfolder}/${fileName}`, {
            responseType: 'blob',
        })
        .then((response: { data: BlobPart }) => {
            const blob = new Blob([response.data])
            saveAs(blob, fileName)
        })
}



