import Keycloak from 'keycloak-js'

// @ts-ignore
const keycloak = new Keycloak('/keycloak.json')
export default keycloak
