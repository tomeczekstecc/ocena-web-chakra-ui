import {configureStore} from '@reduxjs/toolkit'
import {toaster, user, ocena, wniosek} from './'

const store = configureStore({
    reducer: {
        toaster: toaster,
        user: user,
        ocena: ocena,
        wniosek: wniosek
    }
})

export default store
