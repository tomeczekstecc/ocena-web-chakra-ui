import {createSlice} from "@reduxjs/toolkit";


export const slice = createSlice({
    name: 'wniosek', initialState: {}, reducers: {
// @ts-ignore
        setWniosekData: (state, action) => {
            const {payload} = action
            return {...payload}
        },
    }
})

export const wniosekData = (state: any) => state.wniosek

export const {setWniosekData} = slice.actions

export default slice.reducer
