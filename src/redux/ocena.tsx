import {createSlice} from "@reduxjs/toolkit";


export const slice = createSlice({
    name: 'ocena', initialState: {}, reducers: {
// @ts-ignore
        setOcenaData: (state, action) => {
            const {payload} = action
            return {...state, data: payload}
        },
        setMode: (state, action) => {
            const {payload} = action
            return {...state, mode: payload}
        },
    }
})


export const {setOcenaData, setMode} = slice.actions

export default slice.reducer
