import {createSlice} from "@reduxjs/toolkit";
import {toast} from "react-toastify";


export const slice = createSlice({
    name: 'toaster', initialState: [], reducers: {
// @ts-ignore
        addToast: (state, action) => {
            const {payload} = action
            // @ts-ignore

            const time = payload.variant === 'success' ? 3000 : 7000

            // @ts-ignore
            toast[`${payload.variant}`](payload.text, {theme: 'dark', autoClose: time});

        }
    }
})


export const {addToast} = slice.actions

export default slice.reducer
