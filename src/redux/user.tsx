import {createSlice} from "@reduxjs/toolkit";


export const slice = createSlice({
    name: 'user', initialState: {}, reducers: {
// @ts-ignore
        addUserInfo: (state, action) => {
            const {payload} = action
            return {...state, info: payload}
        }, addUserRoles: (state, action) => {
            const {payload} = action
            return {...state, info: payload}
        },
    }
})


export const {addUserInfo} = slice.actions

export default slice.reducer
