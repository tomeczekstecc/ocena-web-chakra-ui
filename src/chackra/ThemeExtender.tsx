import {extendTheme} from '@chakra-ui/react';
import type {GlobalStyleProps, Styles} from '@chakra-ui/theme-tools';
import {mode} from '@chakra-ui/theme-tools';

// setup light/dark mode global defaults
const styles: Styles = {
    global: (props) => ({
        body: {
            color: mode('gray.800', 'whiteAlpha.800')(props),
            bg: mode('whiteAlpha.800', 'gray.800')(props)
        }
    })
};

const components = {
    Link: {
        // setup light/dark mode component defaults
        baseStyle: (props: GlobalStyleProps) => ({
            color: mode('blue.400', 'blue.300')(props)
        })
    }
};

const config = {
    initialColorMode: 'dark'
}

const theme = extendTheme({
    config,
    components,
    styles
});

export default theme













