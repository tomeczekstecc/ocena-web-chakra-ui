import React from 'react';
import {Button, HStack, useColorMode} from "@chakra-ui/react";
import PropTypes from "prop-types";
import {ArrowLeftIcon, ArrowRightIcon} from "@chakra-ui/icons";
import {useNavigate} from "react-router-dom";

export const ButtonsPanel = (props: any) => {

    const {colorMode} = useColorMode()

    const navigate = useNavigate()


    return (
        <HStack zIndex={999} boxShadow={`0 0 5px 5px ${colorMode === 'light' ? 'white' : '#1A202C'}`} rounded={'md'}
                p={0}
                opacity={.85}
                bg={colorMode === 'light' ? 'white' : 'gray.800'}
                float={'right'}
                position={'fixed'}
                bottom={'2vw'}
                right={'3vw'}
        >

            <Button disabled={props.disablePrev} title={'poprzedni'} display={props.noNav ? 'none' : undefined}
                    variant={'outline'}
                    colorScheme={'blue'}
                    onClick={() => props.handleTabsChange(props.curIndex - 1)}>{props.prevBtnText}</Button>

            <Button title={'zapisz'}
                    display={(props.noSave || props.watchMode || props.verifyMode) ? 'none' : undefined}
                    variant={'outline'}
                    colorScheme={'green'}
                    onClick={() => props.save()}>{props.saveBtnText}</Button>

            <Button title={'prześlij'}
                    display={(props.noSubmit || props.watchMode || props.verifyMode) ? 'none' : undefined}
                    variant={'outline'}
                    colorScheme={'red'}
                    onClick={() => props.submit()}>{props.subBtnText}</Button>

            <Button title={'zatwierdź ocenę'} display={(props.noSubmit || !props.verifyMode) ? 'none' : undefined}
                    variant={'outline'}
                    colorScheme={'green'}
                    onClick={() => props.approve()}>{props.approveBtnText}</Button>

            <Button title={'zwróć ocenę'} display={(props.noDecline || !props.verifyMode) ? 'none' : undefined}
                    variant={'outline'}
                    colorScheme={'red'}
                    onClick={() => props.decline()}>{props.declineBtnText}</Button>

            <Button title={'wróć'} display={props.noCancel ? 'none' : undefined} variant={'outline'}
                    colorScheme={'orange'}
                    onClick={() => navigate(props.cancelDest)}>{props.cancelBtnText}</Button>

            <Button disabled={props.disableNext} title={'następny'} display={props.noNav ? 'none' : undefined}
                    variant={'outline'}
                    colorScheme={'blue'}
                    onClick={() => props.handleTabsChange(props.curIndex + 1)}>{props.nextBtnText}</Button>
        </HStack>
    );
}

ButtonsPanel.defaultProps = {
    saveBtnText: 'Zapisz',
    declineBtnText: 'Zwróć',
    approveBtnText: 'Zatwierdź',
    subBtnText: 'Prześlij',
    prevBtnText: <ArrowLeftIcon/>,
    cancelBtnText: 'Anuluj',
    cancelDest: -1,
    nextBtnText: <ArrowRightIcon/>,
}

ButtonsPanel.propTypes = {
    saveBtnText: PropTypes.string,
    submitBtnText: PropTypes.string,
    declineBtnText: PropTypes.string,
    approveBtnText: PropTypes.string,
    prevBtnText: PropTypes.any,
    nextBtnText: PropTypes.any,
    cancelBtnText: PropTypes.string,
    handleTabsChange: PropTypes.func,
    save: PropTypes.func,
    submit: PropTypes.func,
    approve: PropTypes.func,
    decline: PropTypes.func,
    noNav: PropTypes.bool,
    noCancel: PropTypes.bool,
    noSave: PropTypes.bool,
    noDecline: PropTypes.bool,
    noSubmit: PropTypes.bool,
    cancelDest: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    curIndex: PropTypes.number,
    disableNext: PropTypes.bool,
    disablePrev: PropTypes.bool,
    watchMode: PropTypes.bool,
    verifyMode: PropTypes.bool,
}


export default ButtonsPanel;
