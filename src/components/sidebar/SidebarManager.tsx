import React from 'react';
import {SidebarDataManager} from './SidebarData';
import SubMenu from './SubMenu';
// import { IconContext } from 'react-icons/lib';
import {Box, useColorMode} from "@chakra-ui/react";
import {Logo} from "../Logo";


export const SidebarManager = () => {

    const mode = useColorMode()

    return (
        <Box
            flexGrow={1}
             bg={mode.colorMode === 'dark' ? "gray.700" : "gray.100"}>
            <Logo name='OCENY'/>
            {SidebarDataManager.map((item: any, index: React.Key | null | undefined) => {
                return <SubMenu item={item} key={index}/>;
            })}

            {/*</IconContext.Provider>*/}
        </Box>
    );
};

