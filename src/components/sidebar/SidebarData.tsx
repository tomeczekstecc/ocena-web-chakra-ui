import React from 'react'
import * as GhIcons from 'react-icons/go'
import * as RiIcons from 'react-icons/ri'
import {RiFolderUploadFill, RiFileList2Fill, RiTeamFill, RiHome2Line} from "react-icons/ri";

export const SidebarDataManager = [
    {
        title: 'Start',
        path: '/manager',
        icon: <RiHome2Line size={24}/>,
        iconClosed: <RiIcons.RiArrowDownSFill/>,
        iconOpened: <RiIcons.RiArrowUpSFill/>,

        // subNav: [
        //     {
        //         title: 'Users',
        //         path: '/overview/users',
        //         icon: <GhIcons.GoFile />
        //     },
        //     {
        //         title: 'Revenue',
        //         path: '/overview/revenue',
        //         icon: <GhIcons.GoFile />
        //     }
        // ]
    },
    {
        title: 'Wnioski',
        path: '/manager/wnioski',
        icon: <RiFileList2Fill size={24}/>,
        iconClosed: <RiIcons.RiArrowDownSFill/>,
        iconOpened: <RiIcons.RiArrowUpSFill/>,

    },
    {
        title: 'Oceny',
        path: '/manager/oceny',
        icon: <RiIcons.RiArticleLine size={24}/>,
    },
    {
        title: 'Lista oceniających',
        path: '/manager/oceniajacy',
        icon: <RiTeamFill size={24}/>,
    },
    // {
    //     title: 'Messages',
    //     path: '/manager/oceny',
    //     icon: <RiTodoFill/>,
    //
    //     iconClosed: <RiIcons.RiArrowDownSFill/>,
    //     iconOpened: <RiIcons.RiArrowUpSFill/>,
    //
    //     subNav: [
    //         {
    //             title: 'Message 1',
    //             path: '/manager/oceny',
    //             icon: <GhIcons.GoFile/>,
    //         },
    //         {
    //             title: 'Message 2',
    //             path: '/manager/oceny',
    //             icon: <GhIcons.GoFile/>,
    //         },
    //     ],
    // },

    {
        title: 'Dodaj wniosek',
        path: '/manager/dodaj',
        icon: <RiIcons.RiFileAddLine size={24}/>,
    },

    {
        title: 'Repozytorium',
        path: '/manager/upload',
        icon: <RiIcons.RiFileCloudLine size={24}/>,
    },

]
export const SidebarDataAdmin = [
    {
        title: 'Start',
        path: '/manager',
        icon: <GhIcons.GoHome size={24}/>,
        iconClosed: <RiIcons.RiArrowDownSFill/>,
        iconOpened: <RiIcons.RiArrowUpSFill/>,

        // subNav: [
        //     {
        //         title: 'Users',
        //         path: '/overview/users',
        //         icon: <GhIcons.GoFile />
        //     },
        //     {
        //         title: 'Revenue',
        //         path: '/overview/revenue',
        //         icon: <GhIcons.GoFile />
        //     }
        // ]
    },
]

export const SidebarDataOceniajacy = [
    {
        title: 'Start',
        path: '/oceniajacy',
        icon: <RiHome2Line size={24}/>,
        iconClosed: <RiIcons.RiArrowDownSFill/>,
        iconOpened: <RiIcons.RiArrowUpSFill/>
    },
    {
        title: 'Twoje oceny',
        path: '/oceniajacy/oceny',
        icon: <RiIcons.RiArticleLine size={24}/>,
        iconClosed: <RiIcons.RiArrowDownSFill/>,
        iconOpened: <RiIcons.RiArrowUpSFill/>
    },
    {
        title: 'Materiały',
        path: '/oceniajacy/materialy',
        icon: <RiIcons.RiFileCloudLine size={24}/>,
        iconClosed: <RiIcons.RiArrowDownSFill/>,
        iconOpened: <RiIcons.RiArrowUpSFill/>
    },
    {
        title: 'Ważne strony',
        icon: <RiIcons.RiExternalLinkFill size={24}/>,
        iconClosed: <RiIcons.RiArrowDownSFill/>,
        iconOpened: <RiIcons.RiArrowUpSFill/>,

        subNav: [
            {
                title: 'Strona projektu',
                a: 'https://efs-stypendia.slaskie.pl/content/o_projekcie',
                icon: <RiIcons.RiExternalLinkFill size={20}/>
            }
            , {
                title: 'Strona RPO Śląskie',
                a: 'https://rpo.slaskie.pl/',
                icon: <RiIcons.RiExternalLinkFill size={20}/>
            }
            , {
                title: 'Strona Województwa',
                a: 'https://slaskie.pl/',
                icon: <RiIcons.RiExternalLinkFill size={20}/>
            },
            //  {
            //     title: 'Strona RPO Śląskie',
            //     path: '/oceniajacy/linki/2',
            //     icon: <RiIcons.RiExternalLinkFill/>
            // },
        ]
    }
]
