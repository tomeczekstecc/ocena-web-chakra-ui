import React, {useState} from 'react'
import {Link, useLocation} from 'react-router-dom'
// @ts-ignore
import styled from 'styled-components'
import {Flex, useColorMode,} from '@chakra-ui/react'


// @ts-ignore
const SubMenu = ({item}) => {
    const {colorMode} = useColorMode()
    const path = useLocation()

    const SidebarLink = styled(Link)`
      display: flex;

      font-size: 0.85rem;
      justify-content: space-between;
      align-items: center;
      padding-left: 1rem;
      padding-right: 4rem;
      margin-bottom: .5rem;
      list-style: none;
      height: 3rem;
      text-decoration: none;
      position: relative;

      &:hover {
        background: ${(props: any) =>
                props.colormode === 'dark' ? '#1A202C' : '#CBD5E0'};
        cursor: pointer;

        &::before {
          content: '';
          position: absolute;
          height: 100%;
          left: 0;
          border-left: 3px solid ${(props: any) =>
                  props.colormode !== 'dark' ? '#1A202C' : '#CBD5E0'};
        }
      }
    `

    const SidebarLabel = styled.span`
      margin-left: 1rem;
    `

    const DropdownLink = styled(Link)`
      height: 2.75rem;
      padding-left: 2rem;
      display: flex;
      align-items: center;
      text-decoration: none;
      font-size: 0.8rem;
      position: relative;

      &:hover {
        background: ${(props: any) =>
                props.colorMode === 'dark' ? '#1A202C' : '#CBD5E0'};
        cursor: pointer;

        &::before {
          content: '';
          position: absolute;
          height: 100%;
          left: 0;
          border-left: 2px solid ${(props: any) =>
                  props.colorMode !== 'dark' ? '#1A202C' : '#CBD5E0'};
        }
      }
    `
    const DropdownLinkA = styled.a`
      height: 2.75rem;
      padding-left: 2rem;
      display: flex;
      align-items: center;
      text-decoration: none;
      font-size: 0.9rem;
      position: relative;

      &:hover {
        background: ${(props: any) =>
                props.colorMode === 'dark' ? '#1A202C' : '#CBD5E0'};
        cursor: pointer;

        &::before {
          content: '';
          position: absolute;
          height: 100%;
          left: 0;
          border-left: 2px solid ${(props: any) =>
                  props.colorMode !== 'dark' ? '#1A202C' : '#CBD5E0'};
        }
      }
    `

    const [subnav, setSubnav] = useState(false)

    const showSubnav = () => setSubnav(!subnav)

    // @ts-ignore
    return (
        <>
            <SidebarLink
                // @ts-ignore
                colormode={colorMode}
                to={item.path ? item.path : path?.pathname}
                onClick={item.subNav && showSubnav}
            >
                <Flex alignItems={'center'}>
                    {item.icon}
                    <SidebarLabel>{item.title}</SidebarLabel>
                </Flex>
                <div>
                    {item.subNav && subnav
                        ? item.iconOpened
                        : item.subNav
                            ? item.iconClosed
                            : null}
                </div>
            </SidebarLink>
            {subnav &&
                item.subNav.map(
                    (
                        item: {
                            a: any;
                            path: any; icon: any; title: any
                        },
                        index: React.Key | null | undefined
                    ) => {
                        if (item.a) {

                            return <DropdownLinkA
                                // @ts-ignore
                                colorMode={colorMode} href={item.a} target="_blank">  {item.icon}
                                <SidebarLabel>{item.title}</SidebarLabel></DropdownLinkA>


                        } else {
                            return (
                                <DropdownLink
                                    // @ts-ignore
                                    colorMode={colorMode}
                                    to={item.path}
                                    key={index}
                                >
                                    {item.icon}
                                    <SidebarLabel>{item.title}</SidebarLabel>
                                </DropdownLink>
                            )
                        }

                    }
                )}
        </>
    )
}

export default SubMenu
