import React from 'react';
import {SidebarDataOceniajacy} from './SidebarData';
import SubMenu from './SubMenu';
// import { IconContext } from 'react-icons/lib';
import {Box, useColorMode} from "@chakra-ui/react";
import {Logo} from "../Logo";


export const SidebarOceniajacy = () => {

    const mode = useColorMode()

    return (
        <Box flex={1}
             bg={mode.colorMode === 'dark' ? "gray.700" : "gray.100"}>
            <Logo name='OCENY'/>
            {SidebarDataOceniajacy.map((item: any, index: React.Key | null | undefined) => {
                return <SubMenu item={item} key={index}/>;
            })}

            {/*</IconContext.Provider>*/}
        </Box>
    );
};

