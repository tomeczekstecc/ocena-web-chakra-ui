// noinspection ES6CheckImport

import React, {useEffect, useMemo, useState} from "react";
import {
    useTable,
    useSortBy,
    useGlobalFilter,
    useFilters,
    usePagination,
// @ts-ignore
} from "react-table";
import {
    Button,
    Table,
    ButtonGroup,
    Select,
    Input,
    Thead,
    Tr,
    Th,
    Tbody,
    Td,
    Flex,
    Box,
    As,
    OmitCommonProps,
    TableRowProps
} from "@chakra-ui/react";
import PropTypes from "prop-types";
import GlobalFilter from "./GlobalFilter";
import ColumnFilter from "./ColumnFilter";
// import Loader from "../Loader";
import {
    BsChevronDoubleLeft,
    BsChevronDoubleRight,
    BsChevronLeft,
    BsChevronRight, TiArrowSortedDown,
    TiArrowSortedUp, TiArrowUnsorted
} from "react-icons/all";
import {HeaderTitle} from "../HeaderTitle";

const SimpleTable = (props: any) => {

    const styles = {
        header: {
            textAlign: 'left',
        },
        tr: {},
        td: {
            lineHeight: 1,
            borderTop: '1px white',
            paddingBottom: props.inModal ? '.75rem' : '1.4rem',
            paddingTop: props.inModal ? '.75rem' : '1.4rem',
        },
        pageNumber: {width: "50px", textAlign: "center"}
    }


    const data = useMemo(() => props.data || [], [props.data]);
    const columns = useMemo(() => props.columns, [props.columns]);
    const defaultColumn = useMemo(() => {
        return {
            Filter: ColumnFilter,
        };
    }, []);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        // rows,
        page,
        nextPage,
        previousPage,
        canNextPage,
        pageOptions,
        gotoPage,
        pageCount,
        state,
        setPageSize,
        canPreviousPage,
        prepareRow,
        setGlobalFilter,
    } = useTable(
        {
            columns,
            data: data || [],
            defaultColumn,
            initialState: props.tableInitState,
            autoResetFilters: false,
            autoResetPage: false,
            autoResetGlobalFilter: false,
            autoResetSortBy: false

        },
        useFilters,
        useGlobalFilter,
        useSortBy,
        usePagination,
        // useFlexLayout
    );


    const {globalFilter, pageIndex, pageSize} = state;
    const [pageNumber, setPageNumber] = useState(1)


    useEffect(() => {
        gotoPage(pageNumber - 1)
        // eslint-disable-next-line
    }, [pageNumber])


    // @ts-ignore
    return (
        <>
            <div>
                {props.modals && props.modals(props.reload)}
                <div
                    className={
                        "d-flex justify-content-between align-items-center flex-row-reverse mb-2"
                    }
                >
                </div>
                {/*@ts-ignore*/}
                {!props.noHeader ? <HeaderTitle size={props.titleSize} text={props.tableTitle} icon={props.headerIcon}>
                    <Flex alignItems={'center'}
                          justifyContent={'space-between'} gap={2}>
                        <Box>{props.topAction && props.topAction(props.reload)}</Box>
                        <Box className={"global-filter"}>
                            {data.length > 0 && props.globalFilter && (
                                <GlobalFilter filter={globalFilter}
                                              setFilter={setGlobalFilter}/>
                            )}
                        </Box>
                    </Flex>
                </HeaderTitle> : <Box className={"global-filter"}>
                    {data.length > 0 && props.globalFilter && (
                        <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter}/>
                    )}
                </Box>}
                {data.length > 0 ? (
                    <Table  {...getTableProps()}>
                        <Thead>
                            {headerGroups.map((headerGroup: { getHeaderGroupProps: () => JSX.IntrinsicAttributes & OmitCommonProps<React.DetailedHTMLProps<React.HTMLAttributes<HTMLTableRowElement>, HTMLTableRowElement>, keyof TableRowProps> & TableRowProps & OmitCommonProps<any, keyof TableRowProps> & { as?: As<any> | undefined; }; headers: any[]; }, idx: React.Key | null | undefined) => (
                                //@ts-ignore
                                <Tr style={styles.header} key={idx} {...headerGroup.getHeaderGroupProps()}>
                                    {headerGroup.headers.map((column, idx) => (
                                        <Th
                                            key={idx}
                                            {...column.getHeaderProps(column.getSortByToggleProps())}
                                        >
                                            <Flex alignItems={'center'} gap={1}>
                                                {column.isSorted ?
                                                    column.isSortedDesc ?
                                                        <TiArrowSortedUp color={'#0d47a1'} size={14}/> :
                                                        <TiArrowSortedDown color={'#0d47a1'} size={14}/> :
                                                    <TiArrowUnsorted size={13}/>
                                                }
                                                {column.render("Header")}
                                            </Flex>

                                        </Th>
                                    ))}
                                    {props.addTh && props.addTh()}
                                </Tr>
                            ))}
                        </Thead>
                        <Tbody {...getTableBodyProps()}>
                            {page.map((row: { getRowProps: () => JSX.IntrinsicAttributes & OmitCommonProps<React.DetailedHTMLProps<React.HTMLAttributes<HTMLTableRowElement>, HTMLTableRowElement>, keyof TableRowProps> & TableRowProps & OmitCommonProps<any, keyof TableRowProps> & { as?: As<any> | undefined; }; cells: any[]; }, idx: React.Key | null | undefined) => {
                                prepareRow(row);
                                return (
                                    //@ts-ignore
                                    <Tr style={styles.row} key={idx} {...row.getRowProps()} >
                                        {row.cells.map((cell, idx) => {
                                            return (
                                                <Td style={styles.td} key={idx} {...cell.getCellProps()}>
                                                    {cell.render("Cell")}
                                                </Td>
                                            );
                                        })}
                                        {props.addTd &&
                                            props.addTd(row)}
                                    </Tr>
                                );
                            })}
                        </Tbody>
                    </Table>
                ) : (
                    <h6>Brak danych do wyświetlenia</h6>
                )}

                {props.pagination && data.length > 10 && (
                    <Flex my={2} justifyContent={'space-between'}>
                        <div>
                            <Select size={"sm"} value={pageSize}
                                    onChange={(e) => setPageSize(Number(e.target.value))}>
                                {[10, 20, 50, 100].map((o) => (
                                    <option key={o} value={o}>
                                        pokaż na stronie: {o}
                                    </option>
                                ))}
                            </Select>
                        </div>


                        <Flex alignItems={'center'} gap={2}>
                            <ButtonGroup size={'sm'} isAttached>
                                <Button
                                    title={'idź do pierwszej'}
                                    disabled={!canPreviousPage}
                                    onClick={() => {
                                        setPageNumber(1)
                                        gotoPage(0)
                                    }}>
                                    <BsChevronDoubleLeft/>
                                </Button>

                                <Button
                                    title={'idź do poprzedniej'} disabled={!canPreviousPage}
                                    onClick={() => {
                                        setPageNumber(pageNumber - 1)
                                        previousPage()
                                    }}>
                                    <BsChevronLeft/>
                                </Button>
                            </ButtonGroup>
                            <div>
                                <strong>
                                    {pageIndex + 1} z {pageOptions.length}{" "}
                                </strong>
                            </div>
                            <div>| idź do:</div>
                            <Input
                                size={'sm'}
                                //@ts-ignore
                                style={styles.pageNumber}
                                type={"number"}
                                value={pageNumber}
                                onChange={(e) => {

                                    if (+e.target.value >= pageCount) {
                                        setPageNumber(pageCount)
                                        return
                                    }
                                    if (+e.target.value <= 0) {
                                        setPageNumber(1)
                                        return
                                    }
                                    setPageNumber(+e.target.value)
                                }
                                }
                            />
                            <ButtonGroup size={'sm'} isAttached>
                                <Button
                                    disabled={!canNextPage} onClick={() => {
                                    setPageNumber(pageNumber + 1)
                                    nextPage()
                                }}>
                                    <BsChevronRight/>
                                </Button>
                                <Button
                                    disabled={!canNextPage} onClick={() => {
                                    gotoPage(pageCount - 1)
                                    setPageNumber(pageCount)
                                }}>
                                    <BsChevronDoubleRight/>
                                </Button></ButtonGroup>
                        </Flex>
                    </Flex>
                )}
            </div>
        </>);
};

SimpleTable.propTypes = {
    data: PropTypes.array.isRequired,
    columns: PropTypes.array.isRequired,
    pagination: PropTypes.bool,
    globalFilter: PropTypes.bool,
    columnFilter: PropTypes.bool,
    tableInitState: PropTypes.object,
    modals: PropTypes.func,
    addTh: PropTypes.func,
    addTd: PropTypes.func,
    reload: PropTypes.func,
    loading: PropTypes.bool,
    topAction: PropTypes.func,
    borders: PropTypes.bool,
    tableTitle: PropTypes.string,
    titleSize: PropTypes.string,
    headerIcon: PropTypes.any,
    stats: PropTypes.func,
    noHeader: PropTypes.bool,
    inModal: PropTypes.bool
};

export default SimpleTable;


