import React, {useEffect, useState} from 'react'

// @ts-ignore
import {useAsyncDebounce} from 'react-table'
import {Input, Select} from "@chakra-ui/react";
// import {InputU, Select} from "../inputs";


const ColumnFilter = (props: any) => {


    const {filterValue, setFilter} = props.column
    const [value, setValue] = useState(filterValue)

    const onChange = useAsyncDebounce((value: any) => {
        setFilter(value || undefined)
    }, 400)
    //@ wybór opcji dla selecta

    let options: any[] = []
    if (props.column.fieldType === 'checkbox') {
        options = [
            {value: 'Tak'},
            {value: 'Nie'},
        ]
    } else if (props.column.fieldType === 'select') {
        // @ts-ignore
        options = [...new Map(props.data?.map(i => ({value: i[props.column.id]})).map((a) => [a.value, a])).values()]
    }
    useEffect(() => {
        setFilter(value || undefined)
        // eslint-disable-next-line
    }, [props.data]);

    return (
        <>
            {(props.column.fieldType === 'checkbox' || props.column.fieldType === 'select') ?
                <div onClick={e => e.stopPropagation()}
                     className={'w-100'}>
                    <Select
                        placeholder={'Szukaj...'}
                        size={'sm'}
                        //@ts-ignore
                        options={options}
                        onChange={e => {
                            if (parseInt(e.target.value) === -1) {
                                setValue('')
                                setFilter(undefined)
                            } else {
                                setValue((e.target.value));
                                onChange(e.target.value)
                            }
                        }} value={value || ''}/>
                </div>
                :
                <div onClick={e => e.stopPropagation()}
                     className={'w-100'}>
                    <Input placeholder={'Szukaj...'}
                           size={'xs'}
                           onChange={e => {
                               setValue((e.target.value));
                               onChange(e.target.value)
                           }} value={value || ''}/>
                </div>
            }
        </>
    )

}

export default ColumnFilter
