import React, {useEffect, useRef, useState} from 'react'
//@ts-ignore
import {useAsyncDebounce} from 'react-table'
import {Input} from "@chakra-ui/react";


// @ts-ignore
const GlobalFilter = ({filter, setFilter}) => {
    const ref = useRef()

    const [value, setValue] = useState(filter)

    const onChange = useAsyncDebounce((value: any) => {
        setFilter(value || undefined)
    }, 400)

    useEffect(() => {
        //@ts-ignore
        ref.current.focus()
    }, [])
    return (
        //@ts-ignore
        <div><Input ref={ref} placeholder={'Szukaj w tabeli...'}
                    size={'sm'}
                    onChange={e => {
                        setValue((e.target.value));
                        onChange(e.target.value)
                    }} value={value || ''}/>
        </div>
    )

}

export default GlobalFilter
