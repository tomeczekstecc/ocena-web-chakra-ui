import React from "react";
import {Spinner} from "@chakra-ui/react";

export const Loader = () => {
    return (
        // @ts-ignore
        <div style={styles.spinner}>
            <Spinner
                // @ts-ignore
                thickness='5px'
                speed='0.65s'
                emptyColor='gray.200'
                color='teal.300'
                size='lg'
            /></div>
    )
}

const styles = {
// @ts-ignore
    spinner: {
        position: 'fixed',
        bottom: '1rem',
        right: '1rem',
        zIndex: '1000 !important',
    },
    overlay: {
        position: 'fixed',
        width: '100vw',
        height: '100vh',
        zIndex: 10000,

    }
}
