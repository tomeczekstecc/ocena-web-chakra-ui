import React from "react";
import {useKeycloak} from "@react-keycloak/web";
import PropTypes from "prop-types";
import {auth} from '../config'


const Authorized = (props: any) => {
    const {keycloak, initialized} = useKeycloak();


    const effectiveRoles = props.top
        ? keycloak?.tokenParsed?.user_type
        : keycloak?.tokenParsed?.resource_access?.[auth?.resource].roles;


    const hasAnyRole =
        initialized &&
        (!props?.roles ||
            effectiveRoles?.map((r: any) => props?.roles?.includes(r) || false).reduce((a: any, b: any) => a || b, false));
    return hasAnyRole ? props.children : props.forbidden ? <div>Nie masz dostępu do tego zasobu.</div> : <></>;
};

Authorized.propTypes = {
    roles: PropTypes.array.isRequired,
    forbidden: PropTypes.any,
    top: PropTypes.bool
};

export default Authorized;
