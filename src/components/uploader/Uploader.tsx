import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {useDropzone} from 'react-dropzone';
import PropTypes from "prop-types";
import {Box, Button, Text, IconButton, Td, Th, useToast} from "@chakra-ui/react";
import {titles} from "../../utils/messages/manager";
import {useSelector} from "react-redux";
import {columnsTable} from './tableColumns'
import SimpleTable from "../simpleTable/SimpleTable";
import {DeleteIcon} from "@chakra-ui/icons";
import {files} from "../../config";

const baseStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '20px',
    borderWidth: 2,
    borderRadius: 2,
    borderColor: '#bababa',
    borderStyle: 'dashed',
    transition: 'border .24s ease-in-out'
};

// const focusedStyle = {
//     borderColor: '#2196f3'
// };

const acceptStyle = {
    width: "100%",
    boxSizing: 'border-box',
    borderColor: 'green',
    borderWidth: '3px'
};

const rejectStyle = {
    width: "100%",
    boxSizing: 'border-box',
    borderColor: '#ff1744',
    borderWidth: '3px'
};


export const Uploader = (props: {
    meta: any;
    titleSize: any;
    uploadCb: (arg0: { variables: { file: any; meta: { folder: string } } }) => void;
}) => {
    const tableInitState = {sortBy: [{id: 'id', desc: false}], pageSize: 10}

    const toast = useToast()

    // @ts-ignore
    const userId = useSelector(store => store?.user?.info?.sub)


    //@ Wyświetl toastera gdy nieopuszczalne pliiki
    const onDropRejected = useCallback(
        (res) => {
            toast({
                title: titles.fail.limitExceeded,
                description: 'Dopuszczalna liczba plików to 5, maksymalna wielkość pliku to 4MB',
                status: 'error',
                duration: 8000,
                isClosable: true,
                position: 'top'
            })
        },
        // eslint-disable-next-line
        [],
    );


    //@ użyj Dropzone's hook
    const {
        acceptedFiles, getRootProps, getInputProps, open, isFocused,
        isDragAccept,
        isDragReject
    } = useDropzone({
        maxFiles: 5, onDropRejected,
        maxSize: files.maxSize,
        accept: files.mimeTypesAllowed,
        noClick: true,
    });


    const style = useMemo(() => ({
        ...baseStyle,
        // ...(isFocused ? focusedStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
    }), [
        isFocused,
        isDragAccept,
        isDragReject
    ]);

    const [filesToUplaod, setFilesToUpload] = useState<any[]>([])

    const remove = (path: any) => setFilesToUpload(prev => [...prev].filter(f => f.path !== path))

    //@ prześlij pliki na serwer
    const sendFiles = () => {
        // @ts-ignore
        filesToUplaod.map(f => {
            props.uploadCb({variables: {file: f, meta: {...props.meta, userId}}})
            return remove(f.path)
        })
    }


    const addTh = () => <Th p={0}>Usuń</Th>

    const addTd = (row: any) => {
        return <Td p={0}> <IconButton
            title={'usuń'}
            colorScheme={'red'}
            onClick={() => remove(row.original.path)}
            variant='ghost'
            aria-label='usuń'
            icon={<DeleteIcon/>}
        />
        </Td>
    }

    useEffect(() => {
        return setFilesToUpload(prev => [...prev, ...acceptedFiles])
    }, [acceptedFiles])

    return (
        <Box>
            <div {...getRootProps(
                /*@ts-ignore*/
                {style})}>
                <input {...getInputProps()} />
                <Text>Przeciągnij pliki na to pole lub kliknij "Dodaj pliki"</Text>
                <Button mt={2} variant={'solid'} colorScheme={'teal'} onClick={() => open()}>Dodaj pliki</Button>
            </div>
            <aside>
                {/*@ts-ignore*/}
                <SimpleTable
                    inModal
                    titleSize={props.titleSize}
                    tableTitle={'Pliki do przesłania'}
                    data={filesToUplaod}
                    columns={columnsTable} tableInitState={tableInitState}
                    addTh={addTh}
                    addTd={addTd}
                    pagination
                />

                {filesToUplaod.length > 0 && <Button mt={3} onClick={() => sendFiles()}>Prześlij</Button>}
            </aside>
        </Box>
    );
}

Uploader.propTypes = {
    uploadCb: PropTypes.func, // Funkcja upload
    meta: PropTypes.object,  // obiekt z metadanymi pliku
    titleSize: PropTypes.string,  // obiekt z metadanymi pliku
};
