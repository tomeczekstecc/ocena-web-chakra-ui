import {fileSizeConverter} from "../../utils/helpers/fileSizeConverter";

export const columnsTable = [
    {Header: 'Nazwa pliku', accessor: 'name'},
    {
        Header: 'Rozmiar', accessor: (d: any) => {
            return fileSizeConverter(d.size)

        }
    },
]
