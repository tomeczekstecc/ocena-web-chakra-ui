import React, {useEffect, useState} from 'react'
import {Top} from './Top'
import {Box, Container, Flex} from "@chakra-ui/react";
import {useKeycloak} from "@react-keycloak/web";
import {useDispatch} from "react-redux";
import {addUserInfo} from "../redux/user";
import PropTypes from "prop-types";


// @ts-ignore
export const Layout = (props) => {

    const dispatch = useDispatch()
    const [user, setUser] = useState({})
    const {keycloak} = useKeycloak()


    const getUserInfo = async () => {
        // @ts-ignore
        keycloak.loadUserInfo().then(() => {
            dispatch(addUserInfo(keycloak.userInfo))

            // @ts-ignore
            return setUser(keycloak.userInfo)
        })
    }


    useEffect(() => {
        getUserInfo()

        // eslint-disable-next-line
    }, [])

    return (
        <Flex h={'100vh'} alignContent={'flex-start'}>
            <Flex flex={2}>
            {props.sidebar}
            </Flex>
            <Flex flex={11} px={8} flexDir={'column'} flexGrow={'initial'} >
                    <Box mb={5}>
                        <Top logout={keycloak.logout} user={user}/>
                    </Box>
                    {props.children}
            </Flex>
        </Flex>
    )
}

Layout.propTypes = {
    sidebar: PropTypes.any
}
