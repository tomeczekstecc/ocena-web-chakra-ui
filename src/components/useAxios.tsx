import React from "react";
import {makeUseAxios} from "axios-hooks";
import axios from "axios";

import keycloak from "../keycloak";
import store from '../redux/store'
import {addToast} from "../redux/toaster";


export const Axios = (() => {


    const instance = axios.create({
        // baseURL: "http://localhost:4000/api/v1",
        baseURL: "https://stypendia-ocena-server-dev.tomaszstec.me/api/v1",
    });

    instance.interceptors.request.use((config) => {
        // @ts-ignore
        config.headers["Authorization"] = "Bearer " + keycloak.token;
        // @ts-ignore
        config.headers["X-Requested-With"] = "XMLHttpRequest";
        return config;
    });

    instance.interceptors.response.use(
        (response) => {

            return response;
        },
        (error) => {
            if (error?.response?.status === 404) return

            store.dispatch(
                addToast({
                    variant: "error",
                    text: `${error?.response?.statusText} (${error?.response?.status})`,
                })
            )
            if (error?.response?.status === 401) {
                keycloak.login();
            }
            return error;
        }
    );
    return instance;
})();

const useAxios = makeUseAxios({
    axios: Axios,
    cache: false,
});

export default useAxios;
