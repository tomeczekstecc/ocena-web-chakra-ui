import {Box, Flex, GridItem, Text, useColorMode} from "@chakra-ui/react";
import {NavLink} from "react-router-dom";
import {Link as ChackraLink} from "@chakra-ui/react";
import {ArrowForwardIcon} from "@chakra-ui/icons";
import React from "react";
// @ts-ignore
const ActionCard = ({title, description, icon, onClick, to}) => {
    const mode = useColorMode()


    return (
        // <Box
        //     onClick={onClick}
        //     cursor={'pointer'}
        //     _hover={{
        //         bg: 'gray.100',
        //         transform: 'scale(1.01)',
        //         transition: 'all 0.2s',
        //     }}
        //     p={4}
        //     rounded={'md'}
        //     bg={'white'}w
        //     boxShadow={'lg'}>
        //     <Flex alignItems={'center'}>
        //         <Box p={2} rounded={'full'} bg={'gray.100'}
        //                 _hover={{ bg: 'gray.200' }}>
        //             {icon}
        //         </Box>
        //         <Box pl={4}>
        //             <Heading fontSize={'xl'}>{title}</Heading>
        //             <Text mt={2} color={'gray.500'}>{description}</Text>
        //         </Box>
        //     </Flex>
        // </Box>

        <GridItem onClick={onClick} >
            <NavLink to={to || '/'}>
                <Box border={'1px'}
                     p={6}
                     py={10}
                     rounded={'md'}
                     _hover={{
                         bg:  mode.colorMode === 'light' ? 'gray.100' : 'gray.700',
                            transform: 'scale(1.05)',
                            transition: 'all 0.2s',
                     }}
                >
                    <Flex alignItems={'center'} gap={2}>
                        {icon}
                        <Flex flexDir={'column'}>
                            <Text fontSize={22} as={ChackraLink}>{title}<ArrowForwardIcon/> </Text>
                            <Text>{description}</Text>
                        </Flex>
                    </Flex>
                </Box>
            </NavLink>
        </GridItem>
    )
}


export default ActionCard
