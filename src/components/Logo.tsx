import React from "react";
import {Box, Flex, Text} from "@chakra-ui/react";
import PropTypes from "prop-types";

export const Logo = (props: any) => {
    return <Box
display={'flex'} alignItems={'center'} justifyContent={'center'}
gap={2}
        bg={'blue.400'} rounded={4} p={1} textAlign={'center'} letterSpacing={2} opacity={.9} m={2}
        fontSize={'20px'} fontWeight={'bold'}

    >
        <img width={'25px'} src="/logo-stypendia.png" alt="logo"/>
        <Text>  {props.name}</Text>
    </Box>
}


Logo.propTypes = {
    name: PropTypes.oneOf(["OCENY", "WNIOSKI", "UMOWY"]),
}
