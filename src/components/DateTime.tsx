import React, {forwardRef} from 'react'
import DatePicker from "react-datepicker";
import styled from 'styled-components'
import moment from 'moment'// Importy obce
import 'moment/locale/pl'
import "react-datepicker/dist/react-datepicker.css";
import {registerLocale, setDefaultLocale} from "react-datepicker";
import pl from 'date-fns/locale/pl';
import {Input} from "@chakra-ui/react";
import {SmallCloseIcon} from "@chakra-ui/icons";

moment().locale('pl')
setDefaultLocale('pl')
registerLocale('pl', pl)

export const DateTime = (props: { onChange: (date: Date | null, event: React.SyntheticEvent<any, Event> | undefined) => void; readOnly: boolean }) => {

    const icon = <SmallCloseIcon/>

    const ClBtn = styled.div`
      .cl-btn {
        button {
          background: green;
        }

        &::after {
          //@ts-ignore
          content: icon;
          font-size: 1.2rem;
          color: #b03838;
          background: none
        }
      }
    `

    // @ts-ignore
    const CustomInput = forwardRef(({value, onClick}, ref) =>
        // @ts-ignore
        <Input onChange={() => null} onClick={onClick} ref={ref} value={value}/>
    );
    // @ts-ignore

    return (
        <ClBtn>
            <DatePicker
                clearButtonClassName={'cl-btn'}
                isClearable={!props.readOnly}
                readOnly={props.readOnly}
                clearButtonTitle={'Wyczyść'}
                locale="pl"
                dateFormat={'yyyy-MM-dd'}
                // @ts-ignore
                selected={props.value && moment(props.value).toDate()}
                onChange={props.onChange}
                customInput={<CustomInput/>}
            /></ClBtn>
    );
}
