import {Box, Flex, Avatar, IconButton, Divider} from '@chakra-ui/react'
import React from 'react'
import {ColorModeSwitcher} from '../chackra/ColorModeSwitcher'
// import {useNavigate} from 'react-router-dom'
import {Nav} from './Nav'
import {IdleTimer} from "./IdleTimer";
import {RiLogoutBoxRLine} from "react-icons/ri";
import {Hr} from "react-bootstrap-icons";


// @ts-ignore
export const Top = (props) => {


    // const mode = useColorMode()w

    // const navigate = useNavigate()

    return (<>
        <Box py={2}
        >
            <Flex justify={'space-between'} alignItems={'center'}>
                <Nav/>
                <Flex flexDir={'row'} alignItems={'center'} gap={6}>
                    <Box>
                        <IconButton
                            size="sm"
                            fontSize="lg"
                            variant="ghost"
                            colorScheme={'gray'}
                            color={'gray'}
                            title={'zarządzaj kontem'}
                            onClick={() => props.logout()}
                            icon={<Avatar
                                colorScheme={'gray'}
                                mx={2}
                                size='xs'
                                name={props.user.name}
                            />
                            }
                            aria-label={'user'}/>
                        <IconButton
                            size="sm"
                            fontSize="lg"
                            variant="ghost"
                            color="current"
                            title={'wyloguj'}
                            onClick={() => props.logout()}
                            icon={<RiLogoutBoxRLine/>}
                            aria-label={'logout'}/>
                    </Box>

                    <Box>
                        <Flex justify={'space-between'} alignItems={'center'} gap={1} >
                        <div title={'sesja aktywna przez:'}>
                            <Box pb={'3px'} ml={2}><IdleTimer/></Box></div>
                        <ColorModeSwitcher justifySelf='flex-end'/>
                        </Flex>
                    </Box>
                </Flex>
            </Flex>
            <Divider mt={2} />
        </Box>
    </>)
}

