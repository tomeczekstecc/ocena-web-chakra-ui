import {
    background,
    Breadcrumb,
    BreadcrumbItem,
    BreadcrumbLink,
    Text,
} from '@chakra-ui/react'
import React from 'react'
import {Link, useLocation} from 'react-router-dom'

export const Nav = () => {
    const location = useLocation()
    const pathnames = location.pathname.split('/').filter((p) => p)
    return (
        <>
            <Breadcrumb fontSize={'sm'}>
                {pathnames.map((p, idx) => {
                    const routeTo = `${pathnames.slice(0, idx + 1).join('/')}`
                    const isLast = idx === pathnames.length - 1
                    if (p === 'manager') p = 'start'
                    return (
                        <BreadcrumbItem key={idx}>
                            {isLast ? (
                                <Text fontSize={'sm'}>{p}</Text>
                            ) : (
                                <BreadcrumbLink as={Link} to={routeTo}>
                                    {p}
                                </BreadcrumbLink>
                            )}
                        </BreadcrumbItem>
                    )
                })}
            </Breadcrumb>
        </>
    )
}
