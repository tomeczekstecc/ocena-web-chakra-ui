import React from "react";
import {Box, Flex, IconButton, Text} from "@chakra-ui/react";
import {downloadFile} from "../utils/services/downloadFile";
import {RiDownloadFill} from "react-icons/ri";
import {logToC} from "../utils/helpers";
import {IconContext} from "react-icons";
import {GrDocumentTxt} from "react-icons/gr";
import {AiFillFileExcel, AiFillFileImage, AiFillFilePpt, AiFillFileWord} from "react-icons/ai";

import {MdAttachFile} from "react-icons/md";
import {BsFillArchiveFill} from "react-icons/bs";
import {VscFilePdf} from "react-icons/vsc";

export const CardFile = (props: { file: { filePath: React.Key | null | undefined; fileName: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; }; }) => {

    const reg = /[^.]+$/
    // @ts-ignore
    const found = reg.exec(props.file.fileName);

    logToC(props.file)

    const renderIcon = (ext: string) => {
        switch (ext) {
            case 'pdf':
                return <VscFilePdf/>;
            case 'doc':
                return <AiFillFileWord/>;
            case 'docx':
                return <AiFillFileWord/>;
            case 'xls':
                return <AiFillFileExcel/>;
            case 'xlsx':
                return <AiFillFileExcel/>;
            case 'ppt':
                return <AiFillFilePpt/>;
            case 'pptx':
                return <AiFillFilePpt/>;
            case 'txt':
                return <GrDocumentTxt/>;
            case 'jpg':
                return <AiFillFileImage/>;
            case 'jpeg':
                return <AiFillFileImage/>;
            case 'png':
                return <AiFillFileImage/>;
            case 'gif':
                return <AiFillFileImage/>;
            case 'bmp':
                return <AiFillFileImage/>;

            case 'zip':
                return <BsFillArchiveFill/>;
            case 'rar':
                return <BsFillArchiveFill/>;
            case '7z':
                return <BsFillArchiveFill/>;

            default:
                return <MdAttachFile/>;
        }
    }

    return <Box
        p={4}
        border={'1px solid'}
        borderRadius={3}
        width={'12rem'}
        height={'16rem'}
        key={props.file.filePath}>
        <IconContext.Provider value={{color: 'white', size: '30', style: {verticalAlign: 'middle', color: 'white'}}}>
            <Flex flexDir={'column'} h={"100%"} justifyContent={'space-between'}>
                <Box gap={1}>
                    <Box d={'inline-flex'}>{renderIcon(found![0])}</Box>
                    <Text wordBreak={'break-word'} fontWeight={'bold'}>{props.file.fileName}</Text>
                </Box>
                <IconButton
                    size={'lg'}
                    title={'pobierz'}
                    // @ts-ignore
                    onClick={() => downloadFile(props.file.name, props.file.fileName, 'resources')}
                    variant='ghost'
                    aria-label='pobierz'
                    icon={<RiDownloadFill size={20}/>}
                />
            </Flex>
        </IconContext.Provider>

    </Box>

}
