import React from 'react'
import {Avatar, Box, Flex, HStack, Tag, Text} from "@chakra-ui/react";
import {HeaderTitle} from "./HeaderTitle";

// @ts-ignore
export const OceniajacyDetails = ({user, filtered}) => {

    return <Box width={'50%'}><Box border={'1px'} p={4} borderRadius={4} mb={6}>
        <Flex wrap={'wrap'} justifyContent={'space-between'}>
            <Box>
                <HStack>
                    <Avatar
                        size={'sm'}
                        name={`${user?.name}`}/>
                    <HeaderTitle borderless size={'md'} text={`Szczegóły oceniającego`}/>
                </HStack>

                <Box mt={5}>
                    <Box fontSize={'lg'}>
                        <Text
                            d={'inline'}
                            fontWeight={'bold'}>Imię:
                        </Text> {user?.given_name || ''}
                    </Box>
                    <Box mt={2} fontSize={'lg'}>
                        <Text
                            d={'inline'}
                            fontWeight={'bold'}>Nazwisko:</Text> {user?.family_name || ''}
                    </Box>
                    <Box mt={2} fontSize={'lg'}>
                        <Text
                            d={'inline'}
                            fontWeight={'bold'}>Email: </Text>{user?.email || ''}
                    </Box>
                </Box>
            </Box>
            <Box>
                {/*// @ts-ignore*/}
                <Box fontSize={'lg'}><Tag>Wszystkie oceny: {filtered?.wszystkie?.length}</Tag>
                </Box>
                <Box mt={2} fontSize={'lg'}>
                    {/*// @ts-ignore*/}
                    <Tag colorScheme={'green'}>Zatwierdzone: {filtered?.zatwierdzone?.length}</Tag>
                </Box>
                <Box mt={2} fontSize={'lg'}>
                    <Tag colorScheme={'purple'}>W
                        {/*// @ts-ignore*/}

                        {' '} edycji: {filtered?.wEdycji?.length}</Tag>
                </Box>
                <Box mt={2} fontSize={'lg'}>
                    {/*// @ts-ignore*/}
                    <Tag colorScheme={'cyan'}>Przesłane: {filtered?.przeslane?.length}</Tag>
                    <Text mt={2} fontSize={'lg'}>
                    </Text>
                    {/*// @ts-ignore*/}
                    <Tag colorScheme={'red'}>Zwrócone: {filtered?.zwrocone?.length}</Tag>
                </Box>
            </Box>
        </Flex>
    </Box>
    </Box>
}
