import React from 'react'
import {Center, Stack, Text} from "@chakra-ui/react";


export const LoadingComponent = () => {
    return (
        <Center h={'100vh'} flexDir={'row'} style={{position: 'relative'}}>
            <Stack alignItems={'center'}>
                <img width={'150px'} src="/react-keycloak-logo.png" alt="image"/>
                <Text style={{fontSize: "1.3rem", fontWeight: 300}} fontSize={'2xl'}>Weryfikujemy uprawnienia</Text>
            </Stack></Center>
    )
}
