import React from 'react'

import {useNavigate} from "react-router-dom";
import {Button, Center, Stack, Text} from "@chakra-ui/react";


export const NotFound = () => {
    const navigate = useNavigate()
    return <Center style={{height: '70vh'}}
    >
        <Stack alignItems={'center'}>
            <Text fontSize={'xl'}>Błąd 404 | nie znaleziono strony</Text>
            <Button variant={'link'} size={'lg'} onClick={() => navigate('/')}>Wróć na stronę startową</Button>
        </Stack>
    </Center>
}

