import React, {useEffect, useState} from 'react'
import {useIdleTimer} from 'react-idle-timer'
import {useKeycloak} from '@react-keycloak/web'
import {miliToMinSec} from "../utils/helpers/miliToMinSec";
import {Badge, Modal, ModalBody, ModalContent, ModalHeader, ModalOverlay, Text, Button, Flex} from "@chakra-ui/react";
import {app} from "../config/app";


export const IdleTimer = () => {
    const {keycloak} = useKeycloak()

    const [modalisOpen, setModalIsOpen] = useState(false)

    const {getRemainingTime, getLastActiveTime} = useIdleTimer({
        timeout: app.idleTime,
        onIdle: () => keycloak.logout(),
        onActive: () => setModalIsOpen(false),
        // onAction:()=> setModalIsOpen(false),
        debounce: 500,
        crossTab: true,
    })

    const [timeLeft, setTimeLeft] = useState('00:00')
    const [millisecondsLeft, setMillisecondsLeft] = useState(app.idleTime)


    useEffect(() => {
        const timerInterval = setInterval(
            () => {
                // @ts-ignore
                setMillisecondsLeft(getRemainingTime());
                setTimeLeft(miliToMinSec(getRemainingTime()))
            },
            1000
        )
        return () => {
            clearInterval(timerInterval)
        }
    }, [])

    useEffect(() => {
        // @ts-ignore
        if (millisecondsLeft < app.idleTimePopUp) setModalIsOpen(true)
    }, [millisecondsLeft])

    return <div>
        <Modal
            // @ts-ignore
            isOpen={millisecondsLeft < app.idleTimePopUp}
            onClose={() => null}
        >
            <ModalOverlay/>
            <ModalContent>
                <ModalHeader>Przedłuż sesję</ModalHeader>
                <ModalBody>Z powodu braku aktywności niedługo Cię wylogujemy.
                    <Flex flexDir={'column'} alignItems={'center'} justifyContent={'center'}>
                        <Text fontWeight={'bold'} color={'red'} fontSize={'3xl'}>{timeLeft}</Text>

                        <Button m={4} size={'sm'} colorScheme='red'>
                            Przedłuż sesję

                        </Button>
                    </Flex>
                </ModalBody>


            </ModalContent>
        </Modal>
        <Badge variant='subtle' colorScheme={modalisOpen ? "red" : "gray"}>
            <Text fontSize={'xs'}>{timeLeft}</Text>

        </Badge>

    </div>
}
