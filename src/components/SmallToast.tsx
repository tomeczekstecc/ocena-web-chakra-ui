import React from 'react'
import {Tag} from "@chakra-ui/react";
import PropTypes from "prop-types";

export const SmallToast = (props: any) =>
    <Tag color={"white"} opacity={.75} py={1} px={2} bg={props.bg}>
        {props.text}
    </Tag>

SmallToast.defaultProps = {
    text: "Zapisano",
    bg: "green.500"
}

SmallToast.propTypes = {
    text: PropTypes.string,
    bg: PropTypes.string
}
