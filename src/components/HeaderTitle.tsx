import React from 'react'
import {Heading, Box, Flex, useColorMode} from "@chakra-ui/react";
import PropTypes from "prop-types";


export const HeaderTitle = (props: {
    borderless: boolean | string,
    size: string;
    icon: any;
    children: any;
    text: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined;
}) => {

    const mode = useColorMode()

    return <Box py={2} borderBottom={props.borderless ? 0 : '1px'} borderTop={props.borderless ? 0 : '1px'}
                borderColor={mode.colorMode === 'dark' ? 'whiteAlpha.400' : 'blackAlpha.400'}>
        <Flex justifyContent={'space-between'} alignItems={'center'}>
            <Heading size={props.size}>
                <Flex gap={2} alignItems={'center'}
                      justifyContent={'center'}> {props.icon} {props.text} </Flex></Heading>
            <Flex alignItems={'center'} gap={4}>{props.children}</Flex>

        </Flex>
    </Box>
}


HeaderTitle.defaultProps = {
    size: 'md'
}

HeaderTitle.propTypes = {
    text: PropTypes.string,
    size: PropTypes.string,
    children: PropTypes.any,
    icon: PropTypes.any,
    borderless: PropTypes.bool
}
