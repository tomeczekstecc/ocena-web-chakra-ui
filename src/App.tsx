import * as React from 'react'
import {Button, ChakraProvider} from '@chakra-ui/react'
import {useKeycloak} from '@react-keycloak/web'
import {adminRoutes, managerRoutes, oceniajacyRoutes} from './routes'
import {Layout} from './components'
import {Main} from './Main'
import {BrowserRouter, useNavigate} from 'react-router-dom'
import {Provider as ReduxProvider} from 'react-redux'
import store from './redux/store'
import theme from './chackra/ThemeExtender'
import {ToastContainer} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Authorized from "./components/Authorized";
import {SidebarAdmin} from "./components/sidebar/SidebarAdmin";
import {SidebarManager} from "./components/sidebar/SidebarManager";
import {SidebarOceniajacy} from "./components/sidebar/SidebarOceniajacy";

export const App = () => {
    const {keycloak, initialized} = useKeycloak()


    React.useEffect(() => {
        if (initialized && !keycloak.authenticated) {
            keycloak.login({
                // @ts-ignore
                redirectUri: window.location,
            })
        }
    }, [keycloak, keycloak.authenticated, initialized])

    if (!initialized) return <h4>Loading...</h4>
    return !!keycloak.authenticated ? (
        <ChakraProvider theme={theme}>

            <ReduxProvider store={store}>
                {/*// @ts-ignore*/}
                <Authorized top roles={["/manager"]}>
                    <BrowserRouter>
                        {/*// @ts-ignore*/}

                        <Layout sidebar={<SidebarManager/>}>
                            <ToastContainer/>
                            <Main routes={managerRoutes}/>
                        </Layout>

                    </BrowserRouter>
                </Authorized>

                {/*// @ts-ignore*/}
                <Authorized top roles={["/oceniajacy"]}>
                    <BrowserRouter>
                        {/*// @ts-ignore*/}

                        <Layout sidebar={<SidebarOceniajacy/>}>
                            <ToastContainer/>
                            <Main routes={oceniajacyRoutes}/>
                        </Layout>

                    </BrowserRouter>
                </Authorized>

                {/*// @ts-ignore*/}

                <Authorized top roles={["/admin"]}>
                    <BrowserRouter>
                        {/*// @ts-ignore*/}

                        <Layout sidebar={<SidebarAdmin/>}>
                            <ToastContainer/>
                            <Main routes={adminRoutes}/>
                        </Layout>

                    </BrowserRouter>
                </Authorized>
            </ReduxProvider>
        </ChakraProvider>
    ) : (
        <div>Oczekiwanie na autoryzację</div>
    )
}
