import React from 'react'
import {PageOceniajacyList} from '../pages/manager/PageOceniajacyList/PageOceniajacyList'
import {PageWnioskiList} from '../pages/manager/PageWnioskiList/PageWnioskiList'
import {PageOcenyList} from '../pages/manager/PageOcenyList/PageOcenyList'
import {PageOcena} from '../pages/oceniajacy/PageOcena/PageOcena' // specjalnie - nie dało się już przenieść łatwo komponentów
import {PageWniosek} from '../pages/manager/PageWniosek'
import {PageOceniajacy} from '../pages/manager/PageOceniajacy/PageOceniajacy'
import {PageUpload} from '../pages/manager/PageUpload/PageUpload'
import {PageHomeManager} from "../pages/manager/PageHomeManager";
import {PageAddWniosek} from "../pages/manager/PageAddWniosek/PageAddWniosek";

export const managerRoutes = [
    {
        path: '/manager',
        exact: true,
        forbidden: true,
        roles: ['manager'],
        view: <PageHomeManager/>,
    }, {
        path: '/',
        exact: true,
        forbidden: true,
        roles: ['manager'],
        view: <PageHomeManager/>,
    },
    {
        path: '/manager/oceniajacy',
        exact: true,
        forbidden: true,
        roles: ['manager'],
        view: <PageOceniajacyList/>,
    },
    {
        path: '/manager/oceniajacy/:oceniajacyId',
        exact: true,
        forbidden: true,
        roles: ['manager'],
        view: <PageOceniajacy/>,
    },
    {
        path: '/manager/wnioski',
        exact: true,
        forbidden: true,
        roles: ['manager'],
        view: <PageWnioskiList/>,
    },
    {
        path: '/manager/wnioski/:wniosekId',
        exact: true,
        forbidden: true,
        roles: ['manager'],
        view: <PageWniosek/>,
    },
    {
        path: '/manager/oceny',
        exact: true,
        forbidden: true,
        roles: ['manager'],
        view: <PageOcenyList/>,
    },
    {
        path: '/manager/oceny/:ocenaId',
        exact: true,
        forbidden: true,
        roles: ['manager'],
        view: <PageOcena/>,
    },

    {
        path: '/manager/upload',
        exact: true,
        forbidden: true,
        roles: ['manager'],
        view: <PageUpload/>,
    },
    {
        path: '/manager/dodaj',
        exact: true,
        forbidden: true,
        roles: ['manager'],
        view: <PageAddWniosek/>,
    },

]
