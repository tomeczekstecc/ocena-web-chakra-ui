import React from "react";
import {PageHomeOceniajacy} from "../pages/oceniajacy";
import {PageOceniajacy} from "../pages/oceniajacy/PageOceniajacy/PageOceniajacy";
import {PageOcena} from "../pages/oceniajacy/PageOcena/PageOcena";
import {PageMaterialy} from "../pages/oceniajacy/PageMaterialy/PageMaterialy";

export const oceniajacyRoutes = [
    {
        path: '/oceniajacy',
        exact: true,
        forbidden: true,
        roles: ['oceniajacy'],
        view: <PageHomeOceniajacy/>,
    },
    {
        path: '/',
        exact: true,
        forbidden: true,
        roles: ['oceniajacy'],
        view: <PageHomeOceniajacy/>,
    },
    {
        path: '/oceniajacy/oceny',
        exact: true,
        forbidden: true,
        roles: ['oceniajacy'],
        view: <PageOceniajacy/>,
    },
    {
        path: '/oceniajacy/oceny/:ocenaId',
        exact: true,
        forbidden: true,
        roles: ['oceniajacy'],
        view: <PageOcena/>,
    },
    {
        path: '/oceniajacy/materialy',
        exact: true,
        forbidden: true,
        roles: ['oceniajacy'],
        view: <PageMaterialy/>,
    },
]
