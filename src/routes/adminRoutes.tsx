import React from "react";
import {PageHomeAdmin} from "../pages/admin";

export const adminRoutes = [
    {
        path: '/',
        exact: true,
        forbidden: true,
        roles: ['admin'],
        view: <PageHomeAdmin/>,
    },
]
