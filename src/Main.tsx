import React from 'react'
import {Routes, Route} from 'react-router-dom'
import Authorized from "./components/Authorized";
import {NotFound} from "./components/NotFound";

export const Main = (props: any) => {

    return (
        <>
            <Routes>
                <Route path={'*'} element={<NotFound/>}/>
                {/*// @ts-ignore*/}
                {props.routes.map((r: any, i: number) => <Route key={i} path={r.path} element={<Authorized
                        roles={r.roles}
                        forbidden={r.forbidden}>
                        {r.view}
                    </Authorized>}>
                    </Route>
                )}
            </Routes>
        </>
    )
}
