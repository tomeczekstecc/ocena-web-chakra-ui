import {ColorModeScript} from '@chakra-ui/react'
import * as React from 'react'
import ReactDOM from 'react-dom'
import {App} from './App'
import reportWebVitals from './reportWebVitals'
import * as serviceWorker from './serviceWorker'
import {ReactKeycloakProvider} from '@react-keycloak/web'
import keycloak from './keycloak'
import {
    ApolloClient,
    InMemoryCache,
    ApolloProvider,
    // createHttpLink,
} from '@apollo/client'
import {setContext} from '@apollo/client/link/context'
import {createUploadLink} from 'apollo-upload-client'
import {ErrorBoundary} from 'react-error-boundary'
import {LoadingComponent} from "./components/LoadingComponent";

// @ts-ignore
export const ErrorFallback = ({error, resetErrorBoundary}) => (
    <div className="col-md-12" style={{zIndex: 100000}}>
        <div className="error-template">
            <h1>Oops!</h1>
            <div className="error-details">
                <pre>{error.message}</pre>
            </div>
            <div className="error-actions">
                <button style={{color: 'red', zIndex: 100000000}} onClick={resetErrorBoundary}>Try again</button>
                <button style={{color: 'red', zIndex: 100000000, cursor: 'pointer'}} onClick={resetErrorBoundary}>Try
                    again
                </button>
            </div>
        </div>
    </div>
);

const link = createUploadLink({
    uri: 'https://stypendia-ocena-server-dev.tomaszstec.me/graphql',
    // uri: 'http://localhost:4000/graphql',
    credentials: 'same-origin',
})
// const link = createHttpLink({
//     uri: 'http://localhost:4000/graphql',
//     credentials: 'same-origin',
// })
const authLink = setContext((_, {headers}) => {
    // get the authentication token from local storage if it exists
    const token = keycloak.token
    // return the headers to the context so httpLink can read them
    return {
        headers: {
            ...headers,
            authorization: token ? `Bearer ${token}` : '',
        },
    }
})

const client = new ApolloClient({
    uri: 'https://stypendia-ocena-server-dev.tomaszstec.me/graphql',
    // uri: 'https://localhost:4000/graphql',
    link: authLink.concat(link),
    cache: new InMemoryCache({
        addTypename: false,
    }),
})

ReactDOM.render(
    <ReactKeycloakProvider
        LoadingComponent={<LoadingComponent/>}
        authClient={keycloak}
        initOptions={{onLoad: 'login-required', checkLoginIframe: true}}
    >
        <ApolloProvider client={client}>
            <React.StrictMode>
                <ColorModeScript/>
                {/*// @ts-ignore*/}
                <ErrorBoundary

                    fallbackRender={ErrorFallback}
                >
                    <App/></ErrorBoundary>
            </React.StrictMode>
        </ApolloProvider>
    </ReactKeycloakProvider>,

    document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorker.unregister()

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
