import {useQuery, useLazyQuery, useMutation} from '@apollo/client'
import {
    Box,
    HStack,
    IconButton,
    Td,
    Th,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
    useDisclosure,
    Tabs,
    TabList,
    Tab,
    TabPanels,
    TabPanel,
    Tag,
    Text,
    ModalFooter,
    Button,
    Flex,
} from '@chakra-ui/react'

import {DeleteIcon, EditIcon} from "@chakra-ui/icons";
import React, {useEffect, useState} from 'react'
import {apollo} from '../../../config'
import {listOcenyQ, listWnioskiQ} from '../../../graphql/manager/queries'
import SimpleTable from "../../../components/simpleTable/SimpleTable";
import {columnsTableOceny} from "./tableColumns";
import {
    RiArrowLeftRightFill, RiCheckFill, RiCloseLine,
    RiEyeFill,
    RiFileExcel2Line, RiTodoFill, RiUserAddFill,
} from "react-icons/all";
import {downloadReport} from "../../../utils/services";
import {listOceniajacyQ} from "../../../graphql/manager/queries/listOceniającyQ";
import {deleteOcenaM, zmienOceniajacegoM} from "../../../graphql/manager/mutations";
import {useDispatch} from "react-redux";
import {addToast} from '../../../redux/toaster'
import {msg} from "../../../utils/messages/manager";
import {Loader} from "../../../components";
import {reportListOcenyQ} from "../../../graphql/manager/queries/reports";
import {columnsTableOceniajacy} from "../PageWnioskiList/tableColumns";
import {useNavigate} from "react-router-dom";
import {setMode} from "../../../redux/ocena";


export const PageOcenyList = () => {
    //@ wstępna konfiguracja react-table
    const tableInitState = {
        sortBy: [
            {
                id: 'id',
                desc: false
            }
        ]
        ,
        pageSize: 10
    }
    //@ chakra's hook do otwierania modala
    const {isOpen: isOpenDelete, onOpen: onOpenDelete, onClose: onCloseDelete} = useDisclosure()
    const {isOpen: isOpenSwap, onOpen: onOpenSwap, onClose: onCloseSwap} = useDisclosure()

    //@ dispatch reduxa
    const dispatch = useDispatch()

    //@ nawigacja react-router-dom
    const navigate = useNavigate()

    //@ lista ocen z serwera
    const {data: oceny, loading: loadingOceny} = useQuery(listOcenyQ, {
        pollInterval: apollo.pollInterval,
    })

    //@ pobieranie oceniających
    const [listOceniajacy, {data: oceniajacy, loading: loadingOceniajacy}] = useLazyQuery(listOceniajacyQ)


    //@ zmiana oceniających
    const [zmienOceniajacego, {loading: swapping}] = useMutation(zmienOceniajacegoM)


    //@ Lista tabsów
    const tabs: any[] = [
        {name: 'Wszystkie', data: oceny?.listOceny || [], scheme: null},
        {
            name: 'Zatwierdzone',
            data: oceny?.listOceny.filter((o: { status: { id: string } }) => o.status.id === 'ZATWIERDZONY') || [],
            scheme: 'green'
        },
        {
            name: 'W edycji',
            data: oceny?.listOceny.filter((o: { status: { id: string } }) => o.status.id === 'W_EDYCJI') || [],
            scheme: 'purple'
        },
        {
            name: 'Przesłane',
            data: oceny?.listOceny.filter((o: { status: { id: string } }) => o.status.id === 'PRZESLANY') || [],
            scheme: 'blue'
        },
        {
            name: 'Zwrócone',
            data: oceny?.listOceny.filter((o: { status: { id: string } }) => o.status.id === 'ZWROCONY') || [],
            scheme: 'red'
        },
    ]

    //@ pobieranie excela
    const [reportListOceny, {data, loading: loadingReport}] = useLazyQuery(reportListOcenyQ, {
        fetchPolicy: 'no-cache',
        nextFetchPolicy: "no-cache"
    })


    //@ stan lokalny
    const [ocena, setOcena] = useState()
    const [loading, setLoading] = useState(false)
    const [pending, setPending] = useState('')
    const [swapData, setSwapData] = useState({ocenaId: null, oceniajacyId: null})
    const [currOceniajacyId, setCurrOceniajacyId] = useState(undefined)

    //@ usuń ocenę
    const [deleteOcena, {loading: deleteing}] = useMutation(deleteOcenaM, {
        onCompleted: () => dispatch(addToast({text: msg.success.deletedOcena, variant: 'success'})),
        // @ts-ignore
        variables: {ocena_id: ocena?.id},
        refetchQueries: [listOcenyQ, listWnioskiQ, listOceniajacyQ]
    })


    //@ handler zmien oceniającego
    const handleZmien = (row: any) => {
        const {ocenaId, oceniajacyId} = swapData
        setLoading(true)
        zmienOceniajacego({
            variables: {
                oceniajacy_id: oceniajacyId!,
                ocena_id: +ocenaId!,
                oceniajacy_email: row.original.email,
                oceniajacy_first_name: row.original.first_name,
                oceniajacy_last_name: row.original.last_name,
            }
        })
            .then(() => dispatch(addToast({text: msg.success.swappedOceniajacy, variant: 'success'})))
            .then(() => setPending(''))
            .finally(() => {
                setLoading(false)
                onCloseSwap()
            })
        return
    }


    // @ otwórz modala delete
    const handleDelete = (row: any) => {
        setOcena(row.original)
        onOpenDelete()
    }


    // @ obsłuż przycisk edycji modala delete
    const handleZweryfikuj = (id: any) => {
        dispatch(setMode('verify'))
        navigate(`${id}`)
    }

    //@ handle zmien oceniajacego - przygotuj dane
    const handleZmienOceniajacego = (ocenaId: number, oceniajacyId: string) => {
        // @ts-ignore
        setCurrOceniajacyId(oceniajacyId)
        // @ts-ignore
        setSwapData(prev => ({...prev, ocenaId: +ocenaId}))
        return listOceniajacy().then(() => onOpenSwap());
    }

    //@ wybierz oceniajacego do zamiany
    const handleCklickOceniajacy = (oceniajacyId: string) => {
        setPending(oceniajacyId)
        // @ts-ignore
        setSwapData(prev => ({...prev, oceniajacyId}))
    }

    const handleDownloadReport = () => {
        reportListOceny()
            .then(() => dispatch(addToast({text: msg.success.reportDownloaded, variant: 'success'})))
    }


    //@ przygotuj modal
    const renderModals = () => {
        // @ts-ignore
        return <>
            <Modal size={'lg'} isOpen={isOpenDelete} onClose={() => {
                onCloseDelete()
            }}>
                <ModalOverlay/>
                <ModalContent>
                    <ModalHeader>Usuń ocenę</ModalHeader>
                    <ModalCloseButton/>
                    <ModalBody pb={6}>
                        {/*// @ts-ignore*/}
                        <Text> Zamierzasz usunąć ocenę wniosku {ocena?.wniosek_id} </Text>
                        <Text mt={5} fontSize={'20'}>Czy na pewno?</Text>
                    </ModalBody>

                    <ModalFooter>
                        <Button colorScheme='blue' mr={3} onClick={onCloseDelete}>
                            Rezygnuj
                        </Button>
                        <Button
                            // @ts-ignore
                            onClick={() => deleteOcena({variables: {ocena_id: +ocena.id!}}).then(() => onCloseDelete())
                            } variant='ghost' colorScheme={'red'}>Usuń ocenę</Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>

            <Modal size={'5xl'} isOpen={isOpenSwap} onClose={() => {
                onCloseSwap()
                return setPending('')
            }}>
                <ModalOverlay/>
                <ModalContent>
                    <ModalHeader>Przydziel nowego oceniającego do wniosku</ModalHeader>
                    <ModalCloseButton/>
                    <ModalBody pb={6}>
                        {/*//@ts-ignore*/}
                        <SimpleTable
                            addTh={addThOceniajacy}
                            addTd={addTdOceniajacy}
                            inModal
                            noHeader
                            pagination
                            globalFilter
                            tableInitState={tableInitState}
                            data={oceniajacy?.listOceniajacy.filter((o: { id: number }) => o.id !== currOceniajacyId) || []}
                            columns={columnsTableOceniajacy}
                        />
                    </ModalBody>
                </ModalContent>
            </Modal>


        </>
    }

    //@ Nagłowek dodatkowych kolumn
    function addThOceniajacy() {
        return <Th p={0}>
            Przydziel
        </Th>
    }

    //@ dodatkowe kolumny
    function addTdOceniajacy(row: any) {
        return <Td p={0} m={0}>
            {pending !== row.original.id ?
                <Box minW={75}>
                    <IconButton
                        title={'przydziel do oceniającego'}
                        variant='ghost'
                        onClick={() => handleCklickOceniajacy(row.original.id)}
                        aria-label='Przydziel'
                        icon={<RiUserAddFill/>}
                    /></Box>
                :
                <HStack minW={75}>
                    <IconButton
                        title={'potwierdź'}
                        size={'sm'}
                        variant={'outline'}
                        colorScheme={'green'}
                        onClick={() => handleZmien(row)}
                        aria-label='Potwierdź'
                        icon={<RiCheckFill/>}
                    />

                    <IconButton
                        title={'rezygnuj'}
                        size={'sm'}
                        variant={'outline'}
                        colorScheme={'red'}
                        onClick={() => setPending('')}
                        aria-label='Rezygnuj'
                        icon={<RiCloseLine/>}
                    />
                </HStack>}
        </Td>
    }


    //@ header dodatkowej kolumny
    const addThOcena = () => <Th p={0} m={0}>Operacje</Th>

    //@body dodatkowej kolumny
    const addTdOcena = (row: any) => {
        const status = row.original.status.id
        const perm = {
            show: {
                swap: status !== "ZATWIERDZONY" && status !== "PRZESLANY" && status !== "ZWROCONY",
                edit: status === "ZATWIERDZONY" || status === "PRZESLANY" || status === "ZWROCONY",
            }
        }
        return <Td p={0} m={0}>
            <HStack justifyContent={'flex-start'}>
                <IconButton
                    onClick={() => navigate(`${row.values.id}`)}
                    title={'zobacz'}
                    variant='ghost'
                    aria-label='podejrzyj wniosek'
                    icon={<RiEyeFill/>}
                />
                {perm.show.edit && <IconButton
                    onClick={() => handleZweryfikuj(`${row.values.id}`)}
                    title={'zweryfikuj ocenę'}
                    variant='ghost'
                    aria-label='podejrzyj wniosek'
                    icon={<EditIcon/>}
                />}
                {perm.show.swap && <IconButton
                    title={'zmień oceniającego'}
                    variant='ghost'
                    onClick={() => handleZmienOceniajacego(row.values.id, row.original.oceniajacy_id)}
                    aria-label='usuń'
                    icon={<RiArrowLeftRightFill/>}
                />}
                <IconButton
                    title={'usuń'}
                    variant='ghost'
                    colorScheme={'red'}
                    onClick={() => handleDelete(row)}
                    aria-label='usuń'
                    icon={<DeleteIcon/>}
                />
            </HStack>
        </Td>
    }


    //@ pobierz excela gdy dodstępne dane
    useEffect(() => {
        if (data) {
            downloadReport(data.reportListOceny.fileName)
        }
    }, [data])

    //@ pobierz excela gdy dodstępne dane
    useEffect(() => {
        //@ ustaw mode ocen
        dispatch(setMode('watch'))
        // eslint-disable-next-line
    }, [])

    return (
        <>
            {(loading || loadingReport || swapping || loadingOceniajacy || loadingOceny || deleteing) && <Loader/>}
            <Box>
                <Tabs isLazy colorScheme={'orange'}>
                    <TabList as={Flex} alignItems={'center'} justifyContent={'space-between'}>
                        <Flex flexDir={'row'}>
                            {tabs.map(t =>
                                <Tab key={t.name}>{t.name}
                                    <Tag colorScheme={t.scheme!}
                                         ml={2}>{t.data.length}
                                    </Tag>
                                </Tab>)}
                        </Flex>

                        <Box>
                            <IconButton
                                onClick={handleDownloadReport}
                                size={'sm'}
                                fontSize={16}
                                title={'eksport do excel'}
                                variant='outline'
                                colorScheme='teal'
                                aria-label='excel'
                                icon={<RiFileExcel2Line/>}
                            />
                        </Box>

                    </TabList>
                    <TabPanels>
                        {tabs.map(t =>
                            <TabPanel key={t.name} m={0} p={0}>
                                <SimpleTable
                                    headerIcon={<RiTodoFill/>}
                                    // topAction={topAction}
                                    addTh={addThOcena}
                                    addTd={addTdOcena}
                                    modals={renderModals}
                                    tableTitle={'Lista ocen'}
                                    pagination
                                    globalFilter
                                    tableInitState={tableInitState}
                                    data={t.data || []}
                                    columns={columnsTableOceny}
                                />
                            </TabPanel>)}
                    </TabPanels>
                </Tabs>
            </Box>
        </>
    )
}
