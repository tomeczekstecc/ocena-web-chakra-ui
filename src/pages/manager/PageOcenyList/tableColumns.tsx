export const columnsTableOceny = [
    {
        Header: 'ID', accessor: 'id',
    },
    {Header: 'Oceniaj.imię', accessor: 'oceniajacy_first_name'},
    {Header: 'Oceniaj.nazwisko', accessor: 'oceniajacy_last_name'},
    {Header: 'ID wniosku', accessor: 'wniosek_id'},
    {Header: 'Status oceny', accessor: 'status.nazwa'},


]
export const columnsTableOceniajacy = [
    {Header: 'Login', accessor: 'username'},
    {Header: 'Imie', accessor: 'first_name'},
    {
        Header: 'Nazwisko', accessor: 'last_name',
    },
]
