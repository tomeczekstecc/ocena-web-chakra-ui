export const columnsTableWnioski = [
    {Header: 'ID', accessor: 'id'},
    {Header: 'Numer wniosku', accessor: 'numer'},
    {Header: 'Imię wniosk.', accessor: 'wnioskodawca_imie'},
    {Header: 'Nazwisko wniosk.', accessor: 'wnioskodawca_nazwisko'},
    {Header: 'Status', accessor: 'status'},
    {Header: 'Imię ocen.', accessor: 'ocena.oceniajacy_first_name'},
    {Header: 'Nazwisko ocen.', accessor: 'ocena.oceniajacy_last_name'},
    {Header: 'Status oceny', accessor: 'ocena.status.nazwa'},

]


export const columnsTableOceniajacy = [
    {Header: 'Login', accessor: 'username'},
    {Header: 'Imie', accessor: 'first_name'},
    {Header: 'Nazwisko', accessor: 'last_name'}
]
