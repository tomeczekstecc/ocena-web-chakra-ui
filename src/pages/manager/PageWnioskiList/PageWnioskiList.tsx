import {useQuery, useLazyQuery, useMutation} from '@apollo/client'
import {
    Box, HStack,
    IconButton, Td, Th,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton, useDisclosure, Tabs, TabList, Tab, TabPanels, TabPanel, Tag, Flex,
} from '@chakra-ui/react'
import React, {useEffect, useState} from 'react'
import {
    RiCheckFill,
    RiCloseLine,
    RiEyeFill,
    RiFileExcel2Line, RiFileList2Fill,
    RiUserAddFill
} from "react-icons/all";

import {apollo} from '../../../config'
import {listOcenyQ, listWnioskiQ} from '../../../graphql/manager/queries'
import SimpleTable from "../../../components/simpleTable/SimpleTable";
import {columnsTableOceniajacy, columnsTableWnioski} from "./tableColumns";
import {reportListWnioskiQ} from "../../../graphql/manager/queries/reports";
import {downloadReport} from "../../../utils/services";
import {listOceniajacyQ} from "../../../graphql/manager/queries/listOceniającyQ";
import {addOcenaM} from "../../../graphql/manager/mutations";
import {useDispatch} from "react-redux";
import {addToast} from '../../../redux/toaster'
// import {useToast} from '@chakra-ui/react'
import {msg} from "../../../utils/messages/manager";
import {Loader} from "../../../components";
import {useNavigate} from "react-router-dom";

export const PageWnioskiList = () => {
    //@ wstępna konfiguracja react-table
    const tableInitState = {sortBy: [{id: 'id', desc: false}], pageSize: 10}


    //@ chakra's hook do otwierania modala
    const {isOpen, onOpen, onClose} = useDisclosure()

    //@ hook react-router-dom
    const navigate = useNavigate()

    //@ dispatch reduxa
    const dispatch = useDispatch()

    //@ chackra's toast hook
    // const toast = useToast()

    //@ lista wniosków z serwera
    const {data: wnioski, loading: loadingWnioski} = useQuery(listWnioskiQ, {
        pollInterval: apollo.pollInterval,
    })

    // Lista tabsów
    const tabs = [
        {name: 'Wszystkie', data: wnioski?.listWnioski || [], scheme: null},
        {
            name: 'Przydzielone',
            data: wnioski?.listWnioski.filter((w: { ocena: any }) => w.ocena) || [],
            scheme: 'green'
        },
        {
            name: 'Nieprzydzielone',
            data: wnioski?.listWnioski.filter((w: { ocena: any }) => !w.ocena) || [],
            scheme: 'red'
        },
    ]

    const stdErr = (text = undefined) => dispatch(
        addToast({
            variant: "error",
            text: text || msg.fail.std,
        }))

    //@ pobieranie excela
    const [reportListWnioski, {data, loading: loadingReport}] = useLazyQuery(reportListWnioskiQ, {
        fetchPolicy: 'no-cache',
        nextFetchPolicy: "no-cache",
        // @ts-ignore
        onError: () => stdErr(msg.fail.reportDownloaded),
        onCompleted: () => dispatch(addToast({text: msg.success.reportDownloaded, variant: 'success'}))
    })

    //@ pobieranie oceniających
    const [listOceniajacy, {data: oceniajacy, loading: loadingOceniajacy}] = useLazyQuery(listOceniajacyQ, {
        // @ts-ignore
        onError: stdErr,
    })

    //@ przydzielenie oceniającego
    const [przydziel] = useMutation(addOcenaM, {
        refetchQueries: [listWnioskiQ, listOcenyQ],
        // @ts-ignore

        onError: () => {
            onClose()
            stdErr()
        },
        onCompleted: () => {
            setPending('')
            setLoading(false)
            onClose()
            dispatch(addToast({text: msg.success.addedOcena, variant: 'success'}))
        }
    })

    //@ stan lokalny
    const [wniosekId, setWniosekId] = useState(null)
    const [loading, setLoading] = useState(false)
    const [pending, setPending] = useState('')

    //@ handler kliknij wniosek
    const handleClickWniosek = (id: React.SetStateAction<null>) => {
        setWniosekId(id)
        return listOceniajacy().then(() => onOpen());
    }

    //@ handler kliknij oceniającego i przydziel
    const handleClickOceniajacy = (row: any) => {
        setLoading(true)
        przydziel({
            variables: {
                oceniajacy_id: row.original.id,
                wniosek_id: +wniosekId!,
                punkty: Math.floor(Math.random() * 100),
                oceniajacy_email: row.original.email,
                oceniajacy_first_name: row.original.first_name,
                oceniajacy_last_name: row.original.last_name,
                oceniajacy_username: row.original.username,
            }
        })

        return listOceniajacy();
    }

    //@ przygotuj modal
    const renderModals = () => {

        return <Modal size={'5xl'} isOpen={isOpen} onClose={() => {
            onClose()
            return setPending('')
        }}>
            <ModalOverlay/>
            <ModalContent>
                <ModalHeader>Przydziel oceniającego</ModalHeader>
                <ModalCloseButton/>
                <ModalBody pb={6}>

                    {/*//@ts-ignore*/}
                    <SimpleTable
                        addTh={addThOceniajacy}
                        addTd={addTdOceniajacy}
                        inModal
                        noHeader
                        pagination
                        globalFilter
                        tableInitState={tableInitState}
                        data={oceniajacy?.listOceniajacy || []}
                        columns={columnsTableOceniajacy}
                    />
                </ModalBody>
            </ModalContent>
        </Modal>
    }

    //@ header dodatkowej kolumny
    const addThWnioski = () => <Th>Operacje</Th>

    function addThOceniajacy() {
        return <Th p={0}>
            Przydziel
        </Th>
    }

    //@body dodatkowej kolumny
    const addTdWnioski = (row: any) => {
        return <Td p={0} m={0}>
            <HStack justifyContent={'flex-end'}>
                {!row.original?.ocena ? <IconButton
                    title={'przydziel do oceniającego'}
                    variant='ghost'
                    onClick={() => handleClickWniosek(row.values.id)}
                    aria-label='Dodaj użytkownika'
                    icon={<RiUserAddFill/>}
                /> : null}
                <IconButton
                    title={'podejrzyj wniosek'}
                    variant='ghost'
                    aria-label='podejrzyj wniosek'
                    onClick={() => navigate(row.values.id)}
                    icon={<RiEyeFill/>}
                /></HStack>

        </Td>
    }

    function addTdOceniajacy(row: any) {
        return <Td p={0} m={0}>
            {pending !== row.original.id ?
                <Box minW={75}>
                    <IconButton
                        title={'przydziel do oceniającego'}
                        variant='ghost'
                        onClick={() => setPending(row.original.id)}
                        aria-label='Przydziel'
                        icon={<RiUserAddFill/>}
                    /></Box>
                :
                <HStack minW={75}>
                    <IconButton
                        title={'potwierdź'}
                        size={'sm'}
                        variant={'outline'}
                        colorScheme={'green'}
                        onClick={() => handleClickOceniajacy(row)}
                        aria-label='Potwierdź'
                        icon={<RiCheckFill/>}
                    />

                    <IconButton
                        title={'rezygnuj'}
                        size={'sm'}
                        variant={'outline'}
                        colorScheme={'red'}
                        onClick={() => setPending('')}
                        aria-label='Rezygnuj'
                        icon={<RiCloseLine/>}
                    />
                </HStack>}
        </Td>
    }

    //@ pobierz excela gdy dodstępne dane
    useEffect(() => {
        if (data) {
            downloadReport(data.reportListWnioski.fileName)
        }
    }, [data])

    return (
        <>
            {(loading || loadingOceniajacy || loadingReport || loadingWnioski) && <Loader/>}
            <Box>
                <Tabs isLazy colorScheme={'facebook'}>
                    <TabList as={Flex} alignItems={'center'} justifyContent={'space-between'}>
                        <Flex flexDir={'row'}>
                            {tabs.map(t =>
                                <Tab key={t.name}>{t.name}
                                    <Tag colorScheme={t.scheme!}
                                         ml={2}>{t.data.length}
                                    </Tag>
                                </Tab>)}
                        </Flex>

                        <Box>
                            <IconButton
                                onClick={() => reportListWnioski()}
                                size={'sm'}
                                fontSize={16}
                                title={'eksport do excel'}
                                variant='outline'
                                colorScheme='teal'
                                aria-label='excel'
                                icon={<RiFileExcel2Line/>}
                            />
                        </Box>

                    </TabList>
                    <TabPanels>
                        {tabs.map(t =>
                            <TabPanel key={t.name} m={0} p={0}>
                                <SimpleTable
                                    headerIcon={<RiFileList2Fill/>}
                                    // topAction={topActionWnioski}
                                    addTh={addThWnioski}
                                    addTd={addTdWnioski}
                                    modals={renderModals}
                                    tableTitle={'Lista wniosków'}
                                    pagination
                                    globalFilter
                                    tableInitState={tableInitState}
                                    data={t.data || []}
                                    columns={columnsTableWnioski}
                                />
                            </TabPanel>
                        )
                        }

                    </TabPanels>
                </Tabs>


            </Box>
        </>
    )
}
