export const columnsTableOceniajacy = [
    {Header: 'Oceniaj.imię', accessor: 'first_name'},
    {Header: 'Oceniaj.nazwisko', accessor: 'last_name'},
    {Header: 'Oceniaj.login', accessor: 'username'},
    {Header: 'E-mail', accessor: 'email'},
    {Header: 'Wnioski', accessor: 'count'},
]

export const columnsTableWnioski = [
    {Header: 'ID', accessor: 'id',},
    {Header: 'Imię wnioskodawcy', accessor: 'wnioskodawca_imie'},
    {Header: 'Nazwisko wnioskodawcy', accessor: 'wnioskodawca_nazwisko'},
    {Header: 'Imię ucznia', accessor: 'uczen.imie'},
    {Header: 'Nazwisko ucznia', accessor: 'uczen.nazwisko'},
]
