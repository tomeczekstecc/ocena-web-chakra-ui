import {useQuery, useLazyQuery, useMutation} from '@apollo/client'
import {
    Box,
    HStack,
    IconButton,
    Td,
    Th,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
    useDisclosure,
    Tabs,
    TabList,
    Tab,
    TabPanels,
    TabPanel,
    Tag,
    Flex,
} from '@chakra-ui/react'
import React, {useEffect, useState} from 'react'
import {listOcenyQ, listWnioskiNieprzydzieloneQ, listWnioskiQ} from '../../../graphql/manager/queries'
import SimpleTable from "../../../components/simpleTable/SimpleTable";
import {columnsTableWnioski, columnsTableOceniajacy} from "./tableColumns";
import {RiCheckFill, RiCloseLine, RiEyeFill, RiFileExcel2Line, RiMenuAddFill, RiTeamFill} from "react-icons/all";
import {downloadReport} from "../../../utils/services";
import {listOceniajacyQ} from "../../../graphql/manager/queries/listOceniającyQ";
import {addOcenaM} from "../../../graphql/manager/mutations";
import {useDispatch} from "react-redux";
import {addToast} from '../../../redux/toaster'
import {msg} from "../../../utils/messages/manager";
import {Loader} from "../../../components";
import {reportListOceniajacyQ} from "../../../graphql/manager/queries/reports";
import {apollo} from "../../../config";
import {useNavigate} from 'react-router-dom'


export const PageOceniajacyList = () => {
    //@ wstępna konfiguracja react-table
    const tableInitState = {
        sortBy: [{id: 'id', desc: false}], pageSize: 10
    }
    //@ chakra's hook do otwierania modala
    const {isOpen: isOpenWnioski, onOpen: onOpenWnioski, onClose: onCloseWnioski} = useDisclosure()

    //@ dispatch reduxa
    const dispatch = useDispatch()

    //@ hook nawigacji react-router
    // @ts-ignore
    const navigate = useNavigate()


    //@ lista oceniających z serwera
    const {data: oceniajacyList, loading: loadingOceniajacy} = useQuery(listOceniajacyQ, {
        pollInterval: apollo.pollInterval,
    })

    //@ pobieranie oceniających
    const [listWnioskiNieprzydzielne, {
        data: wnioski,
        loading: loadingWnioski
    }] = useLazyQuery(listWnioskiNieprzydzieloneQ)


    //@ przydzielanie wniosków
    const [przydzielWniosek, {loading: attaching}] = useMutation(addOcenaM,
        {refetchQueries: [listOcenyQ, listWnioskiQ, listOceniajacyQ]})


    //@ Lista tabsów
    const tabs = [
        {name: 'Wszystkie/cy', data: oceniajacyList?.listOceniajacy || [], scheme: null},
    ]

    //@ pobieranie excela
    const [reportListOceniajacy, {data, loading: loadingReport}] = useLazyQuery(reportListOceniajacyQ)


    //@ stan lokalny
    const [oceniajacy, setOceniajacy] = useState(null)
    const [wniosekId, setWniosekId] = useState(null)
    const [loading, setLoading] = useState(false)
    const [pending, setPending] = useState(null)


    //@ handler przydziel wniosek
    const handlePrzydziel = (row: any) => {
        setLoading(true)
        przydzielWniosek({
            variables: {
                // @ts-ignore
                oceniajacy_id: oceniajacy.id,
                wniosek_id: +wniosekId!,
                punkty: Math.floor(Math.random() * 100),
                // @ts-ignore
                oceniajacy_email: oceniajacy.email,
                // @ts-ignore
                oceniajacy_first_name: oceniajacy.first_name,
                // @ts-ignore
                oceniajacy_last_name: oceniajacy.last_name,
                // @ts-ignore
                oceniajacy_username: oceniajacy.username,
            }
        })
            .then(() => dispatch(addToast({text: msg.success.addedOcena, variant: 'success'})))
            .then(() => setPending(null))
            .finally(() => {
                setLoading(false)
                onCloseWnioski()
            })
        return
    }


    //@ handle przydziel wniosek - przygotuj dane
    const handleClickWniosek = (wniosekId: React.SetStateAction<null>) => {
        setWniosekId(wniosekId)
        setPending(wniosekId)
        // @ts-ignoreo
        // return listWnioski().then(() => onOpen());
    }

    //@ wybierz oceniajacego
    const handleClickOceniajacy = async (oceniajacy: React.SetStateAction<null>) => {
        setOceniajacy(oceniajacy)
        // noinspection ES6MissingAwait
        listWnioskiNieprzydzielne()
        await listWnioskiNieprzydzielne()
        onOpenWnioski()
    }


    //@ Pobierz raport i wygeneruj toasta
    const handleDownloadReport = () => {
        reportListOceniajacy()
            .then(() => dispatch(addToast({text: msg.success.reportDownloaded, variant: 'success'})))
    }

    //@ przygotuj modal`
    const renderModals = () =>
        <Modal size={'5xl'} isOpen={isOpenWnioski} onClose={() => {
            // @ts-ignore
            setPending(0)
            onCloseWnioski()
        }}>
            <ModalOverlay/>
            <ModalContent>
                <ModalHeader>Przydziel wniosek do oceniającego (wnioski nieprzydzielone)</ModalHeader>
                <ModalCloseButton/>
                <ModalBody pb={6}>
                    {/*//@ts-ignore*/}
                    <SimpleTable
                        addTh={addThWniosek}
                        addTd={addTdWniosek}
                        inModal
                        noHeader
                        pagination
                        globalFilter
                        tableInitState={tableInitState}
                        data={wnioski?.listWnioskiNieprzydzielone || []}
                        columns={columnsTableWnioski}
                    />
                </ModalBody>
            </ModalContent>
        </Modal>

    //@ Nagłowek dodatkowych kolumn
    function addThWniosek() {
        return <Th p={0}>
            Przydziel
        </Th>
    }

    //@ dodatkowe kolumny
    function addTdWniosek(row: any) {
        return <Td p={0} m={0}>
            {pending !== row.original.id ?
                <Box minW={75}>
                    <IconButton
                        title={'przydziel do oceniającego'}
                        variant='ghost'
                        onClick={() => handleClickWniosek(row.original.id)}
                        aria-label='Przydziel'
                        icon={<RiMenuAddFill/>}
                    /></Box>
                :
                <HStack minW={75}>
                    <IconButton
                        title={'potwierdź'}
                        size={'sm'}
                        variant={'outline'}
                        colorScheme={'green'}
                        onClick={() => handlePrzydziel(row)}
                        aria-label='Potwierdź'
                        icon={<RiCheckFill/>}
                    />

                    <IconButton
                        title={'rezygnuj'}
                        size={'sm'}
                        variant={'outline'}
                        colorScheme={'red'}
                        onClick={() => setPending(null)}
                        aria-label='Rezygnuj'
                        icon={<RiCloseLine/>}
                    />
                </HStack>}
        </Td>
    }

    //@ header dodatkowej kolumny
    const addThOceniajacy = () => <Th p={0} m={0}>Operacje</Th>

    //@body dodatkowej kolumny
    const addTdOceniajacy = (row: any) => {
        return <Td p={0} m={0}>
            <HStack justifyContent={'flex-end'}>
                <IconButton
                    onClick={() => navigate(`${row.original.id}`)}
                    title={'zobacz'}
                    variant='ghost'
                    aria-label='podejrzyj wniosek'
                    icon={<RiEyeFill/>}
                />
                <IconButton
                    title={'przydziel wniosek'}
                    variant='ghost'
                    onClick={() => handleClickOceniajacy(row.original)}
                    aria-label='usuń'
                    icon={<RiMenuAddFill/>}
                />

            </HStack>
        </Td>
    }


    //@ pobierz excela gdy dodstępne dane
    useEffect(() => {
        if (data) {
            downloadReport(data.reportListOceniajacy.fileName)
        }
    }, [data])

    return (
        <>
            {(loading || loadingReport || attaching || loadingOceniajacy || loadingOceniajacy || loadingWnioski) &&
                <Loader/>}
            <Box>
                <Tabs isLazy colorScheme={'purple'}>
                    <TabList as={Flex} alignItems={'center'} justifyContent={'space-between'}>
                        <Flex flexDir={'row'}>
                            {tabs.map(t =>
                                <Tab key={t.name}>{t.name}
                                    <Tag colorScheme={t.scheme!}
                                         ml={2}>{t.data.length}
                                    </Tag>
                                </Tab>)}
                        </Flex>

                        <Box>
                            <IconButton
                                onClick={handleDownloadReport}

                                size={'sm'}
                                fontSize={16}
                                title={'eksport do excel'}
                                variant='outline'
                                colorScheme='teal'
                                aria-label='excel'
                                icon={<RiFileExcel2Line/>}
                            />
                        </Box>

                    </TabList>
                    <TabPanels>
                        {tabs.map(t =>
                            <TabPanel key={t.name} m={0} p={0}>
                                <SimpleTable
                                    headerIcon={<RiTeamFill/>}
                                    // topAction={topAction}
                                    addTh={addThOceniajacy}
                                    addTd={addTdOceniajacy}
                                    modals={renderModals}
                                    tableTitle={'Lista oceniających'}
                                    pagination
                                    globalFilter
                                    tableInitState={tableInitState}
                                    data={t.data || []}
                                    columns={columnsTableOceniajacy}
                                />
                            </TabPanel>)}
                    </TabPanels>
                </Tabs>
            </Box>
        </>
    )
}
