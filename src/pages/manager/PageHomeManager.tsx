import React from 'react'
import {Stack, Text, Link as ChackraLink, Flex, Grid} from "@chakra-ui/react";
import {NavLink} from "react-router-dom";
import {ArrowForwardIcon} from "@chakra-ui/icons";
import * as RiIcons from "react-icons/ri";
import {useKeycloak} from "@react-keycloak/web";
import {RiFileList2Fill, RiTeamFill} from "react-icons/all";
import ActionCard                   from "../../components/ActionCard";

export const PageHomeManager = () => {
    const {keycloak} = useKeycloak()

    const cards = [
        { to: '/manager/wnioski', icon: <RiFileList2Fill size={60}/>, title: 'Wnioski', description: 'Przeglądaj wnioski stypendialne i przydzielaj oceniających' },
        { to: '/manager/oceny', icon: <RiIcons.RiArticleLine size={60}/>, title: 'Oceny', description: 'Zarządzaj ocenami, zatwierdzaj i odrzucaj przesłane oceny' },
        { to: '/manager/oceniajacy', icon: <RiTeamFill size={60}/>, title: 'Oceniający', description: 'Przeglądaj i zarządzaj ocenami wybranego użytkownika' },
        { to: '/manager/upload', icon: <RiIcons.RiFileCloudLine size={60}/>, title: 'Repozytorium', description: 'Udostępniaj oceniającym materiały pomocnicze' },
        { icon: <RiIcons.RiLogoutBoxRLine size={60}/>, title: 'Wyloguj się', description: 'Opuść aplikację i przejdź na stronę logowania', onClick: () => keycloak.logout()},
    ]

    return <Grid templateColumns='repeat(2, 1fr)' gap={6} >
    {/*// @ts-ignore*/}
        {cards.map((card, index) => <ActionCard key={index} {...card}/>)}
    </Grid>
}
