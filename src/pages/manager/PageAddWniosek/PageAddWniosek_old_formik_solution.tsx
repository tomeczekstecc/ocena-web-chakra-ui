import React from 'react'
import {
    Alert,
    AlertDescription,
    AlertIcon,
    AlertTitle, Button, ButtonGroup, Container, Flex,
    FormControl,
    FormErrorMessage,
    FormLabel,
    Input
} from "@chakra-ui/react";

import * as Yup from 'yup'
import {useFormik} from "formik";
import {logToC} from "../../../utils/helpers";

export const OldPageAddWniosek = () => {


    const initialValues = {
        pesel: '',
        imie: '',
        nazwisko: '',
    }

    const onSubmit = (values: any) => {
        logToC(values)
    }


    const validate = (values: any) => {

        let errors = {
            pesel: '',
            imie: '',
            nazwisko: '',
        }

        if (values.pesel.length < 2) {
            errors.pesel = 'Pole jest wymagane i wartość musi być poprawnym numerem PESEL'
        }
        if (values.imie.length < 2) {
            errors.imie = 'Pole jest wymagane i imię musi składać się co najmniej z 2 znaków'
        }

        if (values.nazwisko.length < 2) {
            errors.nazwisko = 'Pole jest wymagane i nazwisko musi składać się co najmniej z 2 znaków'
        }

        return errors
    }


    const validationSchema = Yup.object({
        pesel: Yup.string().min(2, 'wartość musi być poprawnym numerem PESEL').required('Pole jest wymagane'),
        imie: Yup.string().min(2, 'Wartość musi składać się co najmniej z 2 znaków').required('Pole jest wymagane'),
        nazwisko: Yup.string().min(2, 'Wartość musi składać się co najmniej z 2 znaków').required('Pole jest wymagane'),
    })


    // @ts-ignore
    const formik = useFormik({
            initialValues,
            onSubmit,
            // validate,
            validationSchema
        }
    )


    return <>
        <Alert status='info'>
            <AlertIcon/>
            <AlertTitle>Dodaj wniosek!</AlertTitle>
            <AlertDescription>W tym miejscu możesz dodać wniosek nie złożony poprzez aplikację. Pozostałe minimalne
                informacje wprowadzi oceniający. Oceniającego można przydzielić do wniosku z listy wniosków lub
                oceniających.</AlertDescription>
        </Alert>

        <form onSubmit={formik.handleSubmit}>
            <Container mt={5} as={Flex} direction={'column'} gap={5}>

                <FormControl isInvalid={!!formik.errors.pesel && formik.touched.pesel}>
                    <FormLabel htmlFor='pesel'>PESEL ucznia</FormLabel>
                    <Input
                        onBlur={formik.handleBlur}
                        type={'password'} id='pesel' name={'pesel'} placeholder='pesel' onChange={formik.handleChange}
                        value={formik.values.pesel}/>
                    <FormErrorMessage>{formik.errors.pesel}</FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={!!formik.errors.imie && formik.touched.imie}>
                    <FormLabel htmlFor='imie'>Imię wnioskodawcy</FormLabel>
                    <Input
                        onBlur={formik.handleBlur}
                        id='imie' name={'imie'} placeholder='imie' onChange={formik.handleChange}
                        value={formik.values.imie}/>
                    <FormErrorMessage>{formik.errors.imie}</FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={!!formik.errors.nazwisko && formik.touched.nazwisko}>
                    <FormLabel htmlFor='nazwisko'>Nazwisko wnioskodawcy</FormLabel>
                    <Input
                        onBlur={formik.handleBlur}
                        id='nazwisko' name={'nazwisko'} placeholder='nazwisko' onChange={formik.handleChange}
                        value={formik.values.nazwisko}/>
                    <FormErrorMessage>{formik.errors.nazwisko}</FormErrorMessage>
                </FormControl>

                <ButtonGroup>
                    <Button
                        // @ts-ignore
                        type={'submit'} variant={'outline'} alignSelf={'flex-start'}
                        colorScheme={'green'}>Dodaj</Button>
                    <Button variant={'outline'} alignSelf={'flex-start'} colorScheme={'orange'}>Anuluj</Button>
                </ButtonGroup>
            </Container>
        </form>
    </>
}
