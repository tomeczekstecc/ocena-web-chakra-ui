import React, {useState} from 'react'
import {
    Alert,
    AlertDescription,
    AlertIcon,
    AlertTitle, Button, ButtonGroup, Container, Flex,
    FormControl,
    FormErrorMessage,
    FormLabel,
    Input
} from "@chakra-ui/react";
import {useDispatch} from "react-redux";
import {Formik, Form, Field} from "formik";
import {useMutation} from "@apollo/client";
import {useNavigate} from "react-router-dom";

import {addWniosekPozasystM} from "../../../graphql/manager/mutations";
import {addToast} from "../../../redux/toaster";
import {msg} from "../../../utils/messages/manager";
import {dodajWniosekPozasystemowy} from "../../../utils/validationSchema";

export const PageAddWniosek = () => {

    const dispatch = useDispatch()
    const navigate = useNavigate()

    const [serverErrors, setServerErrors] = useState({pesel: '', wnioskodawca_imie: '', wnioskodawca_nazwisko: ''})

    const initialValues = {
        pesel: '',
        numer: '',
        wnioskodawca_imie: '',
        wnioskodawca_nazwisko: '',
    }

    const onCompleted = () => {
        dispatch(addToast({text: msg.success.savedWniosekPozasyst, variant: 'success'}))
        navigate(-1)
    }

    const onError = (err: any) => {
        return setServerErrors(err.graphQLErrors[0].extensions.c_errors)
    }

    const [addWniosekPozasyst] = useMutation(addWniosekPozasystM, {onError, onCompleted})


    const onSubmit = (values: any) => {
        addWniosekPozasyst({
            variables: {
                wniosekPozasystInput: {
                    ...values
                }
            }
        })
    }


    // @ts-ignore
    // @ts-ignore
    return <>
        <Alert status='info'>
            <AlertIcon/>
            <AlertTitle>Dodaj wniosek!</AlertTitle>
            <AlertDescription>W tym miejscu możesz dodać wniosek nie złożony poprzez aplikację. Pozostałe minimalne
                informacje wprowadzi oceniający. Oceniającego można przydzielić do wniosku z listy wniosków lub
                oceniających.</AlertDescription>
        </Alert>
        <Formik initialValues={initialValues}
                validationSchema={dodajWniosekPozasystemowy}
                onSubmit={onSubmit}>
            <Form>
                <Container mt={5} as={Flex} direction={'column'} gap={5}>

                    <Field name={'numer'}>
                        {/*// @ts-ignore*/}
                        {({form, field}) => <FormControl
                            // @ts-ignore
                            isInvalid={(!!form.errors.numer && form.touched.numer) || serverErrors.numer}>
                            <FormLabel htmlFor='numer'>Numer wniosku</FormLabel>
                            <Input
                                // @ts-ignore
                                onClick={() => setServerErrors({numer: ''})}
                                {...field}
                                id='numer' placeholder='numer'
                            />
                            {/*// @ts-ignore*/}
                            <FormErrorMessage>{form.errors.numer || serverErrors.numer}</FormErrorMessage>
                        </FormControl>}
                    </Field>
                    <Field name={'pesel'}>
                        {/*// @ts-ignore*/}
                        {({form, field}) => <FormControl
                            isInvalid={(!!form.errors.pesel && form.touched.pesel) || serverErrors.pesel}>
                            <FormLabel htmlFor='pesel'>PESEL ucznia</FormLabel>
                            <Input
                                // @ts-ignore
                                onClick={() => setServerErrors({pesel: ''})}
                                {...field}
                                type={'password'} id='pesel' placeholder='pesel'
                            />
                            {/*// @ts-ignore*/}
                            <FormErrorMessage>{form.errors.pesel || serverErrors.pesel}</FormErrorMessage>
                        </FormControl>}
                    </Field>

                    <Field name={'wnioskodawca_imie'}>
                        {/*// @ts-ignore*/}
                        {({form, field}) => <FormControl
                            isInvalid={(!!form.errors.wnioskodawca_imie && form.touched.wnioskodawca_imie) || serverErrors.wnioskodawca_imie}>
                            <FormLabel htmlFor='wnioskodawca_imie'>Imię wnioskodawcy</FormLabel>
                            <Input
                                // @ts-ignore
                                onClick={() => setServerErrors({wnioskodawca_imie: ''})}
                                {...field}
                                id='wnioskodawca_imie' placeholder='wnioskodawca_imie'/>
                            <FormErrorMessage>{form.errors.wnioskodawca_imie || serverErrors.wnioskodawca_imie}</FormErrorMessage>
                        </FormControl>}
                    </Field>

                    <Field name={'wnioskodawca_nazwisko'}>
                        {/*// @ts-ignore*/}
                        {({form, field}) => <FormControl
                            isInvalid={(!!form.errors.wnioskodawca_nazwisko && form.touched.wnioskodawca_nazwisko) || serverErrors.wnioskodawca_nazwisko}>
                            <FormLabel htmlFor='wnioskodawca_nazwisko'>Nazwisko wnioskodawcy</FormLabel>
                            <Input
                                // @ts-ignore
                                onClick={() => setServerErrors({wnioskodawca_nazwisko: ''})}
                                {...field}
                                id='wnioskodawca_nazwisko' placeholder='wnioskodawca_nazwisko'
                            />
                            <FormErrorMessage>{form.errors.wnioskodawca_nazwisko || serverErrors.wnioskodawca_nazwisko}</FormErrorMessage>
                        </FormControl>}
                    </Field>

                    <ButtonGroup>
                        <Button
                            // @ts-ignore
                            type={'submit'} variant={'outline'} alignSelf={'flex-start'}
                            colorScheme={'green'}>Dodaj</Button>
                        <Button variant={'outline'} alignSelf={'flex-start'} colorScheme={'orange'}>Anuluj</Button>
                    </ButtonGroup>
                </Container>
            </Form>
        </Formik>
    </>
}
