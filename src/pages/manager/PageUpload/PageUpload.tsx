import React, {useState} from 'react'
import {useMutation, useQuery} from '@apollo/client'
import {filesQuery, Loader} from '../../../components'
import {Uploader} from "../../../components/uploader";
import {uploadFileM} from "../../../graphql/manager/mutations/uploadFileM";
import {
    Box,
    Flex,
    HStack,
    IconButton,
    Modal, ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalHeader,
    ModalOverlay,
    Td,
    Th, useDisclosure, Text, ModalFooter, Button
} from "@chakra-ui/react";
import SimpleTable from "../../../components/simpleTable/SimpleTable";
import {listPlikiQ} from "../../../graphql/manager/queries";
import {columnsTable} from './tableColumns'
import {RiDownloadFill, RiFileInfoFill} from 'react-icons/ri';
import {DeleteIcon} from "@chakra-ui/icons";
import {deleteFileM} from "../../../graphql/manager/mutations";
import {downloadFile} from "../../../utils/services/downloadFile";

export const PageUpload = () => {
    const tableInitState = {sortBy: [{id: 'id', desc: false}], pageSize: 10}

    const {data: pliki, loading: loadingPliki} = useQuery(listPlikiQ, {variables: {domain: 'ocena'}})

    const [uploadFile, {loading: uploading}] = useMutation(uploadFileM, {
        refetchQueries: [filesQuery, listPlikiQ],
    })

    const [deleteFile, {loading: deleting}] = useMutation(deleteFileM, {
        refetchQueries: [filesQuery, listPlikiQ],
    })

    const meta = {
        folder: 'resources',
        domain: 'ocena',
        fileType: 'dokument'
    }

    //@ chakra's hook do otwierania modala
    const {isOpen, onOpen, onClose} = useDisclosure()


    //@ stan lokalny
    const [file, setFile] = useState(null)

    //@ handler usuń plik
    const handleDelete = async (row: any) => {
        await setFile(row.original)
        onOpen()
    }


    //@ header dodatkowej kolumny
    const addThPliki = () => <Th>Operacje</Th>

    //@body dodatkowej kolumny
    const addTdPliki = (row: any) => {
        const {fileName, name} = row.original
        return <Td p={0}>
            <HStack justifyContent={'flex-center'}>
                <IconButton
                    title={'pobierz'}
                    onClick={() => downloadFile(name, fileName, 'resources')}
                    variant='ghost'
                    aria-label='pobierz'
                    icon={<RiDownloadFill/>}
                />
                <IconButton
                    title={'usuń'}
                    colorScheme={'red'}
                    onClick={() => handleDelete(row)}
                    variant='ghost'
                    aria-label='usuń'
                    icon={<DeleteIcon/>}
                />

            </HStack>
        </Td>
    }

    //@ przygotuj modal
    const renderModals = () => {
        // @ts-ignore
        return <Modal size={'lg'} isOpen={isOpen} onClose={() => {
            onClose()
        }}>
            <ModalOverlay/>
            <ModalContent>
                <ModalHeader>Usuń plik</ModalHeader>
                <ModalCloseButton/>
                <ModalBody pb={6}>
                    {/*// @ts-ignore*/}
                    <Text> Zamierzasz usunąć plik o nazwie {file?.fileName} </Text>

                    <Text mt={5} fontSize={'20'}>Czy na pewno?</Text>
                </ModalBody>

                <ModalFooter>
                    <Button colorScheme='blue' mr={3} onClick={onClose}>
                        Rezygnuj
                    </Button>

                    <Button onClick={() => {
                        // @ts-ignore
                        return deleteFile({variables: {filePath: file.filePath}}).then(() => onClose())
                    }} variant='ghost'
                            colorScheme={'red'}>Usuń plik</Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    }

    return (
        <> {(loadingPliki || uploading || deleting) && <Loader/>}
            <Flex gap={8} wrap={"wrap"}>
                <Box mt={2} flexGrow={2}>
                    <Uploader
                        titleSize={'sm'}
                        uploadCb={uploadFile}
                        meta={meta}
                    />
                </Box>

                <Box flexGrow={3}>

                    {/*// @ts-ignore*/}
                    <SimpleTable
                        inModal
                        data={pliki?.listPliki}
                        columns={columnsTable}
                        tableInitState={tableInitState}
                        pagination
                        addTh={addThPliki}
                        addTd={addTdPliki}
                        modals={renderModals}
                        tableTitle={'Repozytorium dokumentów'}
                        headerIcon={<RiFileInfoFill/>}
                        globalFilter

                    />
                </Box>
            </Flex>
        </>
    )

}
