import React from 'react'
import {Heading} from '@chakra-ui/react'
import {useParams} from 'react-router-dom'
import {WniosekDane} from "../oceniajacy/PageOcena/wniosekDane/WniosekDane";
import {useQuery} from "@apollo/client";
import {WniosekQ} from "../../graphql/oceniająjcy/queries";

export const PageWniosek = () => {
    const {wniosekId} = useParams()


    // @ dane wniosku z serwera
    const {data: wniosek, loading: loadingWniosek} = useQuery(WniosekQ, {
        variables: {
            wniosek_id: +wniosekId!
        }
    })


    return (
        <>
            <Heading>Szczegóły wniosku</Heading>
            <WniosekDane wniosek={wniosek}/>
        </>
    )
}
