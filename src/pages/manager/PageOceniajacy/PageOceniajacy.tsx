import {useQuery, useLazyQuery} from '@apollo/client'
import {
    Box,
    HStack,
    IconButton,
    Td,
    Th,

    Tabs,
    TabList,
    Tab,
    TabPanels,
    TabPanel,
    Tag,
    Flex
} from '@chakra-ui/react'
import React, {useEffect, useState} from 'react'
import {oceniajacyQ} from '../../../graphql/manager/queries'
import SimpleTable from "../../../components/simpleTable/SimpleTable";
import {columnsTableWnioskiOceniajacego} from "./tableColumns";
import {

    RiEyeFill,
    RiFileExcel2Line,
} from "react-icons/all";
import {downloadReport} from "../../../utils/services";
import {useDispatch} from "react-redux";
import {Loader} from "../../../components";
import {RiUserFill} from "react-icons/ri";
import {useNavigate, useParams} from "react-router-dom";
import {listUsersOcenyQ} from "../../../graphql/manager/queries/listUsersOcenyQ";
import {reportListUsersOcenyQ} from "../../../graphql/manager/queries/reports/reportListUserOceny";
import {addToast} from "../../../redux/toaster";
import {msg} from "../../../utils/messages/manager";
import {OceniajacyDetails} from "../../../components/OceniajacyDetails";
import {EditIcon} from "@chakra-ui/icons";
import {setMode} from "../../../redux/ocena";


export const PageOceniajacy = () => {
    //@ wstępna konfiguracja react-table
    const tableInitState = {
        sortBy: [{id: 'id', desc: false}], pageSize: 10
    }
    //@ hooki z react-router i reduxa
    const {oceniajacyId} = useParams()
    const dispatch = useDispatch()

    //@ nawigacja react-router-dom
    const navigate = useNavigate()


    //@ stan lokalny
    const [loading] = useState(false)

    //@ dane oceniającego
    const {data: oceniajacyData, loading: loadingOceniajacy} = useQuery(oceniajacyQ, {
        variables: {
            oceniajacy_id: oceniajacyId
        }
    })

    //@ dane ocen
    const {data: oceny, loading: loadingOceny} = useQuery(listUsersOcenyQ, {
        variables: {
            oceniajacy_id: oceniajacyId
        }
    })


    const [filtered, setFiltered] = useState({
        wszystkie: null,
        zatwierdzone: null,
        wEdycji: null,
        przeslane: null,
        zwrocone: null,
    })

    //@ Lista tabsów
    const tabs: any[] = [{name: 'Wszystkie', data: filtered?.wszystkie || [], scheme: null},
        {name: 'Zatwierdzone', data: filtered?.zatwierdzone || [], scheme: 'green'},
        {name: 'W edycji', data: filtered?.wEdycji || [], scheme: 'purple'},
        {name: 'Przesłane', data: filtered?.przeslane || [], scheme: 'blue'},
        {name: 'Zwrócone', data: filtered?.zwrocone || [], scheme: 'red'}]


    //@ pobieranie excela
    const [reportListUsersOceny, {
        data,
        loading: loadingReport
    }] = useLazyQuery(reportListUsersOcenyQ, {variables: {oceniajacy_id: oceniajacyId}})


    // @ obsłuż przycisk weryfikuj
    const handleZweryfikuj = (id: any) => {
        dispatch(setMode('verify'))
        navigate(`/manager/oceny/${id}`)
    }
    // @ obsłuż przycisk zobacz
    const handleZobacz = (id: any) => {
        dispatch(setMode('watch'))
        navigate(`/manager/oceny/${id}`)
    }


    //@ Pobierz raport i wygeneruj toasta
    const handleDownloadReport = () => {
        reportListUsersOceny()
            .then(() => dispatch(addToast({text: msg.success.reportDownloaded, variant: 'success'})))
    }

    //@ header dodatkowej kolumny
    const addTh = () => <Th p={0} m={0}>Operacje</Th>

    //@body dodatkowej kolumny
    const addTd = (row: any) => {
        const status = row.original.status.id
        const perm = {
            show: {
                edit: status === "ZATWIERDZONY" || status === "PRZESLANY",
            }
        }
        return <Td p={0} m={0}>
            <HStack justifyContent={'flex-end'}>
                <IconButton
                    onClick={() => handleZobacz(`${row.values.id}`)}
                    title={'zobacz'}
                    variant='ghost'
                    aria-label='podejrzyj wniosek'
                    icon={<RiEyeFill/>}
                />
                {perm.show.edit && <IconButton
                    onClick={() => handleZweryfikuj(`${row.values.id}`)}
                    title={'zweryfikuj ocenę'}
                    variant='ghost'
                    aria-label='podejrzyj wniosek'
                    icon={<EditIcon/>}
                />}
            </HStack>
        </Td>
    }


    //@ pobierz excela gdy dostępne dane
    useEffect(() => {
        if (data) {
            downloadReport(data.reportListUsersOceny.fileName)
        }
    }, [data])


    useEffect(() => {
        if (oceny) {
            setFiltered({
                wszystkie: oceny?.listUsersOceny,
                zatwierdzone: oceny?.listUsersOceny.filter((o: { status: { id: string } }) => o.status.id === 'ZATWIERDZONY'),
                wEdycji: oceny?.listUsersOceny.filter((o: { status: { id: string } }) => o.status.id === 'W_EDYCJI'),
                przeslane: oceny?.listUsersOceny.filter((o: { status: { id: string } }) => o.status.id === 'PRZESLANY'),
                zwrocone: oceny?.listUsersOceny.filter((o: { status: { id: string } }) => o.status.id === 'ZWROCONY'),
            })
        }
    }, [oceny])


    // @ts-ignore
    return (
        <>
            {(loading || loadingReport || loadingOceniajacy || loadingOceny) &&
                <Loader/>}
            <OceniajacyDetails filtered={filtered} user={oceniajacyData?.oceniajacy}/>

            <Box>
                <Tabs isLazy colorScheme={'orange'}>
                    <TabList as={Flex} alignItems={'center'} justifyContent={'space-between'}>
                        <Flex flexDir={'row'}>
                            {tabs.map(t =>
                                <Tab key={t.name}>{t.name}
                                    <Tag colorScheme={t.scheme!}
                                         ml={2}>{t.data.length}
                                    </Tag>
                                </Tab>)}
                        </Flex>

                        <Box>
                            <IconButton
                                onClick={handleDownloadReport}

                                size={'sm'}
                                fontSize={16}
                                title={'eksport do excel'}
                                variant='outline'
                                colorScheme='teal'
                                aria-label='excel'
                                icon={<RiFileExcel2Line/>}
                            />
                        </Box>

                    </TabList>
                    <TabPanels>
                        {tabs.map(t =>
                            <TabPanel key={t.name} m={0} p={0}>
                                <SimpleTable
                                    headerIcon={<RiUserFill/>}
                                    // topAction={topAction}
                                    addTh={addTh}
                                    addTd={addTd}
                                    tableTitle={`Lista ocen oceniającego`}
                                    pagination
                                    globalFilter
                                    tableInitState={tableInitState}
                                    data={t.data || []}
                                    columns={columnsTableWnioskiOceniajacego}
                                />
                            </TabPanel>)}
                    </TabPanels>
                </Tabs>
            </Box>
        </>
    )
}
