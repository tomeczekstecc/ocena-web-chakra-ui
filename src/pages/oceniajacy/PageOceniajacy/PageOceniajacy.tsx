import {useQuery, useLazyQuery} from '@apollo/client'
import {
    Box,
    HStack,
    IconButton,
    Td,
    Th,
    Tabs,
    TabList,
    Tab,
    TabPanels,
    TabPanel,
    Tag,
    Flex,
} from '@chakra-ui/react'
import React, {useEffect, useState} from 'react'
import SimpleTable from "../../../components/simpleTable/SimpleTable";
import {columnsTableWnioskiOceniajacego} from "./tableColumns";
import {GrDocumentPdf, RiEyeFill, RiFileExcel2Line, VscFilePdf, ImFilePdf} from "react-icons/all";
import {downloadReport} from "../../../utils/services";
import {useDispatch, useSelector} from "react-redux";
import {Loader} from "../../../components";
import {RiUserFill} from "react-icons/ri";
import {listUsersOcenyQ} from "../../../graphql/oceniająjcy/queries";
import {reportListUsersOcenyQ} from "../../../graphql/oceniająjcy/queries/reports";
import {EditIcon} from "@chakra-ui/icons";
import {useNavigate} from "react-router-dom";
import {setMode} from "../../../redux/ocena";
import {OceniajacyDetails} from "../../../components/OceniajacyDetails";
import {downloadFile} from "../../../utils/services/downloadFile";
import {downloadPdf} from "../../../utils/services/downloadPdf";


export const PageOceniajacy = () => {
    //@ wstępna konfiguracja react-table
    const tableInitState = {
        sortBy: [{id: 'id', desc: false}], pageSize: 10
    }

    //@ stan lokalny
    const [loading] = useState(false)

    //@ pobierz metodę dispatch
    const dispatch = useDispatch()

    //@ dane użytkownika z reduxa
    // @ts-ignore
    const user = useSelector(store => store.user.info)

    //@ hook navigate

    const navigate = useNavigate()


    //@ dane oceniającego z serwera
    const {data: oceny, loading: loadingOceny} = useQuery(listUsersOcenyQ, {
        variables: {
            oceniajacy_id: user?.sub
        }
    })

    const [filtered, setFiltered] = useState({
        wszystkie: null,
        zatwierdzone: null,
        wEdycji: null,
        przeslane: null,
        zwrocone: null,
    })

    //@ Lista tabsów
    const tabs: any[] = [{name: 'Wszystkie', data: filtered?.wszystkie || [], scheme: null},
        {name: 'Zatwierdzone', data: filtered?.zatwierdzone || [], scheme: 'green'},
        {name: 'W edycji', data: filtered?.wEdycji || [], scheme: 'purple'},
        {name: 'Przesłane', data: filtered?.przeslane || [], scheme: 'blue'},
        {name: 'Zwrócone', data: filtered?.zwrocone || [], scheme: 'red'}]

    //@ obsłuż przycisk
    const handleButtonClick = (id: string, mode = 'edit') => {
        dispatch(setMode(mode))
        navigate(id);
    }


    //@ pobieranie excela
    const [reportListOceniajacy, {
        data,
        loading: loadingReport
    }] = useLazyQuery(reportListUsersOcenyQ, {variables: {oceniajacy_id: user?.sub}})

    //@ header dodatkowej kolumny
    const addTh = () => <Th p={0} m={0}>Operacje</Th>

    //@body dodatkowej kolumny
    const addTd = (row: any) => {
        const status = row.original.status.id
        const id = row.original.id.toString()
        const perm = {
            show: {
                edit: status !== "ZATWIERDZONY" && status !== "PRZESLANY",
                pdf: status === "ZATWIERDZONY",
            }
        }
        return <Td p={0} m={0}>
            <HStack justifyContent={'flex-start'}>

                <IconButton
                    onClick={() => handleButtonClick(row.original.id.toString(), 'watch')}
                    title={'zobacz'}
                    variant='ghost'
                    aria-label='podejrzyj wniosek'
                    icon={<RiEyeFill/>}
                />
                {perm.show.edit && <IconButton
                    onClick={() => handleButtonClick(row.original.id.toString())}
                    title={'edytuj'}
                    variant='ghost'
                    aria-label='oceniaj'
                    icon={<EditIcon/>}
                />}
                {perm.show.pdf && <IconButton
                    onClick={() => downloadPdf(`${id}.pdf`, 'oceny', id)}
                    title={'pobierz pdf oceny'}
                    variant='ghost'
                    aria-label='oceniaj'
                    icon={<ImFilePdf/>}
                />}

            </HStack>
        </Td>
    }


    //@ pobierz excela gdy dostępne dane
    useEffect(() => {
        if (data) {
            downloadReport(data.reportListUsersOceny.fileName)
        }
    }, [data])


    useEffect(() => {
        if (oceny) {
            setFiltered({
                wszystkie: oceny?.listUsersOceny,
                zatwierdzone: oceny?.listUsersOceny.filter((o: { status: { nazwa: string } }) => o.status.nazwa === 'Zatwierdzona'),
                wEdycji: oceny?.listUsersOceny.filter((o: { status: { nazwa: string, } }) => o.status.nazwa === 'W edycji'),
                przeslane: oceny?.listUsersOceny.filter((o: { status: { nazwa: string } }) => o.status.nazwa === 'Przesłana'),
                zwrocone: oceny?.listUsersOceny.filter((o: { status: { nazwa: string } }) => o.status.nazwa === 'Zwrócona'),
            })
        }
    }, [oceny])


    // @ts-ignore
    return (
        <>
            {(loading || loadingReport || loadingOceny) && <Loader/>}
            <OceniajacyDetails filtered={filtered} user={user}/>
            <Box>
                <Tabs isLazy colorScheme={'orange'}>
                    <TabList as={Flex} alignItems={'center'} justifyContent={'space-between'}>
                        <Flex flexDir={'row'}>
                            {tabs.map(t =>
                                <Tab key={t.name}>{t.name}
                                    <Tag colorScheme={t.scheme!}
                                         ml={2}>{t.data.length}
                                    </Tag>
                                </Tab>)}
                        </Flex>

                        <Box>
                            <IconButton
                                onClick={() => reportListOceniajacy()}
                                size={'sm'}
                                fontSize={16}
                                title={'eksport do excel'}
                                variant='outline'
                                colorScheme='teal'
                                aria-label='excel'
                                icon={<RiFileExcel2Line/>}
                            />
                        </Box>

                    </TabList>
                    <TabPanels>
                        {tabs.map(t =>
                            <TabPanel key={t.name} m={0} p={0}>
                                <SimpleTable
                                    headerIcon={<RiUserFill/>}
                                    // topAction={topAction}
                                    addTh={addTh}
                                    addTd={addTd}
                                    tableTitle={`Lista ocen oceniającego`}
                                    pagination
                                    globalFilter
                                    tableInitState={tableInitState}
                                    data={t.data || []}
                                    columns={columnsTableWnioskiOceniajacego}
                                />
                            </TabPanel>)}
                    </TabPanels>
                </Tabs>
            </Box>
        </>
    )
}
