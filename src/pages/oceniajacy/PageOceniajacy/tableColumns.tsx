export const columnsTableWnioskiOceniajacego = [
    {Header: 'ID', accessor: 'id',},
    {Header: 'Status oceny', accessor: 'status.nazwa'},
    {Header: 'Nr wniosku', accessor: 'wniosek.numer'},
    {Header: 'Imię wnioskodawcy', accessor: 'wniosek.wnioskodawca_imie'},
    {Header: 'Nazwisko wnioskodawcy', accessor: 'wniosek.wnioskodawca_nazwisko'},
    {Header: 'Punkty', accessor: 'punkty'},
]
