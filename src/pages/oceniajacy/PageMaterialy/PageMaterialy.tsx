import React from "react";
import {useQuery} from "@apollo/client";

import {listPlikiQ} from "../../../graphql/manager/queries";
import {CardFile} from "../../../components/CardFile";
import {Heading, Wrap, WrapItem} from "@chakra-ui/react";
import {Loader} from "../../../components";


export const PageMaterialy = () => {

    const {data: pliki, loading: loadingPliki} = useQuery(listPlikiQ, {variables: {domain: 'ocena'}})

    return <>
        {loadingPliki && <Loader/>}
        <Heading mb={5}>Materiały pomocnicze</Heading>

        <Wrap spacing={10}>{pliki?.listPliki?.map((p: any) =>
            <WrapItem key={p.filePath}>
                <CardFile file={p}/>
            </WrapItem>
        )}</Wrap>

    </>
}
