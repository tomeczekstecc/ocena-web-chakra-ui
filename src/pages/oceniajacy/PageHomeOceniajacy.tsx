import React from 'react'
import {Stack, Text, Link as ChackraLink, Flex, Grid} from "@chakra-ui/react";
import {NavLink} from "react-router-dom";
import {ArrowForwardIcon} from "@chakra-ui/icons";
import * as RiIcons from "react-icons/ri";
import {useKeycloak} from "@react-keycloak/web";
import ActionCard from "../../components/ActionCard";

export const PageHomeOceniajacy = () => {
    const {keycloak} = useKeycloak()

    const cards = [
        { to: '/oceniajacy/oceny', icon: <RiIcons.RiArticleLine size={60}/>, title: 'Twoje oceny', description: 'Przejdź do oceny lub podglądu swoich ocen' },
        { to: '/oceniajacy/materialy', icon: <RiIcons.RiFileCloudLine size={60}/>, title: 'Materiały pomocnicze', description: 'Pobierz i zapoznaj się z materiałami pomocniczymi' },
        { icon: <RiIcons.RiLogoutBoxRLine size={60}/>, title: 'Wyloguj się', description: 'Opuść aplikację i przejdź na stronę logowania', onClick: () => keycloak.logout()},
    ]
    return <Grid templateColumns='repeat(2, 1fr)' gap={6} >
        {/*// @ts-ignore*/}
        {cards.map((card, index) => <ActionCard key={index} {...card}/>)}
    </Grid>
}
