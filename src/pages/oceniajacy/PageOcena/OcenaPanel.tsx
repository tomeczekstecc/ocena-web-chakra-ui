import React, {cloneElement} from 'react'
import {
    FormControl,
    FormErrorIcon,
    FormErrorMessage,
    Input,
    Radio,
    RadioGroup, Select,
    Stack,
    Table,
    TableCaption, Tag,
    Tbody,
    Td,
    Textarea,
    Th,
    Thead,
    Tr, useDisclosure
} from "@chakra-ui/react";
import PropTypes from "prop-types";
import {keyMapper} from "../../../utils/helpers";
import {DateTime} from "../../../components/DateTime";
import {mapOptionToValue} from "../../../utils/helpers";
import {useSelector} from "react-redux";
import {ModalUzupelnienie} from "./ModalUzupelnienie";
import styled from 'styled-components'


const Wrapper = styled.div`
  input, textarea, select {
    border: 1px solid #666;
  }

  table {
    border-collapse: collapse;
  }

  tr td {
    border-bottom: 1px solid rgba(0, 0, 0, 0.2);

  }
`

// @ts-ignore
export const OcenaPanel = ({data, rows, set, form, type, errors}) => {

    // @ts-ignore
    const mode = useSelector(store => store.ocena.mode)

    const renderHeaders = (type: any) => {
        switch (type) {
            case  'dane':
                return <Tr>
                    <Th>Rodzaj danych</Th>
                    <Th>Dane</Th>
                    {/*<Th>Uwagi</Th>*/}
                    {/*<Th isNumeric>Uwagi</Th>*/}
                </Tr>

            case 'formalna':
                return <Tr>
                    <Th>Kryterium</Th>
                    <Th>Spełnia / Nie spełnia</Th>
                    <Th>Uwagi</Th>
                </Tr>

            case 'merytoryczna':
                return <Tr>
                    <Th>Kryterium</Th>
                    <Th>Wartość</Th>
                    <Th>Przyznane punkty</Th>
                </Tr>
            default:
                return
        }
    }


    const renderMiddleColumn = (type: any, r: {
        noOptionWezw: React.ReactNode | undefined;
        optionNie: React.ReactNode | undefined;
        optionNd: React.ReactNode | undefined;
        string: any;
        mapper: string;
        options: any;
        preferredDataKey: string | number | undefined;
        table: string;
        input: any;
        dataKey: string | number;
    }) => {
        // @ts-ignore

        const key: string | number = r?.preferredDataKey || r?.dataKey
        // @ts-ignore
        const radioElement = <RadioGroup>
            <Stack direction='column'>
                <Radio value={1}>Tak</Radio>
                <Radio value={0}>Nie</Radio>
            </Stack>
        </RadioGroup>

        // @ts-ignore
        const Clone = ({child, type = undefined as any}) => {
            // @ts-ignore
            return cloneElement(child, {
                value: form?.[r?.table]?.[key],
                // @ts-ignore
                onChange: (e) => set(r?.table, key, (type === 'date' ? e : type === 'radio' ? +e : e?.target?.value))
            })
        }
        switch (type) {


            case 'dane':
                // @ts-ignore
                return <Td>
                    {/*// @ts-ignore*/}
                    {keyMapper(data?.ocena, key)}
                    <FormControl isReadOnly={mode !== 'edit'} isInvalid={errors[r?.table + '.' + key]}>
                        {r?.input === 'input' ?
                            <Clone child={<Input/>}/> : r?.input === 'input' ?
                                <Clone child={<Select/>}/> : r?.input === 'textarea' ?
                                    <Clone child={<Textarea/>}/> : r?.input === 'date' ?
                                        // @ts-ignore
                                        <Clone type='date' child={<DateTime
                                            readOnly={mode !== "edit"}/>}/> : r?.input === 'radio' ?
                                            <Clone child={radioElement} type={'radio'}/>
                                            : null}
                        <FormErrorMessage> <FormErrorIcon/> {errors[r?.table + '.' + key]}</FormErrorMessage>
                    </FormControl>
                </Td>


            case  'formalna':
                return <Td maxW={'12vw'}>
                    <FormControl isReadOnly={mode !== 'edit'} isInvalid={errors[r?.table + '.' + key]}>
                        {/*// @ts-ignore*/}
                        <RadioGroup name={key} onChange={(e) => {
                            set(r?.table, key, +e)
                        }} value={form?.[r?.table]?.[key]}>
                            <Stack direction='column'>
                                <Radio value={1}>Tak</Radio>
                                {r.optionNie && <Radio value={0}>Nie</Radio>}
                                {r.optionNd && <Radio value={2}>Nie dotyczy</Radio>}
                                {!r.noOptionWezw && <Radio value={3}>Wezwanie do uzupełnienia</Radio>}
                            </Stack>
                        </RadioGroup>
                        <FormErrorMessage> <FormErrorIcon/> {errors[r?.table + '.' + key]}</FormErrorMessage>
                    </FormControl>
                </Td>


            case 'merytoryczna':
                return <Td>
                    <FormControl isDisabled={mode !== 'edit'} isInvalid={errors[r?.table + '.' + key]}>
                        <Select value={form?.[r?.table]?.[key] || ''} onChange={(e) => {
                            const value = r?.string ? e?.target?.value : +e?.target?.value
                            set(r?.table, key, value)
                            set(r?.table, key + '_punkty', mapOptionToValue(value, r?.mapper))
                        }}>
                            {r?.options.map((o: { label: {} | null | undefined; value: string | number | readonly string[] | undefined; }) =>
                                // @ts-ignore
                                <option key={o.label} value={o.value}>{o.label}</option>
                            )}
                        </Select>
                        <FormErrorMessage> <FormErrorIcon/> {errors[r?.table + '.' + key]}</FormErrorMessage>
                    </FormControl>
                </Td>


            default:
                return
        }
    }


    const renderLastColumn = (type: any, r: {
        mapper: string;
        table: string; dataKey: string; able: any;
    }) => {
        const key = r?.table + '.' + r?.dataKey
        switch (type) {

            // case 'dane':
            case 'formalna':
                return <Td minW={'25vw'} flexGrow={2}>
                    <FormControl isReadOnly={mode !== 'edit'} isInvalid={errors[key + '_uwagi']}>
                        <Textarea
                            // hidden={true}
                            disabled={!r?.able && ![0, 3].includes(form?.[r?.table]?.[r?.dataKey])}
                            aria-label={'pole uwagi'}
                            onChange={(e) => set(r.table, r?.dataKey + '_uwagi', e.target.value)}
                            value={form?.[r.table]?.[r?.dataKey + '_uwagi'] || ''}/>
                        <FormErrorMessage> <FormErrorIcon/> {errors[key + '_uwagi']}</FormErrorMessage>
                    </FormControl>
                </Td>


            case 'merytoryczna':
                return <Td flexGrow={2}>
                    <FormControl isReadOnly={mode !== 'edit'}>
                        {!!form?.[r?.table]?.[r?.dataKey + '_punkty'] &&
                            <Tag py={2} fontSize={'18'}
                                 colorScheme={'blue'}>{form?.[r?.table]?.[r?.dataKey + '_punkty']}</Tag>}
                    </FormControl>
                </Td>


            default:
                return
        }
    }

    const {
        isOpen: isOpenModalUzupelnienie,
        onOpen: onOpenModalUzupelnienie,
        onClose: onCloseModalUzupelnienie
    } = useDisclosure()

    return <Wrapper>
        {/*// @ts-ignore*/}
        <ModalUzupelnienie
            isOpen={isOpenModalUzupelnienie}
            onOpen={onOpenModalUzupelnienie}
            onClose={onCloseModalUzupelnienie}
        />
        <Table variant='simple'>
            <TableCaption>Wypełnij pola, wpisz uwagę jeśli kryterium nie jest spełnione.</TableCaption>
            <Thead>
                {renderHeaders(type)}
            </Thead>
            <Tbody>
                {/*// @ts-ignore*/}
                {data.ocena && rows.map((r, idx) => {

                        // eslint-disable-next-line array-callback-return
                        if (r.dataKey === "_1podsumowanie2" && form?.['formalne']?.['_1podsumowanie1'] !== 0) return
                        if (r.dataKey === "_2_2" && form?.['formalne']?.['_2_1'] !== 1) return null

                        return <Tr key={idx}>

                            {/*@Pierwsza kolumna*/}
                            <Td maxW={'500px'}>{r.label}</Td>


                            {/*@Druga kolumna*/}
                            {renderMiddleColumn(type, r)}


                            {/*@Trzecia kolumna*/}
                            {renderLastColumn(type, r)}
                        </Tr>

                    }
                )}
            </Tbody>
        </Table>
    </Wrapper>
}


OcenaPanel.propTypes = {
    data: PropTypes.object.isRequired,
    rows: PropTypes.array.isRequired,
    set: PropTypes.func.isRequired,
    form: PropTypes.object.isRequired,
    type: PropTypes.oneOf(['dane', 'formalna', 'merytoryczna']).isRequired,
    errors: PropTypes.object
}
