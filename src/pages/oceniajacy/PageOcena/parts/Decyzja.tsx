import React from "react";
import {Box, Flex, Text, Tag, Center} from "@chakra-ui/react";
import {mapDecisionKeyToItem} from "../../../../utils/helpers";

// @ts-ignore
export const Decyzja = ({// @ts-ignore
                            ocena,
                            // @ts-ignore
                            wniosek,
                            // @ts-ignore
                            punkty,
                            // @ts-ignore
                            decision,
                            // @ts-ignore
                            killer,
                            // @ts-ignore
                            wezwanie,
                            // @ts-ignore
                            punktyDodatkowe,
                            // @ts-ignore
                            punktyObow,
                            // @ts-ignore
                            punktyOlimpiady,
                            // @ts-ignore
                            punktyIndyw,
                            // @ts-ignore
                            punktyNiepeln,                            // @ts-ignore
                            formalna1,                            // @ts-ignore
                            formalna2,
                        }) => {

    // decision === 'positive' &&
    const punktyTiltes = [
        {label: "Suma punktów", data: punkty},
        {label: "Punkty za kryteria obowiązkowe", data: punktyObow},
        {label: "Punkty za kryteria dodatkowe", data: punktyDodatkowe},
        {label: "- w tym za olimpiady/konkursy", data: punktyOlimpiady},
        {label: "- w tym za indyw. tor nauki", data: punktyIndyw},
        {label: "- w tym za niepełnosprawność", data: punktyNiepeln},
    ]


    if (decision === 'pending') return <Text>Wypełnij wymagane pola formularza</Text>
    const printDecision = (decision: string) => {
        switch (decision) {
            case 'positive':
                // eslint-disable-next-line react/jsx-no-undef
                return <Tag colorScheme={'green'} py={3} px={5} fontSize={26}>Pozytywny</Tag>
            case 'negative':
                return <Tag colorScheme={'red'} py={3} px={5} fontSize={26}>NEGATYWNY</Tag>
            case 'wezwanie':
                return <Tag colorScheme={'orange'} py={3} px={5} fontSize={26}>WEZWANIE DO UZUPEŁNIENIA</Tag>

            default:
                return
        }
    }

    return <>

        <Center mt={10} w={'60%'}>
            <Box p={5} border={'1px solid'} borderRadius={5} fontSize={17}>
                <Flex justifyContent={'space-between'} justifyItems={'center'}>
                    <Box border={'1px solid'} p={2}>{wniosek?.numer}</Box>
                    {decision === 'positive' && <Box border={'1px solid'} p={2}>PUNKTY: <Text display={'inline'}
                                                                                              fontWeight={'bold'}> {punkty}</Text></Box>}
                    <Box border={'1px solid'} p={2}>{new Date().toLocaleDateString(['pl'])}</Box>
                </Flex>
                <Flex my={7} justifyContent={'center'}>
                    {printDecision(decision)}
                </Flex>
                <Flex justifyContent={'space-between'}>
                    <Box my={4}>
                        <Box fontSize={17}>Uczennica / uczeń:</Box>
                        <Box
                            fontWeight={'bold'}>{wniosek?.uczen.imie} {wniosek?.uczen.nazwisko}</Box>
                    </Box>
                    <Box my={4}>
                        <Box fontSize={17}>Data wpływu wniosku:</Box>
                        <Box
                            fontWeight={'bold'}>{ocena?.dane?.data_wplywu_wniosku ? new Date(ocena?.dane?.data_wplywu_wniosku).toLocaleDateString(['pl']) : ''}</Box>
                    </Box>

                    <Box my={4}>
                        <Box fontSize={17}>Opiekun dydaktyczny:</Box>
                        <Box fontWeight={'bold'}>{wniosek?.opiekun?.imie} {wniosek?.opiekun?.nazwisko}</Box>
                    </Box>
                </Flex>

                <Box my={4}>
                    <Box fontSize={17}>Ocena formalna I:</Box>
                    <Box fontWeight={'bold'}>{formalna1}</Box>
                </Box>
                <Box my={4}>
                    <Box fontSize={17}>Ocena formalna II:</Box>
                    <Box fontWeight={'bold'}>{formalna2}</Box>
                </Box>


                {decision === 'positive' && <Box>
                    <Box my={4} fontSize={17}>Ocena merytoryczna:</Box>
                    <Flex flexWrap={'wrap'} gap={2}>
                        {punktyTiltes.map(t =>
                            <Flex justifyContent={'center'} justifyItems={'center'} key={t.label}
                                  p={2} alignItems={'center'}
                                  flexDir={'column'} border={'1px solid'}
                                  flex={1}>
                                <Box textAlign={'center'}> {t.label}</Box>
                                <Box fontWeight={'bold'}>{t.data}</Box>
                            </Flex>)}


                    </Flex>
                </Box>}
                {(wezwanie.length > 0 && killer.length === 0) && <Box>
                    <hr/>
                    <Text mt={3} fontSize={17}>Kryteria podlegające wezwaniu do uzupełniania:</Text>
                    {wezwanie.map((w: string, idx: number) => {
                            const item = mapDecisionKeyToItem(w[0])
                            const keyUwaga = w[0] + "_uwagi"
                            return <Box my={4}><Text fontWeight={'bold'} key={w[0]}>[{idx + 1}] {item!.label}</Text>
                                <Text>Treść uwagi: {ocena.formalne[keyUwaga]} </Text>
                            </Box>
                        }
                    )}
                </Box>}
                {killer.length > 0 && <Box>
                    <hr/>

                    <Text mt={3} fontSize={17}>Kryterium niespełnione:</Text>
                    {killer.map((w: string) => {
                            const item = mapDecisionKeyToItem(w[0])
                            const keyUwaga = w[0] + "_uwagi"
                            return <Box key={w[0]}><Text fontWeight={'bold'}>{item!.label}</Text>
                                <Text>Treść uwagi: {ocena.formalne[keyUwaga]} </Text>
                            </Box>
                        }
                    )}
                </Box>}
            </Box>


        </Center>
    </>
}
