import {
    danePodstawowe,
    ocenaCzescA_B,
    ocenaDecyzjaIFormalna,
    ocenaDecyzjaIIFormalna,
    ocenaKryteriaDodatkowe, ocenaKryteriaFormalne, ocenaKryteriaObligatoryjne, ocenaKryteriaObowiazkowe
} from "../rows";

export const makeOcenaDecision = (form: any, setKiller: any, setWezwanie: any, setDecision: any, setPunkty: any) => {
    //@ wyznacz wymagane pola
    const requiredFieldsArr =
        [...danePodstawowe, ...ocenaCzescA_B, ...ocenaDecyzjaIFormalna, ...ocenaDecyzjaIIFormalna, ...ocenaKryteriaDodatkowe,
            // @ts-ignore
            ...ocenaKryteriaFormalne, ...ocenaKryteriaObligatoryjne, ...ocenaKryteriaObowiazkowe].reduce((arr, cur) => {
            // @ts-ignore
            if (cur.required) arr.push(cur.dataKey)
            return arr
        }, [])
    // @ts-ignore
    // @@ dodatkowe pola wymagane warunkowo
    if (form?.formalne?._1podsumowanie1 === 0) requiredFieldsArr.push('_1podsumowanie2')
    // @ts-ignore
    if (form?.formalne?._2_1 === 1) requiredFieldsArr.push('_2_2')

    //@ wyznacz wypełnione pola
    const shallowForm = Object.assign({}, form?.dane, form?.formalne, form?.merytoryczne)
    let fieldsFilled = Object.entries(shallowForm).reduce((arr, cur) => {
        // @ts-ignore
        if (cur[1] !== null) arr.push(cur[0])
        return arr
    }, [])

    //@ sprawdź czy wszystkie pola wymagane są wypełnione
    const ready = requiredFieldsArr.every(f => fieldsFilled.includes(f))

    //@ wyklucz część formularza gdzie występują zera nie będące killerami (od razu negatywna)
    const mayKillerBePresentForm = Object.assign({}, form?.formalne)
    const killer = Object.entries(mayKillerBePresentForm).filter(e => e[0] !== '_1podsumowanie1' && e[0] !== '_2_1' && e[1] === 0)
    // @ts-ignore
    setKiller(killer)
    const wezwanie = Object.entries(mayKillerBePresentForm).filter(e => e[0] !== '_1podsumowanie1' && e[0] !== '_2_1' && e[1] === 3)
    // @ts-ignore
    setWezwanie(wezwanie)


    // ostatecznie ustal decyzję
    if (ready && !killer.length && !wezwanie.length) {
        setDecision('positive')
        // @ts-ignore
        setPunkty(Object.entries(shallowForm).reduce((acc, cur) => {
            if (cur[0].endsWith('_punkty')) { // @ts-ignore
                acc = acc + cur[1]
            }
            return acc
        }, 0))
    } else if (killer.length > 0) {
        setDecision('negative')
        setPunkty(null)
    } else if (ready && wezwanie.length > 0) {
        setDecision('wezwanie')
        setPunkty(null)
    } else {
        setDecision('pending')
        setPunkty(null)
    }
}
