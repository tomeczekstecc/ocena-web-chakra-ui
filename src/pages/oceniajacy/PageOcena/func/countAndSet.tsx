export const countAndSet = (form: any, setter: (arg0: number) => void, key: string) => {
    // @ts-ignore
    setter(Object.entries(form?.merytoryczne).reduce((acc, cur) => {
        // @ts-ignore*                            }
        if (cur[0].startsWith(key) && cur[0].endsWith('_punkty')) acc = acc + cur[1]
        return acc
    }, 0))
}
