export const daneOpiekuna = [

    {
        label: "Imię opiekuna",
        dataKey: "imie",
    },
    {
        label: "Nazwisko opiekuna",
        dataKey: "nazwisko",
    },
    {
        label: "Profil opiekuna",
        dataKey: "profil",
    },
    {
        label: "Email opiekuna",
        dataKey: "email",
    },
    {
        label: "Telefon opiekuna",
        dataKey: "telefon",
    },


]
