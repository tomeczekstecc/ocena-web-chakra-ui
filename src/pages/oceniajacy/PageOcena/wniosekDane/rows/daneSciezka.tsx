export const daneSciezka = [


    {
        label: "Wybrany przedmiot kierunkowy",
        dataKey: "przedmiot",

    },
    {
        label: "Udział w konkursie/olimpiadzie",
        dataKey: "udzial_w_konk",
        type: 'checkbox'
    },
    {
        label: "Przygotowane pracy badawczej",
        dataKey: "praca_badawcza",
        type: 'textarea'
    },
    {
        label: "Przygotowane referatu/prezentacji",
        dataKey: "referat",
        type: 'textarea'

    },
    {
        label: "Przygotowanie publikacji",
        dataKey: "referat",
        type: 'textarea'

    },
    {
        label: "Przygotowanie wystawy",
        dataKey: "wystawa",
        type: 'textarea'

    },
    {
        label: "Przygotowanie filmu",
        dataKey: "wystawa",
        type: 'textarea'

    },
    {
        label: "Przygotowanie aplikacji",
        dataKey: "wystawa",
        type: 'textarea'

    },
    {
        label: "Uzyskanie certyfikatu",
        dataKey: "certyfikat",
        type: 'textarea'

    },
    {
        label: "Uzyskanie oceny bardzo dobrej",
        dataKey: "uzyskanie_bdb",
        type: 'checkbox'

    },
    {
        label: "Inne",
        dataKey: "inne",
        type: 'textarea'

    },

]
