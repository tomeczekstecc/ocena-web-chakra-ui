export const daneSzkoly = [


    {
        label: "Nazwa",
        dataKey: "nazwa",

    },
    {
        label: "Typ szkoły",
        dataKey: "typ",

    },

    {
        label: "Kod pocztowy",
        dataKey: "kod_pocztowy",
    },
    {
        label: "Miejscowość",
        dataKey: "miejscowosc",
    },
    {
        label: "Ulica",
        dataKey: "ulica",
    }, {
        label: "Numer budynku",
        dataKey: "nr_budynku",
    },
    {
        label: "Województwo",
        dataKey: "wojewodztwo",
    },


]
