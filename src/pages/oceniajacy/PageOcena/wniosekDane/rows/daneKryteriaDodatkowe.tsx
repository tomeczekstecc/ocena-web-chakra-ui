export const daneKryteriaDodatkowe = [

    {
        label: "Finalista",
        dataKey: "czy_finalista",
        type: "checkbox",
    },
    {
        label: "Czu uczennica/uczeń z niepełnosprawnościami",
        type: "checkbox",
        dataKey: "czy_z_niepelnospr",
    },
    {
        label: "Czy uczeń/uczennica posiada zezwolenie",
        dataKey: "czy_zezwolenie",
        type: "checkbox",
    }
]
