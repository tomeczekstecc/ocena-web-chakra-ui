export const danePodstawowe = [
    {
        label: "Numer wniosku",
        dataKey: "numer",
    },
    {
        label: "Imię wnioskodawcy",
        dataKey: "wnioskodawca_imie",
    },

    {
        label: "Nazwisko wnioskodawcy",
        dataKey: "wnioskodawca_nazwisko",
    },
    {
        label: "Email wnioskodawcy",
        dataKey: "wnioskodawca_email",
    },
    {
        label: "Telefon wnioskodawcy",
        dataKey: "wnioskodawca_telefon",

    },
    {
        label: "Typ wnioskodawcy",
        dataKey: "typ_wnioskodawcy",
    },
    {
        label: "Telefon",
        dataKey: "telefon",
    },

]
