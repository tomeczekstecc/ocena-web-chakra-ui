export const daneBudzet = [

    {
        label: "Zakup pomocy dydaktycznych i przyborów szkolnych",
        dataKey: "pomoce_dydakt",
    },
    {
        label: "Wydatki związane z wyposażeniem miejsca domowej nauki",
        dataKey: "wyposaz_domu_do_nauki",
    },
    {
        label: "Zakup sprzętu komputerowego wraz z oprogramowaniem i akcesoriami",
        dataKey: "sprzet_komput",
    },
    {
        label: "Zakup sprzętu elektronicznego",
        dataKey: "sprzet_elektron",
    },
    {
        label: "Opłaty związane z dostępem do Internetu",
        dataKey: "internet",
    },
    {
        label: "Koszty transportu ponoszone przez ucznia/uczennicę w celu dotarcia do szkoły oraz na dodatkowe zajęcia edukacyjne. (Nie dotyczy kosztów związanych z zakupem paliwa samochodowego)",
        dataKey: "transport",
    },
    {
        label: "Opłaty za kursy, szkolenia",
        dataKey: "kursy",
    }, {
        label: "Wydatki zawiązane z wyposażeniem ucznia/uczennicy niezbędnym dla realizacji potrzeb edukacyjno-rozwojowych wskazanych w PRU",
        dataKey: "pru",
    }, {
        label: "Koszty uczestnictwa w konkursach, turniejach, olimpiadach",
        dataKey: "koszt_udzial_w_konkur",
    },
    {
        label: "Koszty uczestnictwa w kulturze wysokiej",
        dataKey: "kultur_wysok",
    }, {
        label: "Inne koszty",
        dataKey: "inne",
    }, {
        label: "Razem koszty",
        dataKey: "razem",
    }, {
        label: "Uzasadnienie planowanych wydatków, które nie mieszczą się w katalogu wskazanym w § 8 ust. 5 Regulaminu",
        dataKey: "uzasadnienie_pozostale",
        type: 'textarea'
    }, {
        label: "Uzasadnienie zakupu sprzętu tożsamego z już zakupionym sprzętem ze środków stypendialnych",
        dataKey: "uzasadnienie_ponowne",
        type: 'textarea'
    },
]
