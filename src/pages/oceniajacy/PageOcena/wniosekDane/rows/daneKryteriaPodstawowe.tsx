export const daneKryteriaPodstawowe = [

    {
        label: "Ocena z matematyki",
        dataKey: "matematyka_ocena",
    },
    {
        label: "Nazwa innego przedmiotu kluczowego",
        dataKey: "kluczowy_inny_przedmiot_nazwa",
    },
    {
        label: "Ocena innego przedmiotu kluczowego",
        dataKey: "kluczowy_inny_przedmiot_ocena",
    },
    {
        label: "Nazwa języka obcego",
        dataKey: "kluczowy_jezyk_obcy_nazwa",
    },
    {
        label: "Ocena z języka obcego",
        dataKey: "kluczowy_jezyk_obcy_ocena",
    },
    {
        label: "średnia przedmiotów kierunkowych",
        dataKey: "srednia_kierunkowe",
    },
    {
        label: "Średnia wszstkich przedmiotów",
        dataKey: "srednia",
    },

]
