import {
    danePodstawowe, daneSzkoly,
    daneUcznia, daneOpiekuna, daneBudzet,
    daneSciezka, daneKryteriaPodstawowe, daneKryteriaDodatkowe
} from "../rows";

export const tabs = {
    tabHeads: [
        {label: 'I. Dane osob. wnioskodawcy'},
        {label: 'II. Dane osob. ucznia'},
        {label: 'III. Dane dotyczące szkoły'},
        {label: 'IV. Dane kandydata na opiekuna'},
        {label: 'V. Kryt.podstawowe'},
        {label: 'VI. Kryt.dodatkowe'},
        {label: 'VII. Ścieżka eduk.1'},
        {label: 'VII. Ścieżka eduk.2'},
        {label: 'VIII. Plan wydatków'},

    ],
    tabBodies: [
        {rows: danePodstawowe, dataKey: 'wniosek'},
        {rows: daneUcznia, dataKey: 'wniosek.uczen'},
        {rows: daneSzkoly, dataKey: 'wniosek.szkola'},
        {rows: daneOpiekuna, dataKey: 'wniosek.opiekun'},
        {rows: daneKryteriaPodstawowe, dataKey: 'wniosek.kryteria_podst'},
        {rows: daneKryteriaDodatkowe, dataKey: 'wniosek.kryteria_dodatk'},
        {rows: daneSciezka, dataKey: 'wniosek.sciezka1'},
        {rows: daneSciezka, dataKey: 'wniosek.sciezka2'},
        {rows: daneBudzet, dataKey: 'wniosek.budzet'},
    ]

}
