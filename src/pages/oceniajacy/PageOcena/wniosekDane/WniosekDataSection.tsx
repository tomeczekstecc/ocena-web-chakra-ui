import React from 'react'
import {Input, FormControl, FormLabel, Box, Textarea} from "@chakra-ui/react";
import PropTypes from "prop-types";

export const WniosekDataSection = (props: { title: any; fields: any[]; data: { [x: string]: any; }; }) => {
    return <Box mb={7}>
        {props?.fields?.map((d, idx) => {
            if (typeof props.data?.[d.dataKey] === 'object') {
                return null
            } else {
                return <FormControl key={idx} mb={5}>
                    <FormLabel>{d.label}</FormLabel>
                    {d.type === 'textarea' ? <Textarea isReadOnly value={props.data?.[d.dataKey]}/>
                        :
                        <Input
                            isReadOnly

                            value={(props.data?.[d.dataKey] === true ? 'Tak' : props.data?.[d.dataKey] === false ? 'Nie' : props.data?.[d.dataKey]) || ''}/>
                    }
                </FormControl>

            }
        })}


    </Box>
}

WniosekDataSection.propTypes = {
    data: PropTypes.oneOfType([PropTypes.array, PropTypes.object]), //tabolica z danymi do wyświetlenia
    fields: PropTypes.any.isRequired, // tablica z labelami i mapperem
}
