import React, {useState} from "react";
import {keyMapper} from "../../../../utils/helpers";
import {WniosekDataSection} from "./WniosekDataSection";
import {Box, Flex, Tab, TabList, TabPanel, TabPanels, Tabs, useColorMode} from "@chakra-ui/react";
import {tabs} from "./tabs/tabs";
import ButtonsPanel from "../../../../components/ButtonsPanel";
import styled from 'styled-components'


const Wrapper = styled.div`
  input, textarea, select {
    border: 1px solid #666;
  }

  table {
    border-collapse: collapse;
  }

  tr td {
    border-bottom: 1px solid rgba(0, 0, 0, 0.2);

  }
`
// @ts-ignore
export const WniosekDane = ({wniosek}) => {
    const {colorMode} = useColorMode()
    const baseBg = colorMode === 'light' ? 'gray.100' : 'gray.700'

// export const WniosekDane = ({wniosekId}) => {

    // // @ dane wniosku z serwera
    // const {data: wniosek, loading: loadingWniosek} = useQuery(WniosekQ, {
    //     variables: {
    //         wniosek_id: +wniosekId || 0
    //     }
    // })

    const [tabIndex, setTabIndex] = useState<number>(0)

    //@ zarządzaj aktywnymi tabami
    const handleTabsChange = (index: number) => {
        const max = tabs.tabHeads.length
        if (index >= max) {
            setTabIndex(max - 1)
        } else if (index <= 0) {
            setTabIndex(0)
        } else {
            setTabIndex(index)
        }
    }


    return <Wrapper>
        {/*{loadingWniosek && <Loader/>}*/}
        <Tabs mt={5} ms={-4} variant='unstyled' isLazy index={tabIndex} onChange={handleTabsChange}>
            <Flex gap={3}>
                <TabList borderRadius={7} bg={baseBg} p={4} flex={2}>
                    <Flex gap={5} flexDir={'column'}>
                        {tabs.tabHeads.map((t, idx) =>
                            <Box key={idx}>
                                <Tab
                                    // fontWeight={'bold'}
                                    fontSize={17}
                                    _selected={{
                                        borderRadius: '3px',
                                        color: 'white',
                                        width: '100%',

                                        bg: `${colorMode === 'light' ? 'gray.500' : 'gray.800'}`,
                                    }}>
                                    {t.label}
                                </Tab> </Box>)}

                    </Flex>
                </TabList>
                <TabPanels borderRadius={7} bg={baseBg} p={4} flex={9}> {tabs.tabBodies.map(b =>
                    <TabPanel key={b.dataKey}>
                        {/*// @ts-ignore*/}
                        <WniosekDataSection
                            fields={b.rows}
                            data={keyMapper(wniosek, b.dataKey)} title={''}/>
                    </TabPanel>)}
                </TabPanels>
            </Flex>
        </Tabs>
        <ButtonsPanel
            curIndex={tabIndex}
            disableNext={tabIndex === tabs.tabHeads.length - 1}
            disablePrev={tabIndex === 0}
            watchMode={true}
            handleTabsChange={handleTabsChange}/>
    </Wrapper>

}
