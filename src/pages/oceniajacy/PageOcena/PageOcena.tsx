import React, {useEffect, useState} from 'react'
import {useNavigate, useParams} from "react-router-dom";
import {useMutation, useQuery} from "@apollo/client";
import {listUsersOcenyQ, ocenaExtendedQ, ocenaQ, WniosekQ} from "../../../graphql/oceniająjcy/queries";
import {listUsersOcenyQ as listUO, listOcenyQ} from "../../../graphql/manager/queries";
import {
    Box,
    Flex,
    Tab,
    TabList,
    TabPanel,
    TabPanels,
    Tabs,
    Tag,
    Alert,
    AlertTitle,
    AlertDescription,
    AlertIcon,
    useToast,
    useDisclosure, useColorMode,
} from "@chakra-ui/react";
import PropTypes from "prop-types";

import {Loader, OcenaPanel} from "../../../components";
import ReactJson from 'react-json-view'
import {updateOcenaM, submitOcenaM, zmienStatusOcenyM} from "../../../graphql/oceniająjcy/mutations";
import {tabs} from "./tabs/tabs";
import {Decyzja} from "./parts/Decyzja";
import {WniosekDane} from "./wniosekDane/WniosekDane";
import {useDispatch, useSelector} from "react-redux";
import ButtonsPanel from "../../../components/ButtonsPanel";
import {countErrors} from "../../../utils/helpers";
import {setOcenaData} from "../../../redux/ocena";
import {setWniosekData} from "../../../redux/wniosek";
import {addToast} from "../../../redux/toaster";
import {msg} from "../../../utils/messages/manager";
import {SmallToast} from "../../../components/SmallToast";
import {zatwierdzOceneM, zwrocOceneM} from "../../../graphql/manager/mutations";
import {ModalWeryfikacja} from "./ModalWeryfikacja";
import {makeOcenaDecision} from "./func/makeDecisionFunc";
import {WeryfikacjeList} from "./Weryfikacje/WeryfikacjeList";
import {countAndSet} from "./func/countAndSet";
// @ts-ignore
import styles from '../../../styles/ocenapanel.module.css'

export const PageOcena = () => {

    const colorMode = useColorMode().colorMode

    const baseBg = colorMode === 'light' ? 'gray.100' : 'gray.700'

    //@ hook params -  id oceny z routingu
    const {ocenaId} = useParams()

    //@ wróć jeżeli nie ma mode
    const navigate = useNavigate()

    // @ts-ignore
    const mode = useSelector(store => store.ocena.mode)

    // @ts-ignore
    const weryfikujacy = useSelector(store => store?.user?.info)
    // @ts-ignore
    const oceniajacyId = useSelector(store => store?.user?.info?.sub)
    const dispatch = useDispatch()
    //@ pobierz dane oceny i wniosku
    const {
        data: ocenaExtended,
        loading: loadingOcenaExtended
    } = useQuery(ocenaExtendedQ, {variables: {ocena_id: +ocenaId!}})


    // @ dane wniosku z serwera
    const {data: wniosek, loading: loadingWniosek} = useQuery(WniosekQ, {
        variables: {
            wniosek_id: +ocenaExtended?.ocena?.wniosek?.id || 0
        }
    })

    if (wniosek) dispatch(setWniosekData(wniosek.wniosek))


    // @ chackra's toast hook
    const toast = useToast()

    //@ chakra's hook do otwierania modala
    const {isOpen: isOpenModalWeryf, onOpen: onOpenModalWeryf, onClose: onCloseModalWeryf} = useDisclosure()

    //@ pobierz dane oceny
    const {data: ocena, loading: loadingOcena} = useQuery(ocenaQ, {variables: {ocena_id: +ocenaId!}})

    //@ Stan lokalny
    const [form, setForm] = useState({...ocena?.ocena})
    const [weryfikacjaForm, setWeryfikacjaForm] = useState<object>({note: '', decyzja: null})
    const [errors, setErrors] = useState<object>({})
    const [werErrors, setWerErrors] = useState<object>({})
    const [decision, setDecision] = useState<string>('pending')
    const [punkty, setPunkty] = useState<string | null>(null)
    const [killer, setKiller] = useState<string | null>(null)
    const [wezwanie, setWezwanie] = useState<string | null>(null)
    const [tabIndex, setTabIndex] = useState<number>(0)
    const [punktyOlimpiady, setPunktyOlimpiady] = useState(null)
    const [punktyObow, setPunktyObow] = useState(null)
    const [punktyNiepeln, setPunktyNiepeln] = useState(null)
    const [punktyIndyw, setPunktyIndyw] = useState(null)
    const [formalna1, setFormalna1] = useState(null)
    const [formalna2, setFormalna2] = useState(null)


    //@ update ocenę
    const [updateOcena] = useMutation(updateOcenaM, {
        variables: {
            ocenaInput: {
                ...form,
                wynik: decision,
                formalna_1: formalna1,
                formalna_2: formalna2,
                punkty_olimpiady: punktyOlimpiady,
                punkty_obow: punktyObow,
                punkty_niepeln: punktyNiepeln,
                punkty_indyw: punktyIndyw
            }
        },
        onCompleted: () => toast({
            duration: 600,
            position: 'top-right',
            render: () => <SmallToast/>
        }), refetchQueries: [{query: listUsersOcenyQ, variables: {oceniajacy_id: oceniajacyId}}]
    })

    //@ akcja po sukcesie
    const handleAtionCompleted = (action: string) => {
        const msgToSend = action === 'submit' ? msg.success.submittedOcena : action === 'decline' ? msg.success.decline : msg.success.approve
        setErrors([])
        dispatch(addToast({text: msgToSend, variant: 'success'}))
        navigate(-1)
    }

    const refetchQueries = [{
        query: listUO,
        variables: {oceniajacy_id: ocenaExtended?.ocena?.oceniajacy_id}
    }, listOcenyQ, {query: listUsersOcenyQ, variables: {oceniajacy_id: ocenaExtended?.ocena?.oceniajacy_id}}]

    //@ Zatwierdź ocenę (strona managera)
    const [zatwierdzOcene] = useMutation(zatwierdzOceneM, {
        refetchQueries,
        onCompleted: () => handleAtionCompleted('approve')
    })


    //@ zwróć ocenę (strona managera)
    const [zwrocOcene, {error: weryfikacjaError}] = useMutation(zwrocOceneM, {
        refetchQueries,
        onCompleted: () => handleAtionCompleted('decline')
    })

    //@ zmień status oceny
    const [zmienStatusOceny, {error: zmienStatusOcenyError}] = useMutation(zmienStatusOcenyM, {
        refetchQueries
    })


    const [przeslijOcene, {error: submitError, loading: loadingSubmit}] = useMutation(submitOcenaM, {
        variables: {ocenaInput: form}, onCompleted: () => handleAtionCompleted('submit'), refetchQueries: [{
            query: listUsersOcenyQ,
            variables: {oceniajacy_id: oceniajacyId}
        }]
    })

    //@ zarządzaj aktywnymi tabami
    const handleTabsChange = (index: number) => {
        const max = tabs.tabHeads.length
        if (index >= max) {
            setTabIndex(max - 1)
        } else if (index <= 0) {
            setTabIndex(0)
        } else {
            setTabIndex(index)
        }
        return mode === 'edit' && updateOcena()
    }


    //@ funkcja generująca podgląd json
    // @ts-ignore
    const renderDebug = () => <ReactJson
        src={form}
        style={{fontSize: 14, background: '#ababab', padding: '.25rem', marginBottom: '.5rem'}}
        name='form'
        collapsed={true}/>


    const printDecision = (decision: string) => {
        switch (decision) {
            case 'positive':
                return <><Tag size={'lg'} colorScheme={'green'} ml={1}>Pozytywny</Tag></>
            case 'negative':
                return <><Tag size={'lg'} colorScheme={'red'} ml={1}>Negatywny</Tag></>
            case 'wezwanie':
                return <><Tag size={'lg'} colorScheme={'orange'} ml={1}>Wezwanie do
                    uzupełnienia</Tag></>
            case 'pending':
                return <><Tag colorScheme={'gray'} ml={1}> w trakcie</Tag></>

            default:
                return

        }
    }

    //@ przygotuj formularz
    const setFormState = (table: string, key: string, value: any) => {
        setForm((prev: any) => ({...prev, [table]: {...prev[table], [key]: value}}))
        //@ wyczyść uwagi gdy TAK
        if (form.formalne[key + '_uwagi'] && value === 1) setForm((prev: any) => ({
            ...prev,
            [table]: {...prev[table], [key + '_uwagi']: ''}
        }))

        //@ wyczyść error gdy pole zmieniane
        // @ts-ignore
        delete errors?.[table + '.' + key]
    }
    const setWeryfikacjaFormState = (key: string, value: any) => {
        setWeryfikacjaForm((prev: any) => ({...prev, [key]: value}))
    }

    //@ obsłuż kliknięcie modala (zatwierdź lub zwróć ocenę)
    const handleWeryfikacjaModal = (res: string) => {
        setWerErrors([])
        res === 'decline' && setWeryfikacjaFormState('decyzja', 'zwrocona')
        res === 'approve' && setWeryfikacjaFormState('decyzja', 'zatwierdzona')
        onOpenModalWeryf()
    }

    //@ obsłuż kliknięcie modala (zatwierdź lub zwróć ocenę)
    const handleWeryfikacja = () => {
        const dataToSend = Object.assign({}, {...weryfikacjaForm}, {
            ocena_id: +ocenaId!,
            user_id: weryfikujacy.sub,
            user_first_name: weryfikujacy.given_name,
            user_last_name: weryfikujacy.family_name,
            user_email: weryfikujacy.email,
            // @ts-ignore
            decyzja: weryfikacjaForm.decyzja,
            // @ts-ignore
            note: weryfikacjaForm.note
        })
        // @ts-ignore
        if (weryfikacjaForm?.decyzja === 'zatwierdzona') {
            zatwierdzOcene({variables: {zatwierdzOceneInput: dataToSend}}).then(() => onCloseModalWeryf())
            // @ts-ignore
        } else if (weryfikacjaForm?.decyzja === 'zwrocona') {
            zwrocOcene({variables: {zwrocOceneInput: dataToSend}}).then(() => onCloseModalWeryf())
        }
    }


    //@ sprawdź na wejsciu czy mode ustawiony
    useEffect(() => {
        if (!mode) navigate(-1)
        // eslint-disable-next-line
    }, [])

    //@ zmień status jeżeli po zwrocie
    useEffect(() => {
        if (mode === 'edit' && ocenaExtended?.ocena?.status?.nazwa === 'Zwrócona') zmienStatusOceny({
            variables: {zmienStatusOcenyInput: {ocena_id: ocenaExtended?.ocena?.id, status_id: 'W_EDYCJI'}}
        })
        // eslint-disable-next-line
    }, [ocenaExtended])

    //@ dodaj dane oceny do stanu, gdy dostępne
    useEffect(() => {
        if (ocena) {
            const ocenaPure = {...ocena.ocena}
            delete ocenaPure.status
            setForm(ocenaPure)
        }
    }, [ocena])
    // @ts-ignore
    //@ przeprowadzaj decyzję po każdej zmianie form
    useEffect(() => {
        dispatch(setOcenaData(form))
        setPunktyNiepeln(form?.merytoryczne?.czy_z_niepelnospr_punkty || 0)
        setPunktyIndyw(form?.merytoryczne?.indywidualny_program_punkty || 0)
        // @ts-ignore
        if (form?.merytoryczne) countAndSet(form, setPunktyOlimpiady, 'dodatk')
        // @ts-ignore
        if (form?.merytoryczne) countAndSet(form, setPunktyObow, 'obow')
        makeOcenaDecision(form, setKiller, setWezwanie, setDecision, setPunkty)
        // @ts-ignore
        setFormalna1(form?.formalne?._1podsumowanie1 !== null && ((form?.formalne?._1podsumowanie1 === 1 || (form?.formalne?._1podsumowanie1 === 0 && form?.formalne?._1podsumowanie2 === 1)) ? "Pozytywny" : 'Negatywny') || '')
        // @ts-ignore
        setFormalna2(form?.formalne?._2_2 !== null && (form?.formalne?._2_2 === 0 ? 'Negatywny' : 'Pozytywny') || '')
        // eslint-disable-next-line
    }, [form])


    useEffect(() => {
        if (submitError) { // @ts-ignore
            setErrors(submitError?.graphQLErrors[0]?.extensions?.c_errors)
        }
        if (weryfikacjaError) { // @ts-ignore
            setWerErrors(weryfikacjaError?.graphQLErrors[0]?.extensions?.c_errors)
        }
    }, [submitError, weryfikacjaError])

    // @ts-ignore
    return <div className={styles}>
        {(loadingOcena || loadingOcenaExtended || loadingSubmit || loadingWniosek) && <Loader/>}
        {/*{renderDebug()}*/}

        <Tabs isLazy variant='enclosed'>

            <TabList>
                <Tab bg={baseBg} me={2}>Ocena wniosku {mode === 'watch' && '[tryb podglądu]'} </Tab>
                <Tab bg={baseBg} me={2}>Dane wniosku</Tab>
                <Tab bg={baseBg} me={2}>Decyzja: {printDecision(decision)}</Tab>
                <Tab bg={baseBg} me={2}>Weryfikacje</Tab>
            </TabList>

            <TabPanels>
                <TabPanel position={'relative'}>

                    {countErrors(errors, '') > 0 && <Alert status='error'>
                        <AlertIcon/>
                        <AlertTitle mr={2}>W formularzu wykryto błędy!</AlertTitle>
                        <AlertDescription>Popraw je zanim prześlesz formularz. Liczba
                            błędów: {countErrors(errors, '')} </AlertDescription>
                    </Alert>}

                    <Tabs minH={'100%'} ms={-4} mt={5} isLazy index={tabIndex} onChange={handleTabsChange}
                          variant='unstyled'>
                        <Flex minH={'100%'}>
                            <TabList borderRadius={7} bg={baseBg} flex={1} p={4} maxW={'18rem'}>
                                <Flex gap={5} flexDir={'column'}>
                                    {tabs.tabHeads.map((t, idx) =>
                                        <Box key={idx}>
                                            <Tab
                                                flex={1}
                                                fontSize={17}
                                                _selected={{
                                                    width: '100%',
                                                    borderRadius: '3px',
                                                    color: 'white',
                                                    bg: `${colorMode === 'light' ? 'gray.500' : 'gray.800'}`,
                                                }}>
                                                {t.label}
                                                {/*// @ts-ignore*/}
                                                {countErrors(errors, t.errorsKey) > 0 &&
                                                    <Tag
                                                        colorScheme={'red'}
                                                        size={'sm'}
                                                        ml={1}
                                                        // @ts-ignore
                                                        title={'błędy'}>{countErrors(errors, t.errorsKey)}
                                                    </Tag>}

                                            </Tab> {t.isLastInType && <Box mt={4}>
                                            <hr/>
                                        </Box>} </Box>)}
                                </Flex>
                            </TabList>
                            <TabPanels flex={4} ml={5}>
                                {tabs.tabBodies.map((b, idx) => <TabPanel bg={baseBg} minH={'100%'} borderRadius={7}
                                                                          key={idx}>
                                    <OcenaPanel

                                        key={idx}
                                        // @ts-ignore
                                        errors={errors}
                                        type={b.type}
                                        form={form}
                                        set={setFormState}
                                        rows={b.rows}
                                        data={ocenaExtended || {}}/>
                                </TabPanel>)}
                            </TabPanels>
                        </Flex>
                    </Tabs>

                    <ModalWeryfikacja
                        isOpenModalWeryf={isOpenModalWeryf}
                        onCloseModalWeryf={onCloseModalWeryf}
                        weryfikacjaForm={weryfikacjaForm}
                        werErrors={werErrors}
                        setWeryfikacjaFormState={setWeryfikacjaFormState}
                        handleWeryfikacja={handleWeryfikacja}
                    />

                    <ButtonsPanel
                        curIndex={tabIndex}
                        noSubmit={ocena?.ocena?.status?.id === 'ZATWIERDZONY'}
                        noDecline={ocena?.ocena?.status?.id === 'ZWROCONY'}
                        disableNext={tabIndex === tabs.tabHeads.length - 1}
                        disablePrev={tabIndex === 0}
                        watchMode={mode === 'watch'}
                        verifyMode={mode === 'verify'}
                        save={updateOcena}
                        submit={przeslijOcene}
                        decline={() => handleWeryfikacjaModal('decline')}
                        approve={() => handleWeryfikacjaModal('approve')}
                        handleTabsChange={handleTabsChange}/>
                </TabPanel>

                <TabPanel>
                    <WniosekDane wniosek={wniosek}/>
                </TabPanel>

                <TabPanel>
                    <Decyzja
                        formalna1={formalna1}
                        formalna2={formalna2}
                        punktyObow={punktyObow}
                        // @ts-ignore
                        punktyDodatkowe={punktyIndyw + punktyNiepeln + punktyOlimpiady}
                        punktyOlimpiady={punktyOlimpiady}
                        punktyIndyw={punktyIndyw}
                        punktyNiepeln={punktyNiepeln}
                        killer={killer} wezwanie={wezwanie} decision={decision} punkty={punkty}
                        ocena={form}
                        wniosek={ocenaExtended?.ocena?.wniosek}/>
                </TabPanel>

                <TabPanel>
                    <WeryfikacjeList data={ocenaExtended?.ocena?.weryfikacja}/>
                </TabPanel>


            </TabPanels>
        </Tabs>

    </div>
}

PageOcena.propTypes = {
    debug: PropTypes.bool,
}
