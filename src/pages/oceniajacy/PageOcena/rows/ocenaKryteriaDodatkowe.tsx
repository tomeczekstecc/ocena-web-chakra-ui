import {options} from "../options/options";

export const ocenaKryteriaDodatkowe = [
    {
        label: "Za każde przedłożone zaświadczenie o posiadaniu tytułu laureata olimpiady/turnieju dotyczącego przedmiotów kluczowych",
        dataKey: "dodatk1_1",
        table: 'merytoryczne',
        options: options.numsTo10,
        mapper: 'dodatk1_1',
    },
    {
        label: "Za każde przedłożone zaświadczenie o posiadaniu tytułu finalisty olimpiady/turnieju dotyczącego przedmiotów kluczowych",
        dataKey: "dodatk1_2",
        table: 'merytoryczne',
        options: options.numsTo10,
        mapper: 'dodatk1_2',
    },
    {
        label: "Za każde przedłożone zaświadczenie o posiadaniu tytułu laureata olimpiady/turnieju dotyczącego przedmiotów pozostałych",
        dataKey: "dodatk1_3",
        table: 'merytoryczne',
        options: options.numsTo10,
        mapper: 'dodatk1_3',
    },
    {
        label: "Za każde przedłożone zaświadczenie o posiadaniu tytułu finalisty olimpiady/turnieju dotyczącego przedmiotów pozostałych.",
        dataKey: "dodatk1_4",
        table: 'merytoryczne',
        options: options.numsTo10,
        mapper: 'dodatk1_4',
    },

    {
        label: "Za każde przedłożone zaświadczenie o posiadaniu tytułu laureata konkursu dotyczącego przedmiotów kluczowych",
        dataKey: "dodatk2_1",
        table: 'merytoryczne',
        options: options.numsTo10,
        mapper: 'dodatk2_1',
    },
    {
        label: "Za każde przedłożone zaświadczenie o posiadaniu tytułu finalisty konkursu dotyczącego przedmiotów kluczowych",
        dataKey: "dodatk2_2",
        table: 'merytoryczne',
        options: options.numsTo10,
        mapper: 'dodatk2_2',
    },
    {
        label: "Za każde przedłożone zaświadczenie o posiadaniu tytułu laureata konkursu dotyczącego przedmiotów pozostałych",
        dataKey: "dodatk2_3",
        table: 'merytoryczne',
        options: options.numsTo10,
        mapper: 'dodatk2_3',
    },
    {
        label: "Za każde przedłożone zaświadczenie o posiadaniu tytułu finalisty konkursu dotyczącego przedmiotów pozostałych",
        dataKey: "dodatk2_4",
        table: 'merytoryczne',
        options: options.numsTo10,
        mapper: 'dodatk2_4',
    },
    {
        label: "Indywidualny program nauczania lub tok nauki",
        dataKey: "indywidualny_program",
        table: 'merytoryczne',
        options: options.indywidualny_program,
        mapper: 'indywidualny_program',
        string: true
    },
    {
        label: "Uczeń/uczennica z niepełnosprawnościami",
        dataKey: "czy_z_niepelnospr",
        table: 'merytoryczne',
        options: options.czy_z_niepelnospr,
        mapper: 'czy_z_niepelnospr',
    },


]

