export const ocenaDecyzjaIFormalna = [
    {
        label: "Czy wniosek spełnia wszystkie ogólne kryteria formalne i może zostać przekazany do oceny merytorycznej?",
        dataKey: "_1podsumowanie1",
        table: 'formalne',
        optionNie: true,
        noOptionWezw: true,
        required: true,

    },
    {
        label: "Czy wniosek może zostać skierowany do uzupełnienia i/lub korekty zidentyfikowanych braków formalnych?",
        dataKey: "_1podsumowanie2",
        table: 'formalne',
        hide: true,
        optionNie: true,
        noOptionWezw: true

    },

]

