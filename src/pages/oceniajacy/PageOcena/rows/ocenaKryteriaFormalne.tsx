export const ocenaKryteriaFormalne = [
    {
        label: "Czy wniosek został złożony w terminie i miejscu wskazanym przez instytucję prowadzącą nabór?",
        dataKey: "_1a1",
        table: 'formalne',
        optionNie: true,
        noOptionWezw: true,
        required: true,

    },
    {
        label: "Czy wniosek został prawidłowo podpisany za pomocą Profilu Zaufanego w przypadku złożenia przez ePuap?",
        dataKey: "_1a2",
        table: 'formalne',
        optionNd: true,
        required: true,

    },
    {
        label: "Czy do wniosku zostało dołączone poprawne świadectwo szkolne wydane na zakończenie roku szkolnego poprzedzającego rok szkolny, na który przyznawane jest stypendium?",
        dataKey: "_1a3",
        table: 'formalne',
        required: true,

    },
    {
        label: "Czy do wniosku zostało dołączone podpisane poprawne oświadczenie kandydata na Opiekuna Dydaktycznego Stypendysty?",
        dataKey: "_1a4",
        table: 'formalne',
        required: true,

    },
]

