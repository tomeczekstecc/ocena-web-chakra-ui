export const danePodstawowe = [
    {
        label: "Numer",
        dataKey: "numer_wniosku",
        preferredDataKey: "wniosek.numer",
        able: true,
        table: 'dane',
    },
    {
        label: "Czy wniosek został złożony w wersji elektronicznej przy wykorzystaniu platformy ePUAP ?",
        dataKey: "czy_epuap",
        input: 'radio',
        table: 'dane',
        required: true,
    },
    {
        label: "Data wpływu",
        dataKey: "data_wplywu_wniosku",
        input: 'date',
        able: true,
        table: 'dane',
        required: true,
    },
    {
        label: "Imię ucznia",
        dataKey: "imie_ucznia",
        preferredDataKey: "wniosek.uczen.imie",
        able: true,
        table: 'dane',
        // @ts-ignore,
        pozaSystemInput: 'input',
    },
    {
        label: "Nazwisko ucznia",
        dataKey: "nazwisko_ucznia",
        preferredDataKey: "wniosek.uczen.nazwisko",
        able: true,
        table: 'dane',
        pozaSystemInput: 'input'

    }, {
        label: "Imię wnioskodawcy",
        dataKey: "wnioskodawca_imie",
        preferredDataKey: "wniosek.wnioskodawca_imie",
        able: true,
        table: 'dane',


    },
    {
        label: "Nazwisko wnioskodawcy",
        dataKey: "nazwisko_wnioskodawcy",
        preferredDataKey: "wniosek.wnioskodawca_nazwisko",
        able: true,
        table: 'dane',

    },
    {
        label: "Telefon wnioskodawcy",
        dataKey: "wnioskodawca_telefon",
        preferredDataKey: "wniosek.wnioskodawca_telefon",
        able: true,
        table: 'dane',
        pozaSystemInput: 'input'
    },
    {
        label: "Email wnioskodawcy",
        dataKey: "wnioskodawca_email",
        preferredDataKey: "wniosek.wnioskodawca_email",
        able: true,
        table: 'dane',
        pozaSystemInput: 'input'
    },
    {
        label: "Rodzaj szkoły",
        dataKey: "rodzaj_szkoly",
        preferredDataKey: "wniosek.szkola.typ",
        able: true,
        table: 'dane',
        pozaSystemInput: 'input'
    },
    {
        label: "Nazwa szkoły",
        dataKey: "szkola",
        preferredDataKey: "wniosek.szkola.nazwa",
        able: true,
        table: 'dane',
        pozaSystemInput: 'input'
    },

    {
        label: "Klasa",
        dataKey: "klasa_ucznia",
        preferredDataKey: "wniosek.uczen.klasa",
        able: true,
        table: 'dane',
        pozaSystemInput: 'select'
    },
    {
        label: "Imię opiekuna",
        dataKey: "imie_opiekuna",
        preferredDataKey: "wniosek.opiekun.imie",
        able: true,
        table: 'dane',
        pozaSystemInput: 'input'
    },
    {
        label: "Nazwisko opiekuna",
        dataKey: "nazwisko_opiekuna",
        preferredDataKey: "wniosek.opiekun.nazwisko",
        able: true,
        table: 'dane',
        pozaSystemInput: 'input'
    },
    {
        label: "Email opiekuna",
        dataKey: "email_opiekuna",
        preferredDataKey: "wniosek.opiekun.email",
        able: true,
        table: 'dane',
        pozaSystemInput: 'input'
    },
    {
        label: "Telefon opiekuna",
        dataKey: "telefon_opiekuna",
        preferredDataKey: "wniosek.opiekun.telefon",
        able: true,
        table: 'dane',
        pozaSystemInput: 'input'
    },


]
