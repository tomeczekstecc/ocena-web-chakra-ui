import {options} from "../options/options";

export const ocenaKryteriaObowiazkowe = [
    {
        label: "Średnia ocen z trzech przedmiotów kierunkowych",
        dataKey: "obow1",
        table: 'merytoryczne',
        options: options.priGradesAvg,
        mapper: 'priGradesAvg',
        required: true,


    },
    {
        label: "Średnia ocen ze wszystkich przedmiotów",
        dataKey: "obow2",
        table: 'merytoryczne',
        options: options.allGradesAvg,
        mapper: 'allGradesAvg',
        string: true,
        required: true,

    },

]

