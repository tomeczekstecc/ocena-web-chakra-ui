export const ocenaCzescA_B = [
    {
        label: "Czy wnioskodawca właściwie wskazał przedmioty kierunkowe w części V wniosku?",
        dataKey: "_1c1",
        table: 'formalne',
        required: true,

    },
    {
        label: "Czy wnioskodawca poprawnie wpisał średnią z wszystkich przedmiotów na świadectwie?",
        dataKey: "_1c2",
        table: 'formalne',
        required: true,

    },

    {
        label: "Czy wnioskodawca poprawnie wpisał średnią z 3 wybranych przedmiotów kierunkowych?",
        dataKey: "_1c3",
        table: 'formalne',
        required: true,

    },
    {
        label: "Czy wnioskodawca  właściwie wskazał przedmioty w części VII wniosku?",
        dataKey: "_1c4",
        table: 'formalne',
        required: true,

    },
    {
        label: "Czy wnioskodawca właściwie wskazał rezultaty w części VII wniosku - tabela nr 1 ?",
        dataKey: "_1c5",
        table: 'formalne',
        required: true,

    },
    {
        label: "Czy wnioskodawca właściwie wskazał rezultaty w części VII wniosku - tabela nr 2 ?",
        dataKey: "_1c6",
        table: 'formalne',
        required: true,

    },
    {
        label: "Czy wnioskodawca prawidłowo przedstawił plan wydatków?",
        dataKey: "_1c7",
        table: 'formalne',
        required: true,

    },
    {
        label: "Czy we wniosku wypełnione są wszystkie wymagane pola?",
        dataKey: "_1c8",
        table: 'formalne',
        required: true,

    },
]

