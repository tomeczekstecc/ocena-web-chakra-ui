export const ocenaDecyzjaIIFormalna = [
    {
        label: "Czy aktualna ocena dokonywana jest po wezwaniu do uzupełnienia?",
        dataKey: "_2_1",
        table: 'formalne',
        optionNie: true,
        noOptionWezw: true,
        required: true,

    },
    {
        label: "Czy zidentyfikowane podczas pierwszej oceny formalnej braki formalne zostały skorygowane w terminie wskazanym w wezwaniu?",
        dataKey: "_2_2",
        table: 'formalne',
        optionNie: true,
        noOptionWezw: true
    },

]

