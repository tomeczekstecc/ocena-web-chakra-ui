export const ocenaKryteriaObligatoryjne = [
    {
        label: "Czy uczeń/uczennica uczęszcza do szkoły ponadpodstawowej na terenie województwa śląskiego?",
        dataKey: "_1b1",
        table: 'formalne',
        optionNie: true,
        noOptionWezw: true,
        required: true,

    },
    {
        label: "Czy średnia ocen z  trzech wybranych przedmiotów kierunkowych uzyskana na zakończenie roku szkolnego 2019/2020 wynosi co najmniej 5,33?",
        dataKey: "_1b2",
        table: 'formalne',
        optionNie: true,
        required: true,

    },
    {
        label: "Czy średnia ocen ze wszystkich przedmiotów uzyskana na zakończenie roku szkolnego 2019/2020 wynosi co najmniej 5,00?",
        dataKey: "_1b3",
        table: 'formalne',
        optionNie: true,
        required: true,

    },
]

