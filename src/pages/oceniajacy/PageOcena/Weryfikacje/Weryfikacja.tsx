import React from 'react';
import {
    AccordionButton,
    AccordionIcon,
    AccordionItem,
    AccordionPanel,
    Avatar,
    Box,
    Flex,
    Tag,
    Text
} from "@chakra-ui/react";

// @ts-ignore
let Weryfikacja = ({cur, i, idx}) => {
    return (
        <AccordionItem>
            {({isExpanded}) => (<Box>
                {/*// @ts-ignore*/}
                <AccordionButton border={isExpanded ? '1px solid' : null} borderBottom={'none'}>
                    <Flex w={'100%'} justifyContent={'space-between'}>
                        <Flex flexWrap={'wrap'} gap={4} w={'100%'} alignItems={'center'}
                              justifyContent={'left'}>
                            <Tag size={'lg'}>{idx + 1}</Tag>
                            <Tag size={'lg'} colorScheme={cur.theme}> {cur.text}</Tag>
                            {/*// @ts-ignore*/}
                            <Box title={cur.dateTime} size={'lg'}
                                 colorScheme={cur.theme}>{cur.dateTimeAgo}</Box>
                            <Flex gap={1} alignItems={'center'}>
                                <Avatar size={'xs'} name={i.user_first_name + ' ' + i.user_last_name}/>
                                <Box> {i.user_first_name} {i.user_last_name} ({i.user_email})</Box>
                            </Flex>
                        </Flex>
                        <Text>{''}</Text>
                        <AccordionIcon/>
                    </Flex>
                </AccordionButton>
                {/*// @ts-ignore*/}

                <AccordionPanel border={isExpanded ? '1px solid' : null} borderTop={'none'} pb={4}
                                pt={10}>
                    {i.note || 'Brak informacji.'}
                </AccordionPanel>
            </Box>)}
        </AccordionItem>);
};
// @ts-ignore
Weryfikacja = React.memo(Weryfikacja);
export default Weryfikacja;
