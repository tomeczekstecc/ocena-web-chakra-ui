import React from 'react'
import {
    Accordion,

} from "@chakra-ui/react";
import moment from 'moment'
import 'moment/locale/pl'
import Weryfikacja from "./Weryfikacja";

let WeryfikacjeList = (props: any) => {
    return <>
        <Accordion allowMultiple mx={10} mt={5}>
            {!props.data.length && 'Dotychczas nie weryfikowano oceny.'}
            {[...props.data]
                .sort((a: { id: any; }, b: { id: any; }) => Number(b?.id) - Number(a?.id))
                .map((i: any, idx: number) => {
                    // @ts-ignore
                    const date = moment.unix(new Date(i.created_at / 1000)).utcOffset("+02:00")
                    const cur = {
                        theme: i.decyzja === 'zatwierdzona' ? 'green' : 'red',
                        text: i.decyzja === 'zatwierdzona' ? 'ZATWIERDZONA' : 'ZWRÓCONA',
                        dateTimeAgo: date.add(1, 'h').fromNow(),
                        dateTime: date.format('YYYY-MM-DD HH:mm')
                    }
                    return <>
                        <Weryfikacja cur={cur} i={i} idx={idx}/>
                    </>
                })}
        </Accordion>
    </>
}
// @ts-ignore
WeryfikacjeList = React.memo(WeryfikacjeList)

export {WeryfikacjeList}
