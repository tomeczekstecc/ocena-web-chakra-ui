import {
    danePodstawowe,
    ocenaCzescA_B,
    ocenaDecyzjaIFormalna, ocenaDecyzjaIIFormalna, ocenaKryteriaDodatkowe,
    ocenaKryteriaFormalne,
    ocenaKryteriaObligatoryjne, ocenaKryteriaObowiazkowe
} from "../rows";

export const tabs = {
    tabHeads: [
        {label: 'Dane identyfikacyjne', bg: 'gray.600', errorsKey: 'dane', isLastInType: true},
        {label: 'Kryteria formalne', bg: 'gray.600', errorsKey: 'formalne._1a'},
        {label: 'Kryteria obligatoryjne', bg: 'gray.600', errorsKey: 'formalne._1b'},
        {label: 'Część A i B  wniosku', bg: 'gray.600', errorsKey: 'formalne._1c'},
        {label: 'Decyzja I Formalna', bg: 'gray.600', errorsKey: 'formalne._1podsumowanie'},
        {label: 'Decyzja II Formalna', bg: 'gray.600', errorsKey: 'formalne._2_', isLastInType: true},
        {label: 'Kryteria obowiązkowe', bg: 'gray.600', errorsKey: 'merytoryczne.obow'},
        {label: 'Kryteria dodatkowe', bg: 'gray.600'},
    ],
    tabBodies: [
        {type: 'dane', rows: danePodstawowe},
        {type: 'formalna', rows: ocenaKryteriaFormalne},
        {type: 'formalna', rows: ocenaKryteriaObligatoryjne},
        {type: 'formalna', rows: ocenaCzescA_B},
        {type: 'formalna', rows: ocenaDecyzjaIFormalna},
        {type: 'formalna', rows: ocenaDecyzjaIIFormalna},
        {type: 'merytoryczna', rows: ocenaKryteriaObowiazkowe},
        {type: 'merytoryczna', rows: ocenaKryteriaDodatkowe},
    ]

}
