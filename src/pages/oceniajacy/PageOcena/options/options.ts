export const options = {
    priGradesAvg: [
        {label: '', value: -1},
        {label: '5,33', value: 5.33},
        {label: '5,67', value: 5.67},
        {label: '6,00', value: 6.00},
    ],
    allGradesAvg: [
        {label: '', value: null},
        {label: '5,00 - 5,17', value: '5,00 - 5,17'},
        {label: '5,18 – 5,33', value: '5,18 – 5,33'},
        {label: '5,34 – 5,50', value: '5,34 – 5,50'},
        {label: '5,51 – 5,67', value: '5,51 – 5,67'},
        {label: '5,68 – 5,84', value: '5,68 – 5,84'},
        {label: '5,85 – 6,00', value: '5,85 – 6,00'},
    ],
    numsTo10: [
        {label: 0, value: 0},
        {label: 1, value: 1},
        {label: 2, value: 2},
        {label: 3, value: 3},
        {label: 4, value: 4},
        {label: 5, value: 5},
        {label: 6, value: 6},
        {label: 7, value: 7},
        {label: 8, value: 8},
        {label: 9, value: 9},
        {label: 10, value: 10},
    ],

    indywidualny_program: [
        {label: '', value: null},
        {label: 'ze wszystkich przedmiotów', value: 'ze wszystkich przedmiotów'},
        {
            label: 'z 1 lub większej ilości przedmiotów kluczowych',
            value: 'z 1 lub większej ilości przedmiotów kluczowych'
        },
        {
            label: 'z 1 lub większej ilości przedmiotów pozostałych',
            value: 'z 1 lub większej ilości przedmiotów pozostałych'
        },
        {label: 'Nie', value: 'Nie'},
    ],
    czy_z_niepelnospr: [
        {label: '', value: null},
        {label: 'Tak', value: 1},
        {label: 'Nie', value: 0},


    ],
}
