import React from "react";
import {
    Button,
    FormControl, FormErrorIcon, FormErrorMessage, FormLabel,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent, ModalFooter,
    ModalHeader,
    ModalOverlay, Textarea
} from "@chakra-ui/react";


export const ModalWeryfikacja = (props: any) => {
    return <Modal size={'3xl'} isOpen={props.isOpenModalWeryf} onClose={() => props.onCloseModalWeryf()}>
        <ModalOverlay/>
        <ModalContent>
            <ModalHeader>Zakończ weryfikację (operacji nie da sie cofnąć)</ModalHeader>
            <ModalCloseButton/>
            <ModalBody pb={6}>
                {/*// @ts-ignore*/}
                {props.weryfikacjaForm?.decyzja === 'zwrocona' ? <>
                    {/*// @ts-ignore*/}
                    <FormControl isInvalid={props.werErrors.note}>
                        <FormLabel>Uzasadnienie</FormLabel>
                        <Textarea
                            rows={7}
                            // @ts-ignore
                            value={props.weryfikacjaForm.note}
                            placeholder={'Podaj uzasadnienie zwrotu oceny do poprawy. Wskaż elementy wymagające poprawy.'}
                            onChange={(e) => props.setWeryfikacjaFormState('note', e.target.value)}/>
                        {/*// @ts-ignore*/}
                        <FormErrorMessage> <FormErrorIcon/> {props.werErrors?.note}</FormErrorMessage>

                    </FormControl>
                </> : 'Potwierdź zatwierdzenie oceny'}

            </ModalBody>
            <ModalFooter>
                <Button colorScheme='blue' mr={3} onClick={props.onCloseModalWeryf}>
                    Rezygnuj
                </Button>
                <Button
                    // @ts-ignore
                    onClick={props.handleWeryfikacja}
                    // onClick={() => deleteOcena({variables: {ocena_id: +ocena.id!}}).then(() => onCloseModalWeryf())}
                    variant='ghost'
                    // @ts-ignore
                    colorScheme={'red'}>{props.weryfikacjaForm?.decyzja === 'zwrocona' ? "Zwróć" : "Zatwierdź"}</Button>
            </ModalFooter>
        </ModalContent>
    </Modal>


}
