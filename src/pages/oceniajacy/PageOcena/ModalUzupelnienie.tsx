import React, {useEffect, useState} from "react";
import {
    Alert, AlertDescription,
    AlertIcon,
    AlertTitle,
    Box, Button, ButtonGroup, FormControl, FormErrorMessage, FormLabel, Grid, GridItem, Input,
    Modal, ModalBody,
    ModalContent,
    ModalHeader,
    ModalOverlay,
} from "@chakra-ui/react";
import {useDispatch, useSelector} from "react-redux";
import {useNavigate} from "react-router-dom";
import {addToast} from "../../../redux/toaster";
import {msg,} from "../../../utils/messages/manager";
import {useMutation} from "@apollo/client";
import {uzupelnijWniosekPozasystM} from "../../../graphql/oceniająjcy/mutations";
import {Field, Form, Formik} from "formik";
import {danePodstawowe} from "./rows";
import {logToC} from "../../../utils/helpers";
import {uzupelnijWniosek} from "../../../utils/validationSchema";
import {ocenaExtendedQ, WniosekQ} from "../../../graphql/oceniająjcy/queries";


// @ts-ignore
export const ModalUzupelnienie = ({onClose, onOpen, isOpen = false}) => {

    const dispatch = useDispatch()
    const navigate = useNavigate()
    // @ts-ignore
    const wniosek = useSelector(store => store?.wniosek)
    // @ts-ignore
    const ocenaId = useSelector(store => store?.ocena?.data?.id)

    let initialValues = {}
    // @ts-ignore
    danePodstawowe.map(d => {
        if (d.pozaSystemInput) { // @ts-ignore
            initialValues[d.dataKey] = ''
        }
    })
    const [serverErrors, setServerErrors] = useState([])

    const onCompleted = () => {
        dispatch(addToast({text: msg.success.filled, variant: 'success'}));
        onClose()
    }

    const onError = (err: any) => {
        return setServerErrors(err.graphQLErrors[0].extensions.c_errors)
    }

    const [uzupelnijWniosekPozasyst] = useMutation(uzupelnijWniosekPozasystM, {
        onError,
        onCompleted,
        refetchQueries: [
            {query: WniosekQ, variables: {wniosek_id: +wniosek.id}},
            {query: ocenaExtendedQ, variables: {ocena_id: ocenaId}},
        ]
    })

    const onSubmit = (values: any) => {
        uzupelnijWniosekPozasyst({
            variables: {
                uzupelnijWniosekPozasystInput: {
                    ...values, wniosek_id: +wniosek.id
                }
            }
        })
    }

    const handleOnClose = () => {
        onClose();
        navigate(-1)
    }

    useEffect(() => {
        if (wniosek) logToC(wniosek)
        if (wniosek && wniosek.systemowy === 0 && wniosek.uzupelniony === 0) onOpen()
        // eslint-disable-next-line
    }, [wniosek]);


    // @ts-ignore
    return <>
        <Modal closeOnOverlayClick={false} size={'5xl'} isOpen={isOpen} onClose={() => {
            // @ts-ignore
            onClose()
        }}>
            <ModalOverlay/>
            <ModalContent>

                <ModalHeader>Uzupełnij dane</ModalHeader>
                <Box mx={4}>
                    <Alert status='warning'>
                        <AlertIcon/>
                        <AlertTitle>Niepełne dane wniosku</AlertTitle>
                        <AlertDescription>Wniosek złożony został poza systemem, więc musisz uzupełnić kilka
                            danych</AlertDescription>
                    </Alert></Box>
                {/*<ModalCloseButton/>*/}
                <ModalBody pb={6}>

                    <Formik
                        initialValues={initialValues}
                        validationSchema={uzupelnijWniosek}
                        onSubmit={onSubmit}>
                        <Form>
                            <Grid templateColumns='repeat(2, 1fr)' gap={6} mb={5}>
                                {danePodstawowe.map((d, idx) => d.pozaSystemInput &&
                                    <GridItem key={idx} w='100%'>
                                        <Field name={d.dataKey}>
                                            {/*// @ts-ignore*/}
                                            {({form, field}) => {
                                                logToC({form})
                                                return <FormControl
                                                    // @ts-ignore
                                                    isInvalid={(!!form.errors[d.dataKey] && form.touched[d.dataKey]) || serverErrors[d.dataKey]
                                                    }>
                                                    <FormLabel htmlFor='dataKey'>{d.label}</FormLabel>
                                                    <Input
                                                        // @ts-ignore
                                                        onClick={() => setServerErrors({dataKey: ''})}
                                                        {...field}
                                                        id='dataKey'
                                                    />
                                                    {/*// @ts-ignore*/}
                                                    <FormErrorMessage>{form.errors[d.dataKey] || serverErrors[d.dataKey]
                                                    }</FormErrorMessage>
                                                </FormControl>
                                            }}
                                        </Field></GridItem>)}
                            </Grid>
                            <ButtonGroup>
                                
                                <Button
                                    // @ts-ignore
                                    type={'submit'} variant={'outline'} alignSelf={'flex-start'}
                                    colorScheme={'green'}>Dodaj</Button>
                                <Button variant={'outline'} alignSelf={'flex-start'}
                                        colorScheme={'orange'} onClick={handleOnClose}>Anuluj</Button>
                            </ButtonGroup>

                        </Form>
                    </Formik>

                </ModalBody>
            </ModalContent>
        </Modal>

    </>
}
