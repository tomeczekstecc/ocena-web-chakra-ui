export const files = {
    
    mimeTypesAllowed: ['image/jpeg', 'image/png', 'application/pdf', '.xlsx', '.xls', '.doc', '.docx', '.zip'],
    maxSize: 4000 * 1024
}
