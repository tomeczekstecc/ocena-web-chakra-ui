const SECOND = 1000
const MINUTE = SECOND * 60
const HOUR = MINUTE * 60

export const app = {
    idleTime: HOUR * 2 - SECOND,
    idleTimePopUp: MINUTE * 5,
    // idleTime: SECOND * 10,
    // idleTimePopUp: SECOND * 5,
}
